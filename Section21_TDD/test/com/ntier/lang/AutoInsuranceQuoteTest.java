package com.ntier.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

class AutoInsuranceQuoteTest {
	

@Test
public void testGetCostForUnder25()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(24,"", 0, 0);
	assertEquals(500, quote.getCost());
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(25,"", 0, 0);
	assertEquals(600, quote1.getCost());
	assertFalse(500 == quote1.getCost());

}

@Test
public void testGetCostFor25To30()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(25,"", 0, 0);
	assertEquals(600, quote.getCost());
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(28,"", 0, 0);
	assertEquals(600, quote1.getCost());
	assertFalse(500 == quote1.getCost());
	
	AutoInsuranceQuote quote2 = new AutoInsuranceQuote(30, "", 0, 0);
	assertEquals(600, quote2.getCost());
	assertFalse(500 == quote2.getCost());

}

@Test
public void testGetCostFor31To40()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(29,"", 0, 0);
	assertFalse(700 == quote.getCost());
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(40,"", 0, 0);
	assertTrue(700 == quote1.getCost());
	
	AutoInsuranceQuote quote2 = new AutoInsuranceQuote(41,"", 0, 0);
	assertFalse(700 == quote2.getCost());

}

@Test
public void testGetCostFor41To50()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(39,"", 0, 0);
	assertFalse(800 == quote.getCost());
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(45,"", 0, 0);
	assertTrue(800 == quote1.getCost());
	
	AutoInsuranceQuote quote2 = new AutoInsuranceQuote(50,"", 0, 0);
	assertFalse(700 == quote2.getCost());
	assertTrue(800 == quote2.getCost());

}

@Test
public void testGetCostOver50()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(49,"", 0, 0);
	assertFalse(1000 == quote.getCost());
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(50,"", 0, 0);
	assertFalse(1000 == quote1.getCost());
	
	AutoInsuranceQuote quote2 = new AutoInsuranceQuote(55,"", 0, 0);
	assertTrue(1000 == quote2.getCost());
}

@Test
public void testGetCharges()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(30, "male", 0, 0);
	assertEquals(60, quote.getCharges());
	assertFalse(50 == quote.getCharges());	
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(30, "female", 0, 0);
	assertEquals(0, quote1.getCharges());
	assertFalse(50 == quote1.getCharges());
	
}

@Test
public void testSetters()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(0, "", 0, 0);
	int age = 23;
	String gender = "male";
	int experience = 2;
	int driversCount = 3;
	quote.setAge(age);
	quote.setGender(gender);
	quote.setExperience(experience);
	quote.setDriversCount(driversCount);
	assertTrue(quote.getAge() == age);
	assertTrue(quote.getGender() == "male");
	assertTrue(quote.getExperience() == 2);
	assertTrue(quote.getDriversCount()== 3);
}

@Test
public void testGetQuote()
{
	AutoInsuranceQuote quote = new AutoInsuranceQuote(24, "male", 2, 2);
	assertEquals(540, quote.getQuote());
	assertFalse(600 == quote.getQuote());	
	
	AutoInsuranceQuote quote1 = new AutoInsuranceQuote(24, "female", 2, 2);
	assertEquals(490, quote1.getQuote());
	assertFalse(600 == quote1.getQuote());	
}

@Test
public void testGetDiscountByDrivers() {
	
	AutoInsuranceQuote quote = new AutoInsuranceQuote(24, "male", 2, 3);
	assertEquals(0.02, quote.getDiscountRateForDrivers());
}





}
