
// CalculatorClient class to test Calculator class

 class CalculatorClient {

	public static void main(String[] args) {
		
		Calculator cal = new Calculator();
		
		System.out.println(cal.add(2, 3));  			// invoking add method and displaying result
		System.out.println(cal.subtract(4, 2));			// invoking subtract method and displaying result
		System.out.println(cal.multiply(2, 2));			// invoking multiply method and displaying result
		System.out.println(cal.divison(4, 2));			// invoking divison method and displaying result
		System.out.println(cal.intDivison(4, 3));		// invoking intDivison method and displaying result
		System.out.println(cal.stringLength("Welcome"));// invoking stringLength method and displaying result
		System.out.println(cal.compare(4, 2));			// invoking compare method and displaying result
		

	}

}
