
// Calculator.java
// This class is of default scope i.e package access

 class Calculator {

	// Add method, takes two arguments and return the result
	 public double add(double num1, double num2)
	 {
		 return num1 + num2;
	 }
	 
	// Subtract method, takes two arguments and return the result
	 public double subtract(double num1, double num2)
	 {
		 return num1 - num2;
	 }
	 
	// Multiply method, takes two arguments and return the result
	 public double multiply(double num1, double num2)
	 {
		 return num1 * num2;
	 }
	 
	// Divison, takes two arguments and return the result
	 public double divison(double num1, double num2)
	 {
		 return num1 / num2;
	 }
	 
	 // Integer Divison, takes two integers and return the result
	 public int intDivison(int num1, int num2)
	 {
		 return num1 / num2;
	 }
	 
	 // Method length, takes String as argument and returns length of the String.
	 public int stringLength(String string)
	 {
		 return string.length();
	 }
	 
	 // Method comparator, takes two numbers and returns true if the first one is larger than the second.
	 public boolean compare(double num1, double num2)
	 {
			return (num1 > num2) ? true : false;			 
	 }
	 
}
