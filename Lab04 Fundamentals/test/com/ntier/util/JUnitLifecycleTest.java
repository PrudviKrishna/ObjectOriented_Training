package com.ntier.util;


import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class JUnitLifecycleTest {
	
	@Before
	public void doSetUp()
	{
		System.out.println("@Before - doSetUp method is executed");
	}

	@After
	public void doTearDown()
	{
		System.out.println("@After - doTearDown method is executed");
	}
	
	@Test
	public void testA()
	{
		System.out.println("@Test - testA method is executed");
	}
	
	@Test
	public void testB()
	{
		System.out.println("@Test - testB method is executed");
	}
	
	@Test
	public void testC()
	{
		System.out.println("@Test - testC method is executed");
	}
}
