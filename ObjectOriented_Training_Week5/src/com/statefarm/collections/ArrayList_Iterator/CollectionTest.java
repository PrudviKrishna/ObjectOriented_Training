package com.statefarm.collections.ArrayList_Iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

// CollectionTest.java
// Collection interface demonstrated via ArrayList object
public class CollectionTest {

	public static void main(String[] args) {
	
		// add elements in colors array to list
		String[] colors = {"MAGENTA", "RED", "WHITE", "BLUE", "CYAN"};
		List<String> list = new ArrayList<String>(); 
		
		for (String color : colors)
		{
			list.add(color);  // adds color to end of the list
		}

		// add elements from removeColors array to removeList list
		String[] removeColors = {"RED", "WHITE", "BLUE"};
		List<String> removeList = new ArrayList<String>();
		
		for (String color : removeColors)
		{
			removeList.add(color);
		}
		
		// output list contents
		System.out.println("ArrayList: ");
		
//		for (String item : list)
//		{
//			System.out.println(item);
//		}
		
		for (int index = 0; index < list.size(); index++)
		{
			System.out.printf("%s ", list.get(index));
		}
		
		// remove from the list the colors contained in removeList
		removeColors(list, removeList);
		
		// output list contents
		System.out.printf("%n%nArrayList after calling removeColors:%n");
		
		for (String color : list)
		{
			System.out.printf("%s ", color);
		}
		
		
	}
	
	public static void removeColors(Collection<String> collection1, Collection<String> collection2)
	{
		// get iterator
		Iterator<String> iterator = collection1.iterator();
		
		// loop while collection has items
		while (iterator.hasNext())
		{
			if (collection2.contains(iterator.next()))
			{
				iterator.remove();	// remove current element
			}
		}
	}

} // end class CollectionTest
