package com.statefarm.collections.LinkedList;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListTest {

	public static void main(String[] args) {
		
		// add colors array elements to list
		String[] colors = {"black", "yellow", "green", "blue", "violet", "silver"};
		List<String> list1 = new LinkedList<String>();

		for (String color : colors)
		{
			list1.add(color);
		}
		
		// add colors2 array elements to list2
		String[] colors2 = {"gold", "white", "brown", "blue", "gray", "silver"};
		List<String> list2 = new LinkedList<String>();

		for (String color : colors2)
		{
			list2.add(color);
		}
		
		list1.addAll(list2); // concatenate lists or add list2 elements to list1
		list2 = null;  // release resources
		
		printList(list1); // print list1 elements
		
		convertToUppercaseStrings(list1); // convert to upper case strings
		printList(list1); // print list1 elements
		
		System.out.printf("%nDeleting elements 4 to 6...");
		removeItems(list1, 4, 7);	// remove items 4 to 6 from list1
		printList(list1); // print list1 elements
		printReversedList(list1); // print list in reverse order
		
		
	}
	
	// output list contents
	private static void printList(List<String> list)
	{
		System.out.printf("%nList:%n");
		for (String item : list)
		{
			System.out.printf("%s ", item);
		}
		System.out.println();
	}
	
	private static void convertToUppercaseStrings(List<String> list)
	{
		ListIterator<String> listIterator = list.listIterator();
		
		while (listIterator.hasNext())
		{
			String upperCase = listIterator.next().toUpperCase(); // get item and convert to upper case
			listIterator.set(upperCase);
		}
	}
	
	// obtain sublist and use clear method to delete sublist items
	private static void removeItems(List<String> list, int start, int end)
	{
		list.subList(start, end).clear(); // remove items
	}
	
	// print reversed list
	private static void printReversedList(List<String> list)
	{
		ListIterator<String> listIterator = list.listIterator(list.size());
		
		System.out.printf("%nReversed List:%n");
		
		// print list in reverse order
		while (listIterator.hasPrevious())
		{
			System.out.printf("%s ", listIterator.previous());
		}
	}

} // end class ListTest
