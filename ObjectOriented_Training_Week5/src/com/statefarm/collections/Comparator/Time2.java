package com.statefarm.collections.Comparator;

// Time2.java
// Time2 class declaration with overloaded constructors
public class Time2 {

	private int hour;	// 0-23
	private int minute; // 0-59
	private int second; // 0-59
	
	// Time2 no-arg constructor
	// Initializes each instance variable to 0
	public Time2()
	{
		this(0, 0, 0); // invoke Time2 constructor with three arguments
	} // end Time2 no-arg constructor
	
	// Time2 constructor: hour supplied, minute and second defaulted to 0
	public Time2(int h)
	{
		this(h, 0, 0); // invoke Time2 constructor with three arguments
	} // end Time2 one-arg constructor
	
	// Time2 constructor: hour and minute supplied, second defaulted to 0
	public Time2(int h, int s)
	{
		this(h, s, 0); // invoke Time2 constructor with three arguments
	} // end Time2 two-arg constructor
	
	// Time2 constructor: hour and minute and second supplied
	public Time2(int hour, int minute, int second)
	{
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	} // end Time2 three-arg constructor

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
	
	@Override
	public String toString()
	{
		return String.format("%d:%d:%d %s", getHour(), getMinute(), getSecond(), ((getHour() < 12)? "AM" : "PM"));
	}
}
