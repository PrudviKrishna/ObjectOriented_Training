package com.statefarm.collections.Comparator;

import java.util.Comparator;

// TimeComparator.java
// Custom comparator class that compares two Time2 objects.
public class TimeComparator implements Comparator<Time2> {

	// compare returns negative integer if first element is less than second element and 0 if both are equal or a positive integer if first is grater than second
	@Override
	public int compare(Time2 time1, Time2 time2) {
		 int hourDifference = time1.getHour() - time2.getHour();
		 
		 if (hourDifference != 0) // test the hour first
		 {
			 return hourDifference;
		 }
		
		 int minuteDifference = time1.getMinute() - time2.getMinute();
		 
		 if (minuteDifference != 0) // If both hours are equal and it returns 0, then test the minutes
		 {
			 return minuteDifference;
		 }
		 
		 int secondDifference = time1.getSecond() - time2.getSecond();
		 return secondDifference;
	}

} // end class TimeComparator
