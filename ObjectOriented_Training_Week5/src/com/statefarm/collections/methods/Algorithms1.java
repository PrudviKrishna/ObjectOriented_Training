package com.statefarm.collections.methods;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// Algorithms1.java
// Collections methods reverse, fill, copy, max and min
public class Algorithms1 {

	public static void main(String[] args) {
		
		// create and display a List<Character>
		Character[] letters = {'P', 'C', 'M'};
		List<Character> list = Arrays.asList(letters); // get a list
		
		System.out.println("list contains: ");
		output(list);
		
		// reverse and display the List<Character>
		Collections.reverse(list);  // revrese order the elements
		System.out.printf("%nAfter calling revrese, list contains:%n");
		output(list);	
		
		// create copyList from an array of 3 characters
		Character[] lettersCopy = new Character[3];
		List<Character> copyList = Arrays.asList(lettersCopy);
		
		// copy the contents of list into copyList
		Collections.copy(copyList, list);
		System.out.printf("%nAfter copying, copyList contains:%n");
		output(copyList);
		
		// fill list with R's
		Collections.fill(list, 'R');
		System.out.printf("%nAfter filling, list contains:%n");
		output(list);
	}
	
	private static void output(List<Character> listRef)
	{
			System.out.print("The list is: ");
			
			for (Character c : listRef)
			{
				System.out.printf("%s ", c);
			}
			
			System.out.printf("%nMax: %s", Collections.max(listRef));
			System.out.printf(" Min: %s%n", Collections.min(listRef));
	}
} // end class Algorithm1
