package com.statefarm.collections.Set;

import java.util.Arrays;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

// SortedSetTest
// Using SortedSets and TreeSets
public class SortedSetTest {

	public static void main(String[] args) {
		
		String[] colors = {"yellow", "green", "black", "tan", "grey", "white", "orange", "red", "green"};
		SortedSet<String> tree = new TreeSet<>(Arrays.asList(colors));
		
		System.out.print("Sorted Set: ");
		printSet(tree);
		
		// get headSet based upon "orange"
		System.out.println("headSet (\"orange\"): ");
		printSet(tree.headSet("orange"));
		
		// get tailSet based upon "orange"
		System.out.println("tailSet (\"orange\"): ");
		printSet(tree.tailSet("orange"));
		
		// get first and last elements 
		System.out.printf("first: %s%n", tree.first());
		System.out.printf("last: %s%n", tree.last());

	}
	
	// output SortedSet using enhanced for statement
	private static void printSet(SortedSet<String> set)
	{
		for (String element : set)
		{
			System.out.printf("%s ", element);
		}
		
		System.out.println();
	}

} // end class SortedSetTest
