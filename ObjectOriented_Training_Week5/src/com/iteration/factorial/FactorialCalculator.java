package com.iteration.factorial;

public class FactorialCalculator {

	// recursive declaration  of method factorial
	public static long factorial(long number)
	{
		long result = 1;
		
		// iterative declaration of method factorial
		for (long i = number; i >= 1; i--)
		{
			result = result * i;
		}
		return result;
	}
	
	// output factorial for values 0-10
	public static void main(String[] args) {
		
		System.out.printf("%-10s %-10s%n", "Number", "Factorial");
		
		// calculate factorials of 0 through 10
		for (int counter = 0; counter <= 10; counter++)
		{
			System.out.printf("%-10d %-10d%n", counter, factorial(counter));
		}
	}
} // end class FactorialCalculator
