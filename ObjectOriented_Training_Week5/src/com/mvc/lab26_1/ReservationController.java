package com.mvc.lab26_1;

public class ReservationController {

	private ReservationDispatcher reservationDispatcher;
	
	public ReservationController()
	{
		reservationDispatcher = new ReservationDispatcher();
	}
	
	public void dispatchRequest(String request)
	{
		reservationDispatcher.dispatch(request);
	}
	
}
