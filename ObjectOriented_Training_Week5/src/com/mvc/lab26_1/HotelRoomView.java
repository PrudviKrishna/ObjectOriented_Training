package com.mvc.lab26_1;

public class HotelRoomView extends Reservable{

	public HotelRoomView(Reservation reservation) {
		super(reservation);
	}

	public void status()
	{
		System.out.println("Your selected room is reserved");
		getReservation().addReservation(this);
	}
}
