package com.mvc.lab26_1;

public abstract class Reservable {

	private Reservation reservation;
	public abstract void status();
	
	public Reservable(Reservation reservation) {
		this.reservation = reservation;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	
	

}
