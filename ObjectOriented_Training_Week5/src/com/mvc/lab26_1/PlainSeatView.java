package com.mvc.lab26_1;

public class PlainSeatView extends Reservable{

	public PlainSeatView(Reservation reservation) {
		super(reservation);
	}

	public void status()
	{
		System.out.println("Your selected seat is reserved");
		getReservation().addReservation(this);
	}
}
