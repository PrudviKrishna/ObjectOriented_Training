package com.mvc.lab26_1;

public class ReservationDispatcher {

	private RentalCarView rentalCarView;
	private HotelRoomView hotelRoomView;
	private PlainSeatView plainSeatView;
	private Reservation reservation;
	
	public ReservationDispatcher()
	{
		
	}
	public ReservationDispatcher(Reservation reservation)
	{
		this.reservation = reservation;
		rentalCarView = new RentalCarView(reservation);
		hotelRoomView = new HotelRoomView(reservation);
		plainSeatView = new PlainSeatView(reservation);
	}
	
	public void dispatch(String request)
	{
		if (request.equalsIgnoreCase("CAR"))
		{
			rentalCarView.status();
		} else if (request.equalsIgnoreCase("ROOM"))
		{
			hotelRoomView.status();
			
		} else if (request.equalsIgnoreCase("SEAT")) {
			plainSeatView.status();
		}
	}
}
