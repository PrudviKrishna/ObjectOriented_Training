package com.mvc.lab26_1;

public class RentalCarView extends Reservable{
	
	public RentalCarView(Reservation reservation) {
		super(reservation);
	}

	public void status()
	{
		System.out.println("Your selected car is reserved");
		getReservation().addReservation(this);
	}
}

