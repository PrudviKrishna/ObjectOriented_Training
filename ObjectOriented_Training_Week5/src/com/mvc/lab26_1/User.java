package com.mvc.lab26_1;

public class User {

	public static void main(String[] args) {
		
		Reservation reservation = new Reservation();
//		new HotelRoom(reservation);
//		new RentalCar(reservation);
//		new PlaneSeat(reservation);
		
//		System.out.println("Book Car reservation: ");
//		reservation.setRental("car");
//		System.out.println("Book Hotel Room reservation: ");
//		reservation.setRental("room");
//		System.out.println("Book Hotel Room reservation: ");
//		reservation.setRental("room");
		
		ReservationController controller = new ReservationController();
		controller.dispatchRequest("car");
		controller.dispatchRequest("room");
		controller.dispatchRequest("seat");

	}

}
