package com.lambdas.EmployeeStream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

// ProcessingEmployees.java
// processing streams of Employee objects
public class ProcessingEmployees {

	public static void main(String[] args) {
		
		// initialize array of elements
		Employee[] employees = {new Employee("Jason", "Red", 5000, "IT"), 
								new Employee("Ashley", "Green", 7600, "IT"),
								new Employee("Matthew", "Indigo", 3587.5, "Sales"),
								new Employee("Jason", "Indigo", 4700.77, "Marketing"),
								new Employee("Luke", "Indigo", 6200, "IT"),
								new Employee("Jason", "Blue", 3200, "Sales"),
								new Employee("Wendy", "Brown", 4236.4, "Marketing")};
		
		// get List view of the Employees
		List<Employee> list = Arrays.asList(employees);
		
		// display all employees
		System.out.println("Complete Employee list:");
		list.stream().forEach(System.out :: println);
		
		// Predicate that returns true for salaries in the range $4000 - $6000
		Predicate<Employee> fourToSixThousand = e -> (e.getSalary() >= 4000 && e.getSalary() <= 6000);
		
		// Display employees with salaries in the range $4000 - $6000
		// sorted into ascending order by salary
		System.out.printf("%nEmployees earning $4000-$6000 per month sorted by salary:%n");
				list.stream().
				filter(fourToSixThousand)
				.sorted(Comparator.comparing(Employee :: getSalary))
				.forEach(System.out :: println);
		
		// display first employee with salary in the range $4000 - $6000
		System.out.printf("%nFirat Employee who earns $4000-$6000:%n%s%n", list.stream().filter(fourToSixThousand).findFirst().get());
		
		// functions for getting first and last names from an employee
		Function<Employee, String> byFirstName = Employee :: getFirstName;
		Function<Employee, String> byLastName = Employee :: getLastName;
		
		// COmparator for comparing Employees by first and last names 
		Comparator<Employee> lastThenFirst = Comparator.comparing(byLastName).thenComparing(byFirstName);
		
		// sort employees by last name then first name
		System.out.printf("%nEmployees in ascending order by last name then first:%n");
		list.stream().sorted(lastThenFirst).forEach(System.out :: println);
		//list.stream().sorted(Comparator.comparing(Employee :: getLastName).thenComparing(Employee :: getFirstName)).forEach(System.out :: println);
		
		// sort employees in descending order by last name, then first name
		System.out.printf("%nEmployees in descending order by last name then first name:%n");
		//list.stream().sorted(Comparator.comparing(byLastName).thenComparing(byFirstName).reversed()).forEach(System.out :: println);
		list.stream().sorted(lastThenFirst.reversed()).forEach(System.out :: println);
		
		// display unique employee last names sorted
		System.out.printf("%nUnique employees last names:%n");
		list.stream().map(Employee :: getLastName).distinct().sorted().forEach(System.out::println);
		
		// display only first and last names
		System.out.printf("%nEmployee names in order by last name then first name:%n");
		list.stream().sorted(lastThenFirst).map(Employee::getName).forEach(System.out::println);
		
		// group employees by department
		System.out.printf("%nEmployees by department:%n");
		Map<String, List<Employee>> groupedByDepartment = list.stream().collect(Collectors.groupingBy(Employee::getDepartment));
		
		groupedByDepartment.forEach((department, employeesInDepartment) -> 
		{
			System.out.println(department);
			employeesInDepartment.forEach(employee -> System.out.printf(" %s%n", employee));
		});
		
		// count number of employees in each department
		System.out.printf("%nCount of employees by department:%n");
		Map<String, Long> employeeCountByDepartment = list.stream().collect(Collectors.groupingBy(Employee :: getDepartment, Collectors.counting()));
		
		employeeCountByDepartment.forEach(	
		(department, count ) -> {
			System.out.printf("%s has %d employees%n", department, count);
		});
		
		
		

		
		
		

	}

}
