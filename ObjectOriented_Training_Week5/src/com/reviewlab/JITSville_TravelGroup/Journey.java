package com.reviewlab.JITSville_TravelGroup;

import java.util.ArrayList;
import java.util.List;

public class Journey {

	List<Passenger> passengers = new ArrayList<>();
	
	public static double getTotalCost(List<Passenger> passengers)
	{
		double totalCost = 0;
		for (Passenger passenger : passengers)
		{
			totalCost += passenger.getCost();
		}
		return totalCost;
	}
	
	
	public static int getTotalNewspapers(List<Passenger> passengers)
	{
		int totalNewspapers = 0;
		for (Passenger passenger : passengers)
		{
			if (passenger.isNewspaper())
			{
				totalNewspapers += 1;
			}
		}
		
		return totalNewspapers;
	}
	
	public static int getTotalMeals(List<Passenger> passengers)
	{
		int totalMeals = 0;
		for (Passenger passenger : passengers)
		{
			totalMeals += ((VacationPassenger) passenger).getMeals();
		}
		return totalMeals;
	}
}
