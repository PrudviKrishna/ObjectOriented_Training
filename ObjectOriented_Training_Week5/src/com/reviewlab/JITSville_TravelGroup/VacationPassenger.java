package com.reviewlab.JITSville_TravelGroup;

// VacationPassenger.java
// Vacation Passenger is a Passenger
public class VacationPassenger extends Passenger{

	private int numMiles;
	
	public VacationPassenger(int numMiles, boolean newspaper) {
		super(newspaper);
		if (numMiles < 5 || numMiles > 4000)
		{
			throw new IllegalArgumentException("Vacation Passenger cannot travel less than 5 miles and more than 4000 miles");
		} else {
			this.numMiles = numMiles;
		}
	}
	
	public int getNumMiles() {
		return numMiles;
	}
	
	public void setNumMiles(int numMiles) {
		if (numMiles < 5 || numMiles > 4000)
		{
			throw new IllegalArgumentException("Vacation Passenger cannot travel less than 5 miles and more than 4000 miles");
		} else {
			this.numMiles = numMiles;
		}
	}

	@Override
	public double getCost() {
		return getRateFactor() * getNumMiles();	// cost is Rate factor times number of miles
	}
	
	public int getMeals()
	{
		double meals = getNumMiles()/100; // one meal for each 100 miles
		return (int) Math.round(meals);
	}

}
