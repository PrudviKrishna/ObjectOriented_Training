package com.reviewlab.JITSville_TravelGroup;

// Commuter.java
// Commuter is a Passenger
public class Commuter extends Passenger {

	private boolean riderCard = false; // do passenger have rider card
	private int numStops;
	
	public Commuter(int numStops, boolean newspaper, boolean riderCard) {
		super(newspaper);
		this.riderCard = riderCard;
	}

	public int getNumStops() {
		return numStops;
	}

	public void setNumStops(int numStops) {
		this.numStops = numStops;
	}

	@Override
	public double getCost() {
		
		if (isRiderCard())
		{
			double cost = getRateFactor() * getNumStops();
			return cost - 0.1 * (cost);
		}
		return getRateFactor() * getNumStops();
	}

	public boolean isRiderCard() {
		return riderCard;
	}

	public void setRiderCard(boolean riderCard) {
		this.riderCard = riderCard;
	}
}
