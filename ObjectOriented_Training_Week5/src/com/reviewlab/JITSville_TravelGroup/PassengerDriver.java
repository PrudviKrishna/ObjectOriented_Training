package com.reviewlab.JITSville_TravelGroup;

import java.util.ArrayList;
import java.util.List;

public class PassengerDriver {

	public static void main(String[] args) {
	
		List<Passenger> passengers = new ArrayList<>();
		
		passengers.add(new Commuter(3, false, true));
		passengers.add(new Commuter(5, false, true));
		passengers.add(new Commuter(4, true, false));
		
		passengers.add(new VacationPassenger(90, false));
		passengers.add(new VacationPassenger(199, true));
		
		double totalCost = 0;
		int totalMeals = 0;
		int totalNewspapers = 0;
		for (Passenger passenger : passengers)
		{
			totalCost += passenger.getCost();
			totalMeals += ((VacationPassenger) passenger).getMeals();
			
			if (passenger.isNewspaper())
			{
				totalNewspapers += 1;
			}
		}
		
		System.out.printf("Total cost of the journey is : %.2f%n", totalCost);
		System.out.printf("Total number of meals needed for all passengers in the journey: %d%n", totalMeals);
		System.out.printf("Total newspaperss needed for all passengers in the journey: %d%n", totalNewspapers);

	}

}
