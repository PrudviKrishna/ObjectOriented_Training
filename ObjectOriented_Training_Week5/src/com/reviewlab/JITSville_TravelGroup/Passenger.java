package com.reviewlab.JITSville_TravelGroup;

// Passenger.java
public abstract class Passenger {

	private static final double RATE_FACTOR = 0.5; // rate factor to calculate cost
	private boolean newspaper = false; // do passenger requested newspaper?
	
	// constructor
	public Passenger(boolean newspaper) {
		this.newspaper = newspaper;
	}

	public boolean isNewspaper() {
		return newspaper;
	}

	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}

	public static double getRateFactor() {
		return RATE_FACTOR;
	}
	
	public abstract double getCost();

}
