package com.customexceptions.lab17_1;

public class ReservationException extends Exception {

	public ReservationException() {
	
	}

	public ReservationException(String message) {
		super(message);
	}

	public ReservationException(Throwable cause) {
		super(cause);
	}

	public ReservationException(String message, Throwable cause) {
		super(message, cause);
	}

//	public ReservationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
//		super(arg0, arg1, arg2, arg3);
//	}

}
