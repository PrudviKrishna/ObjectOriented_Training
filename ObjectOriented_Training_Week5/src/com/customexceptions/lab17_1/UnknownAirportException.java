package com.customexceptions.lab17_1;

public class UnknownAirportException extends Exception {

	public UnknownAirportException() {
		
	}

	public UnknownAirportException(String message) {
		super(message);
	}

	public UnknownAirportException(Throwable cause) {
		super(cause);
	}

	public UnknownAirportException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownAirportException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
