package com.generic_classesAnd_methods.genericclasses;

// EmptyStackException.java
// EmptyStackException class declaration
public class EmptyStackException extends RuntimeException{

	// nor-arg constructor
	public EmptyStackException()
	{
		this("Stack is empty");
	}
	
	// one-arg constructor
	public EmptyStackException(String message)
	{
		super(message);
	}
} // end class EmptyStackException
