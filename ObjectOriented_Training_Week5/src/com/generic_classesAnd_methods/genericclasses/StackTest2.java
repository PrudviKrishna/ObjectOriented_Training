package com.generic_classesAnd_methods.genericclasses;

// StackTest2.java
// Passing generic Stack Objects to generic methods
public class StackTest2 {

	public static void main(String[] args) {
	
		Double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
		Integer[] integerElements = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		// create Stack<Double> and Stack<Integer>
		Stack<Double> doubleStack = new Stack<>();
		Stack<Integer> integerStack = new Stack<>();
		
		// push elements of doubleElements onto doubleStack
		testPush("doubleStack", doubleStack, doubleElements);
		testPop("doubleStack", doubleStack);	// pop from doubleStack
		
		// push elements of integerElements onto integerStack
		testPush("integerStack", integerStack, integerElements);
		testPop("integerStack", integerStack);	// pop from doubleStack
	}
	
	// generic method testPush pushes elements onto a Stack
	public static <T> void testPush(String name, Stack<T> stack, T[] elements)
	{
		System.out.printf("%nPushing elements onto %s%n", name);
		
		// push elements to Stack
		for (T value : elements)
		{
			System.out.printf("%.1f ", value);
			stack.push(value);	// push element on stack
		}
	}
	
	// generic method testPop removes elements from a Stack
	public static <T> void testPop(String name, Stack<T> stack)
	{
		// pop elements from Stack
		try
		{
			System.out.printf("%nPopping elements from %s%n", name);
			T popValue;	// store element removed from stack
			
			// remove all elements from Stack
			while(true)
		{
				popValue = stack.pop();
				System.out.printf("%s ", popValue);
		}	
		} catch (EmptyStackException emptyStackException)
		{
			System.err.println();
			emptyStackException.printStackTrace();
		}
	}

}	// end class StackTest2
