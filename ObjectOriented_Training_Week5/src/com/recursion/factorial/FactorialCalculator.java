package com.recursion.factorial;

// FactorialCalculator.java
// Recursive factorial method
public class FactorialCalculator {

	// output factorials for values 0-21
	public static void main(String[] args) {
		
		System.out.printf("%-10s %-10s%n", "Number", "Factorial");
		
		// calculate factorials of 0 through 21
		for (int counter = 0; counter <= 21; counter++)
		{
			System.out.printf("%-10d %-10d%n", counter, factorial(counter));
		}

	}
	
	// recursive method factorial (assumes its parameter is >= 0)
	public static long factorial(long number)
	{
		if (number <= 0) // test for base case
		{
			return 1;  // base cases: 0! = 1 and 1! = 1
		}
		else {  // recursion step
		return number * factorial(number - 1);
		}
	}

}
