package com.dzone.observer;

import java.util.ArrayList;
import java.util.List;



public class RedDress implements Observable{

	// List of users registered for notification
	private List<Observer> users = new ArrayList<>();
	// state of dress
	private boolean inStock = true;

	// getter for inStock
	public boolean isInStock() {
		return inStock;
	}

	// setter for inStock
	public void setInStock(boolean inStock) {
		this.inStock = inStock;
		notifyObserver();  // value of inStock variable is changed from here. Whenever it is changed notifyObserver() is called to inform all the Observers  about the change.
	}

	@Override
	public void notifyObserver() {
		// notify all the users
		for (Observer user : users)
		{
			user.update();
		}
		
	}

	@Override
	public void addObserver(Observer o) {
		users.add(o);
		
	}

	@Override
	public void removeObserver(Observer ob) {
		users.remove(ob);
		
	}

	
}
