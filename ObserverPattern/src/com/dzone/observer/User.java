package com.dzone.observer;

public class User implements Observer{

	private Observable observable;
	
	public User(Observable observable)
	{
		this.observable = observable;
	}
	
	@Override
	public void update()
	{
		buyDress();  // When the status of RedDress changes the user buyDress
		unSubscribe(); // After buying dress user is not interested in the status, so it calls unsubscribe.
	}
	
	public void buyDress()
	{
		System.out.println("Got my new Red Dress");
	}
	
	public void unSubscribe()
	{
		observable.removeObserver(this); // calls removeObserver() of Observable to stop further notifications
	}
}
