package com.dzone.observer;

public class ObservableDemo {

	public static void main(String[] args) {
		
		RedDress subject = new RedDress();
		Observer observer = new User(subject);
		
		subject.addObserver(observer);
		subject.setInStock(true);

	}

}
