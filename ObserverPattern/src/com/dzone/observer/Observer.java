package com.dzone.observer;

public interface Observer {

	// to be called by Observable
	public void update();
}
