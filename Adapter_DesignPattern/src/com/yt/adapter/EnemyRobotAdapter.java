package com.yt.adapter;

public class EnemyRobotAdapter implements EnemyAttacker {

	public EnemyRobot robot;
	
	public EnemyRobotAdapter(EnemyRobot newRobot)
	{
		this.robot = newRobot;
	}
	
	@Override
	public void fireWeapon() {
		
		robot.smashWithHands();
	}

	@Override
	public void driveForward() {
		
		robot.walkForward();
	}

	@Override
	public void assignDriver(String driverName) {
	
		robot.reactToHuman(driverName);
	}

}
