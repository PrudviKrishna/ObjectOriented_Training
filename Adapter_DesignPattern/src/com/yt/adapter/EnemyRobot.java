package com.yt.adapter;

import java.util.Random;

public class EnemyRobot {

	Random generator = new Random();
	
	public void smashWithHands()
	{
		int attackDamage = generator.nextInt(10) + 1;
		System.out.println("Enemy tank does " + attackDamage + " Damage with its hands" );
	}
	
	public void walkForward()
	{
		int movement = generator.nextInt(5) + 1;
		System.out.println("Enemy tank moves " + movement + " spaces" );
	}
	
	public void reactToHuman(String driverName)
	{
		System.out.println("Enemy robot Tramps on " + driverName );
	}
}

