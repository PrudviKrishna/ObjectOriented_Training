package com.yt.adapter;

public class TestEnemyAttacker {

	public static void main(String[] args) {
		EnemyTank rx7Tank = new EnemyTank();
		EnemyRobot fredTheRobot = new EnemyRobot();
		EnemyAttacker robotAdapter = new EnemyRobotAdapter(fredTheRobot);
		
		System.out.println("The Robot");
		fredTheRobot.reactToHuman("Optimus Prime");
		fredTheRobot.walkForward();
		fredTheRobot.smashWithHands();
		
		System.out.println("---------------------------");
		
		System.out.println("The Enemy Tank");
		rx7Tank.assignDriver("Mega Man");
		rx7Tank.driveForward();
		rx7Tank.fireWeapon();
		
		System.out.println("---------------------------");
		
		System.out.println("the Robot with Adapter");
		robotAdapter.assignDriver("Megatron");
		robotAdapter.driveForward();
		robotAdapter.fireWeapon();

	}

}
