import java.util.Scanner;

// Invitation.java
// Class that takes your work time off, checks your availability and invites for dinner!

public class Invitation {

	public static void main(String[] args) {
		
		String[] invitees = {"Jill", "Dillon", "Patrick", "Zach", "Mitchell"};
	    //String[] menu = {"Chicken Biryani", "Samosa", "Dosa", "Mango Lassi (Dessert)"};
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter - till what time you work tomorrow (Wednesday): "); // prompt for time
		String time = input.next(); // reads the user input time and stores in variable time.

		int d = Integer.parseInt(time.substring(0, 1));
		
		if (d >= 5 )
		{
			System.out.printf("%nYou are invited for the dinner tomorrow at Signature India Restuarant at 5.30 P.M!%n");

		}
		else {
			System.out.printf("%nAre you busy tomorrow evening at 5:30 PM? Enter yes or no: ");
		    String available = input.next();
		    
		    if (available.equals("no")) {
		    	System.out.printf("%nI'm planning to have a dinner. I'm inviting you to join us tomorrow at Signature India Restaurant at 5:30 P.M! Some Indian food :)");
		  
		    	System.out.printf("%n%n%s%n", "Invitees:-");
		    	for (String invitee : invitees)
		    	{
		    		System.out.println(invitee);
		    	}
		    }
		    else
		    {
		    	System.out.printf("%nNothing much! Have a great evening!");
		    }
			
	}}

}
