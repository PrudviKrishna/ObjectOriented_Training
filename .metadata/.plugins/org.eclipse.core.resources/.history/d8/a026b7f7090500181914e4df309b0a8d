/**
 * 
 */
package com.jits.shipping;

import java.util.Calendar;

/**
 * @author W1WG
 * This class contains the properties of Barcode
 */
public class Barcode {

	// attributes
	private int packageId;              // Unique identifier of a package
	private String shipMethod;  //= {"GRD", "AIR", "RAL"}; // delivery method i.e. ground, air and rail
	private String fromZip;     // from zip code
	private String toZip;		// to zip code
	private double weight;		// weight of the package in ounces	
	private int height;			// height of the package in inches
	private int width;			// width of the package in inches
	private int depth;			// depth of the package in inches
	private String hazards;		// if any hazardous materials in package
	
	// constructor
	public Barcode(int packageId, String shipMethod, String fromZip, String toZip, double weight, int height,
			int width, int depth, String hazards) {
		
		int timeStamp =  Calendar.getInstance().get(Calendar.MILLISECOND);
		this.packageId = packageId + timeStamp;
		
		this.shipMethod = shipMethod;
		this.fromZip = fromZip;
		this.toZip = toZip;
		this.weight = weight;
		this.height = height;
		this.width = width;
		this.depth = depth;
		this.hazards = hazards;
	}

	// get package id
	public int getPackageId() {
		return packageId;
	}

	// set package id
	public void setPackageId(int packageId) {
		
		int timeStamp =  Calendar.getInstance().get(Calendar.MILLISECOND);
		this.packageId = packageId + timeStamp;
	}

	// get shipment method
	public String getShipMethod() {
		return shipMethod;
	}

	// set shipment method
	public void setShipMethod(String shipMethod) {
		if(shipMethod.length() == 3 && (shipMethod == ("GRD" || "AIR" || "RAL")))
		{
			this.shipMethod = shipMethod;
		}
		else throw new IllegalArgumentException("Enter the valid shipment method; 'GRD' for Ground, 'AIR' for air and 'RAL' for rail method");
	}

	// get package from zip code
	public String getFromZip() {
		return fromZip;
	}

	// set package from zip code
	public void setFromZip(String fromZip) {
		this.fromZip = fromZip;
	}

	// get package to zip code
	public String getToZip() {
		return toZip;
	}

	// set package to zip code
	public void setToZip(String toZip) {
		this.toZip = toZip;
	}

	// get weight of the package
	public double getWeight() {
		return weight;
	}

	// set weight of the package
	public void setWeight(double weight) {
		this.weight = weight;
	}

	// get height of the package
	public int getHeight() {
		return height;
	}

	// set height of the package
	public void setHeight(int height) {
		this.height = height;
	}

	// get width of the package
	public int getWidth() {
		return width;
	}

	// set width of the package
	public void setWidth(int width) {
		this.width = width;
	}

	// get depth of the package
	public int getDepth() {
		return depth;
	}

	// set depth of the package
	public void setDepth(int depth) {
		this.depth = depth;
	}

	// get any hazardous material of the package
	public String getHazards() {
		return hazards;
	}

	// set any hazardous material in the package
	public void setHazards(String hazards) {
		this.hazards = hazards;
	}
	
	// String representation of the Barcode
	@Override
	public String toString()
	{
		return String.format("%2d|%s|%s|%s|%.2f|%d|%d|%d|%s%n", getPackageId(), getShipMethod(), getFromZip(), getToZip(),
				getWeight(), getHeight(), getWidth(), getDepth(), getHazards());
	}
	
	
	
	
	
}
