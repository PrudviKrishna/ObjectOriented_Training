package com.generic_classesAnd_methods.genericclasses;

//StackTest.java
// Stack generic class test program
public class StackTest {

	public static void main(String[] args) {
		
		double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
		int[] integerElements = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		// create Stack<Double> and Stack<Integer>
		Stack<Double> doubleStack = new Stack<>();
		Stack<Integer> integerStack = new Stack<>();
		
		// push elements of doubleElements onto doubleStack
		testPushDouble(doubleStack, doubleElements);
		testPopDouble(doubleStack);	// pop from doubleStack
		
		// push elements of integerElements onto integerStack
		testPushInteger(integerStack, integerElements);
		testPopInteger(integerStack);	// pop from doubleStack
	}
	
	// test push method with double stack
	private static void testPushDouble(Stack<Double> doubleStack, double[] doubleElements)
	{
		System.out.printf("%nPushing elements onto doubleStack%n");
		
		// push elements to Stack
		for (double value : doubleElements)
		{
			System.out.printf("%.1f", value);
			doubleStack.push(value);
		}
	}
	
	// test pop method with doubleStack
	private static void testPopDouble(Stack<Double> doubleStack)
	{
		// pop elements from Stack
		try
		{
		System.out.printf("%nPopping elements from doubleStack%n");
		double popValue;	// store element removed from stack
		
		// remove all elements from Stack
		while (true)
		{
		popValue = doubleStack.pop();	// pop from doubleStack
		System.out.printf("%.1f", popValue);
		}
		} catch(EmptyStackException emptyStackException)
		{
			System.err.println();
			emptyStackException.printStackTrace();
		}
	}
	
	// test push method with integer stack
	private static void testPushInteger(Stack<Integer> integerStack, int[] integerElements)
	{	
		System.out.printf("%nPushing elements onto integerStack%n");
		
		// push elements to stack
		for (int element : integerElements)
		{
			System.out.printf("%.1f ", element);
			integerStack.push(element);	// push onto integer stack
		}
	}
	
	// test pop method with integer stack
	private static void testPopInteger(Stack<Integer> integerStack)
	{
		// pop elements from Stack
		try
		{
			System.out.printf("%nPopping elements from integerStack%n");
			int popValue;	// store element removed from stack
			
			// remove all elements from Stack
			while(true)
		{
				popValue = integerStack.pop();
				System.out.printf("%d", popValue);
		}	
		} catch (EmptyStackException emptyStackException)
		{
			System.err.println();
			emptyStackException.printStackTrace();
		}
	}
}	// end class StackTest
