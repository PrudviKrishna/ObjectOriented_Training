package com.statefarm.collections.shuffle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// DeckOfCards.java
// Card shuffling and dealing with Collections method Shuffle

class Card {
	
	public static enum Face {
		Ace, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
	};
	public static enum Suit {
		Clubs, Diamonds, Hearts, Spades
	};
	
	private final Face face;
	private final Suit suit;
	
	// constructor
	public Card(Face face, Suit suit) {
		this.face = face;
		this.suit = suit;
	}

	// returns face of the card
	public Face getFace() {
		return face;
	}

	// returns suit of the card
	public Suit getSuit() {
		return suit;
	}
	
	// return string representation of card
	public String toString()
	{
		return String.format("%s of %s", face, suit);
	}
} // end class Card

public class DeckOfCards {

	private List<Card> list; // declare list that will store cards
	
	// set up deck of cards and shuffle
	public DeckOfCards()
	{
		Card[] deck = new Card[52];
		int count = 0;
		
		for (Card.Face face : Card.Face.values())
		{
			for (Card.Suit suit : Card.Suit.values())
			{
				deck[count] = new Card(face, suit);
				++count;
			}
		}
		
		list = Arrays.asList(deck); // get list
		Collections.shuffle(list); // shuffle deck
	} // end DeckOfCards constructor
	
	// output deck
	public void printCards()
	{
		for (int i=0; i < list.size(); i++)
		{
			System.out.printf("%-19s%d", list.get(i), ((i+1) % 4 == 0) ? "%n" : "" );
		}
	}
	
	public static void main(String[] args)
	{
		DeckOfCards cards = new DeckOfCards();
		cards.printCards();
	}
	
}