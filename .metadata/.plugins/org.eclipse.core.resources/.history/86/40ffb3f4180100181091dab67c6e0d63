package com.statefarm.chapter9_Exercises;

public class HourlyEmployee extends Employee {

	private double hours;   // hours worked
	private double wage;	// wages per hour
	
	// constructor
	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double hours, double wage) {
		super(firstName, lastName, socialSecurityNumber);
		
		if (hours < 0 || hours > 168)
		{
			throw new IllegalArgumentException("Hours worked must be > 0 and less than 168");
		}
		if (wage < 0)
		{
			throw new IllegalArgumentException("Wage must be > 0");
		}
		this.hours = hours;
		this.wage = wage;
	}

	// get hours worked
	public double getHours() {
		return hours;
	}

	//  set number of hours 
	public void setHours(double hours) {
		if (hours < 0 || hours > 168)
		{
			throw new IllegalArgumentException("Hours worked must be > 0 and less than 168");
		}
			
		this.hours = hours;
	}

	// get wage for hours worked
	public double getWage() {
		return wage;
	}

	// set wage for hours
	public void setWage(double wage) {
		if (wage < 0)
		{
			throw new IllegalArgumentException("Wage must be > 0");
		}
		this.wage = wage;
	}
	
	public double getEarnings()
	{
		return getHours() * getWage();
	}
	
	@Override
	public String toString()
	{
		return String.format("%s: %s%n%s: %s%n%s: %s%n%s: %.2f%n%s: %.2f%n%s: %$,.2f%n", "First Name", getFirstName(), "Last Name", getLastName(), 
				"SocialSecurityNumber", getSocialSecurityNumber(), "Hours Worked", getHours(), "Wage", getWage(), "Earnings", getEarnings());
	}
	
}
