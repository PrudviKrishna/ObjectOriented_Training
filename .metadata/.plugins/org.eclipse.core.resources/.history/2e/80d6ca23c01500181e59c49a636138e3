package com.jits.kiosk.desktop;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jits.audit.CostAudit;
import com.jits.core.Address;
import com.jits.core.Box;
import com.jits.core.HazardousDelivery;
import com.jits.core.Letter;
import com.jits.core.Parcel;
import com.jits.core.ShipmentLogging;
import com.jits.transfer.DeliveryRequest;
import com.jits.transfer.IConfirmation;

public class KioskRequestHandler {

	private static final Logger LOG = Logger.getLogger(KioskRequestHandler.class);
	
	private IConfirmation deliveryRequest;
	/**
	 * Called from UI after user has entered all data. UI will display the
	 * returned IConfirmation data via IConfirmation.toString() and prompt user
	 * for whether to proceed or cancel the shipment.
	 */
	public IConfirmation handleRequest(Map request) {
		
		
		// STUDENTS - INSERT CODE HERE
		Parcel parcel = null;		// creating parcel object
		
		LOG.debug("Fetching user entered (UI) parcel type from request map and giving it to parcel object.");
		String type = (String) request.get("type");		// example LA | LG | BA | BG
		String shipment = type.substring(1);
		String parcelType = type.substring(0, 1);
		
		if (parcelType.equals("B"))
		{
			LOG.debug("Instantiating a Box object if user selected Box type in UI");
			parcel = new Box();
			LOG.debug("Fetching parcel is insured from request map and giving it to parcel object.");
			String response = (String) request.get("insured");
			((Box) parcel).setInsured(response);
		} else if (parcelType.equals("L"))
		{
			LOG.debug("Instantiating a Letter object if user selected Letter type in UI");
			parcel = new Letter();
		}
		parcel.setShipment(shipment);
		parcel.setType(parcelType);
		
		String fromName  = (String) request.get("fromName");
		String fromState = (String) request.get("fromState");
		String fromZip	  = (String) request.get("fromZip");
		String fromStreet = (String) request.get("fromStreet");
		String fromCity  = (String) request.get("fromCity");
		LOG.debug("Fetching user entered (UI) origin address from request map and giving it to parcel object.");
		Address origin = new Address(fromName, fromStreet, fromCity, fromState, fromZip);
		parcel.setOrigin(origin);	// adding origin address to parcel
		
		String toName  = (String) request.get("toName");
		String toState = (String) request.get("toState");
		String toZip	  = (String) request.get("toZip");
		String toStreet = (String) request.get("toStreet");
		String toCity  = (String) request.get("toCity");
		LOG.debug("Fetching user entered (UI) destination address from request map and giving it to parcel object.");
		Address destination = new Address(toName, toState, toZip, toStreet, toCity);
		parcel.setDestination(destination);		// adding destination address to the parcel
		
		String id =  (String) request.get("id");
		LOG.debug("Fetching parcel Id from request map and giving it to parcel object.");
		parcel.setParcelId(id);
				
		String width = (String) request.get("width");
		LOG.debug("Fetching user entered (UI) parcel width from request map and giving it to parcel object.");
		parcel.setWidth(width);
		String height = (String) request.get("height");
		LOG.debug("Fetching user entered (UI)  parcel width from request map and giving it to parcel object.");
		parcel.setHeight(height);
		String depth = (String) request.get("depth");
		LOG.debug("Fetching user entered (UI) parcel depth from request map and giving it to parcel object.");
		parcel.setDepth(depth);
		
		LOG.debug("Instantiating DeliveryRequest object which is of type IConfirmation");
		deliveryRequest = new DeliveryRequest(parcel);
	    return deliveryRequest;
	}	

	/**
	 * Called from UI after user has chosen to proceed or cancel the shipment.
	 * UI will display the returned IConfirmation data via
	 * IConfirmation.toString(). After this call returns, shipment is now done
	 * or has been cancelled.
	 */
	public IConfirmation handleUserDecision(boolean proceedWithShipment) {
		 
		// STUDENTS - INSERT CODE HERE
		
		if (proceedWithShipment)	// If true
		{	
			// Calling costAuditing method from CostAudit class to audit delivery costs in the format {Id cost timestamp}
			String id = ((DeliveryRequest) deliveryRequest).getId();
			double cost = deliveryRequest.getCost();
			
			LOG.debug("Auditing the Shipping cost to the file; if user confirms the delivery");
			CostAudit.costAuditing(id, cost);
			
			// Parcel is scanned for Hazards and precautions are applied
			Parcel parcel = ((DeliveryRequest)deliveryRequest).getParcel();
			List<String> hazards = HazardousDelivery.getHarzards(parcel);
			for (String hazard : hazards)
			{
				System.out.println(hazard);
				LOG.debug("Adding precautions to the parcel if it contains Hazardous items");
				HazardousDelivery.addPrecaution(hazard);
			}
			
			DeliveryRequest deliveryRequest1 = new DeliveryRequest(parcel);
			ShipmentLogging.shipmentLogging(deliveryRequest1);
			
			return deliveryRequest;
		}
	    
		return null;
	}
}