package com.statefarm.chapter7;

// GradeBook class using an array to store test grades
public class GradeBook {

	private String courseName; // name of course this GradeBook represents
	private int[] grades; // array of student grades
	
	// constructor
	public GradeBook(String name, int[] grades)
	{
		this.courseName = courseName;
		this.grades = grades;
	}
	
	public void setCourseName(String name)
	{
		this.courseName = courseName;
	}
	
	public String getCourseName()
	{
		return courseName;
	}
	
	// perform various operations on data
	public void processGrades()
			{
		        // output grades array
				outputGrades();
		
		// call method getAverage to calculate the average grade
		System.out.printf("%nClass average is %.2f%n", getAverage());
		
		// call methods getMinimum and getMaximum
		System.out.printf("%nLowest grade is %d%nHighest grade is %d%n%n", getMinimum(), getMaximum());
		
		// call outputBarChart to print grade distribution chart
		outputBarChart();
			}
	
	// find minimum grade
	public int getMinimum()
	{
		int minimum = grades[0]; // assume lowest grade is grade[0]
		
		// loop through grades array
		for (int grade : grades)
		{
			// if grade lower than assumed minimum then assign it to minimum
			if (grade < minimum)
			{
				minimum = grade; // newest minimum grade
			}
			
		}
		
		return minimum;
	}
	

	// find maximum value
	public int getMaximum()
	{
		int maximum = grades[0]; // assume highest grade is grade[0]
		
		// loop through grades array
		for(int grade : grades)
		{
			// if grade is higher than assumed maximum then assign it to maximum
			if (grade > maximum)
			{
				maximum = grade; // newest maximum grade
			}
		}
		
		return maximum;
	}
	
	// determine average grade for test	
	public double getAverage()
	{
		int total = 0;
		
		// sum grades for one student
		for (int grade : grades)
		{
			total += grade;
		}
		
		// return average of grades
		return (double) total / grades.length;
	}
	
	// output bar chart displaying grade distribution
	public void outputBarChart()
	{
		System.out.println("Grade distribution:");
		
		// stores frequency of grades in each range of 10 grades
		int[] frequency = new int[11];
		
		// for each grade, increment the appropriate frequency
		for (int grade : grades)
		{
			++frequency[grade / 10];
		}
		
		// for each grade frequency, print bar in chart
		for (int count = 0; count < frequency.length; count++)
		{
			// output bar label ("00-09: ", ..., "90-99: ", "100: ")
			if (count == 10)
			System.out.printf("%5d: ", 100);
			else
				System.out.printf("%02d-%02d: ", count * 10, (count * 10) + 9);
			
			for (int star = 0; star < frequency[count]; star++)
			{
				System.out.print("*");	
			}
			
			System.out.println();
			
		}
		
	}
	
	// output the contents of grades array
	public void outputGrades()
	{
		System.out.printf("The grades are: %n%n");
		
		// output each student's grades
		for (int student = 0; student < grades.length; student++)
		{
			System.out.printf("%nStudent %2d: %3d%n", student + 1, grades[student]);
		}
	}
	
	
}
