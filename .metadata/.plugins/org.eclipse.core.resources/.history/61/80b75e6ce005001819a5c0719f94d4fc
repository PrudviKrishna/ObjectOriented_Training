/**
 * 
 */
package com.jits.shipping;

import java.util.Calendar;

/**
 * @author W1WG
 * This class contains the properties of Barcode
 */
public class Barcode {

	// attributes
	private int packageId;      // Unique identifier of a package
	private String shipMethod;  // delivery method i.e. ground, air or rail
	private String fromZip;     // from zip code
	private String toZip;		// to zip code
	private double weight;		// weight of the package in ounces	
	private int height;			// height of the package in inches
	private int width;			// width of the package in inches
	private int depth;			// depth of the package in inches
	private String hazards;		// if any hazardous materials in package
	
	// constructor
	public Barcode(int packageId, String shipMethod, String fromZip, String toZip, double weight, int height,
			int width, int depth, String hazards) { // throws InvalidBarcodeException {
		
		int timeStamp =  Calendar.getInstance().get(Calendar.MILLISECOND);
		// validate packageId
		if(packageId < 0 || packageId > 99999)
		{
			throw new IllegalArgumentException("Package Id must be between 1 and 99999");
		}
		
		else this.packageId = Integer.parseInt(String.valueOf(packageId) + String.valueOf(timeStamp));
		
	//	System.out.printf("%s%n%s%n%s%n%s%n", "Enter the Shipment method: ", "Enter 1 if shipment is through Ground", 
	//			"Enter 2 if shipment is through Air", "Enter 3 if shipment is through Rail");
			switch(shipMethod)
			{
			case "1":
				this.shipMethod = "GRD";
				break;
			case "2":
				this.shipMethod = "AIR";
				break;
			case "3":
				this.shipMethod = "RAL";
			}
		
			// validate fromZip code
			if (fromZip.length() == 5)
			{
				this.fromZip = fromZip;
			}
			else {
				throw new IllegalArgumentException("Zip code must be 5 digits.");
			}
		
		// validate toZip code
			if (toZip.length() == 5)
			{
				this.toZip = toZip;
			}
			else {
				throw new IllegalArgumentException("Zip code must be 5 digits.");
			}
			
		// validate weight
			if (weight < 0)
			{
				throw new IllegalArgumentException("Enter valid weight");
				//throw new InvalidBarcodeException("Enter valid weight");
			} else {
				this.weight = Math.round(weight);        // rounding the decimal to nearest ounce
			}
		
			// validate height
			if (height < 0)
			{
				throw new IllegalArgumentException("Enter valid height");
			} else {
				this.height = Math.round(height);        // rounding the decimal to nearest inch
			}

			// validate width
			if (width < 0)
			{
				throw new IllegalArgumentException("Enter valid width");
			} else {
				this.width = Math.round(width);        // rounding the decimal to nearest inch
			}
		
			// validate depth
			if (depth < 0)
			{
				throw new IllegalArgumentException("Enter valid depth");
			} else {
				this.depth = Math.round(depth);        // rounding the decimal to nearest inch
			}
		
		this.hazards = hazards;
	}

	// get package id
	public int getPackageId() {
		return packageId;
	}

	// set package id; package id contains number on kiosk which is between 1-99999 and timestamp in milliseconds
	public void setPackageId(int packageId) {
		
		int timeStamp =  Calendar.getInstance().get(Calendar.MILLISECOND);
		
		if(packageId < 0 || packageId > 99999)
		{
			throw new IllegalArgumentException("Package Id must be between 1 and 99999");
		}
		else this.packageId = packageId + timeStamp;
	}

	// get shipment method
	public String getShipMethod() {
		return shipMethod;
	}

	// set shipment method;
	public void setShipMethod(String shipMethod) {

//		System.out.printf("%s%n%s%n%s%n%s%n", "Enter the Shipment method: ", "Enter 1 if shipment is through Ground", 
//				"Enter 2 if shipment is through Air", "Enter 3 if shipment is through Rail");
			switch(shipMethod)
			{
			case "1":
				this.shipMethod = "GRD"; // GRD for ground
				break;
			case "2":
				this.shipMethod = "AIR"; // AIR for air
				break;
			case "3":
				this.shipMethod = "RAL"; // RAL for rail
			}
	}

	// get package from zip code
	public String getFromZip() {
		return fromZip;
	}

	// set package from zip code
	public void setFromZip(String fromZip) {
		if (fromZip.length() == 5)
		{
			this.fromZip = fromZip;
		}
		else {
			throw new IllegalArgumentException("Zip code must be 5 digits.");
		}
	}

	// get package to zip code
	public String getToZip() {
		return toZip;
	}

	// set package to zip code
	public void setToZip(String toZip) {
		if (toZip.length() == 5)
		{
			this.toZip = toZip;
		}
		else {
			throw new IllegalArgumentException("Zip code must be 5 digits.");
		}
	}

	// get weight of the package
	public double getWeight() {
		return weight;
	}

	// set weight of the package
	public void setWeight(double weight) {
		// validate weight
		if (weight < 0)
		{
			throw new IllegalArgumentException("Enter valid weight");
		} else {
			this.weight = Math.round(weight);        // rounding the decimal to nearest ounce
		}
	}

	// get height of the package
	public int getHeight() {
		return height;
	}

	// set height of the package
	public void setHeight(int height) {
		this.height = height;
	}

	// get width of the package
	public int getWidth() {
		return width;
	}

	// set width of the package
	public void setWidth(int width) {
		this.width = width;
	}

	// get depth of the package
	public int getDepth() {
		return depth;
	}

	// set depth of the package
	public void setDepth(int depth) {
		this.depth = depth;
	}

	// get any hazardous material of the package
	public String getHazards() {
		return hazards;
	}

	// set any hazardous material in the package
	public void setHazards(String hazards) {
		this.hazards = hazards;
	}
	
	// String representation of the Barcode
	@Override
	public String toString()
	{
		return String.format("%2d|%s|%s|%s|%.0f|%d|%d|%d|%s%n", getPackageId(), getShipMethod(), getFromZip(), getToZip(),
				getWeight(), getHeight(), getWidth(), getDepth(), getHazards());
	}
	
	
	
	
	
}
