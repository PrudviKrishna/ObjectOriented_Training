package com.jits.shipping;

import java.io.IOException;
import java.util.HashMap;

// Route.java
// This class is used to determine the route each package should take

public class Route1 {
	
	// HashMap for distribution center and its corresponding states.
	private HashMap<String, String> stateOfDistributionCenter;	
	// Hash Map of String key-value pairs; key for states and values for distribution centers.
	private HashMap<String, String> distributionCenterForState; 

	private String toZip;
	private String destinationStreet;
	private String warehouseLocation;
	private String distributionCenterLocation;
	private String destinationLocation;
	
	public Route1()
	{
		stateOfDistributionCenter = new HashMap<String, String>();
		distributionCenterForState = new HashMap<String, String>();
		loadMaps();
		
		toZip = null;
		destinationStreet = null;
		warehouseLocation = null;
		distributionCenterLocation = null;
		destinationLocation = null;
	}
		
	public void loadMaps()
	{
		loadDistributionCenterMap();
		loadStatesForDistributionCenter();
		try {
			ReadingZipCodeFromExcel.loadMap();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadStatesForDistributionCenter()
	{
		// Adding key value pairs to HashMap
		stateOfDistributionCenter.put("DC1", "Raleigh");
		stateOfDistributionCenter.put("DC2", "Kansas City");
		stateOfDistributionCenter.put("DC3", "Denver");
	}
	
	public void loadDistributionCenterMap()
	{
		// Adding key value pairs to the hash map
		distributionCenterForState.put("AL",	 "DC2");
		distributionCenterForState.put("AK",	 "DC3");
		distributionCenterForState.put("AZ",	 "DC3");
		distributionCenterForState.put("AR",	 "DC2");
		distributionCenterForState.put("CA",	 "DC3");
		distributionCenterForState.put("CO",	 "DC3");
		distributionCenterForState.put("CT",	 "DC1");
		distributionCenterForState.put("DE",	 "DC1");
		distributionCenterForState.put("DC",	 "DC1");
		distributionCenterForState.put("FL",	 "DC1");
		distributionCenterForState.put("GA",	 "DC1");
		distributionCenterForState.put("HI",	 "NA");
		distributionCenterForState.put("ID",	 "DC3");
		distributionCenterForState.put("IL",	 "DC2");
		distributionCenterForState.put("IN",	 "DC2");
		distributionCenterForState.put("IA",	 "DC2");
		distributionCenterForState.put("KS",	 "DC3");
		distributionCenterForState.put("KY",	 "DC2");
		distributionCenterForState.put("LA",	 "DC2");
		distributionCenterForState.put("ME",	 "DC1");
		distributionCenterForState.put("MD",	 "DC1");
		distributionCenterForState.put("MA",	 "DC1");
		distributionCenterForState.put("MI",	 "DC2");
		distributionCenterForState.put("MN",	 "DC2");
		distributionCenterForState.put("MS",	 "DC2");
		distributionCenterForState.put("MO",	 "DC2");
		distributionCenterForState.put("MT",	 "DC3");
		distributionCenterForState.put("NE",	 "DC3");
		distributionCenterForState.put("NV",	 "DC3");
		distributionCenterForState.put("NH",	 "DC1");
		distributionCenterForState.put("NJ",	 "DC1");
		distributionCenterForState.put("NM",	 "DC3");
		distributionCenterForState.put("NY",	 "DC1");
		distributionCenterForState.put("NC",	 "DC1");
		distributionCenterForState.put("ND",	 "DC3");
		distributionCenterForState.put("OH",	 "DC1");
		distributionCenterForState.put("OK",	 "DC3");
		distributionCenterForState.put("OR",	 "DC3");
		distributionCenterForState.put("PA",	 "DC1");
		distributionCenterForState.put("RI",	 "DC1");
		distributionCenterForState.put("SC",	 "DC1");
		distributionCenterForState.put("SD",	 "DC3");
		distributionCenterForState.put("TN",	 "DC2");
		distributionCenterForState.put("TX",	 "DC3");
		distributionCenterForState.put("UT",	 "DC3");
		distributionCenterForState.put("VT",	 "DC1");
		distributionCenterForState.put("VA",	 "DC1");
		distributionCenterForState.put("WA",	 "DC2");
		distributionCenterForState.put("WV",	 "DC1");
		distributionCenterForState.put("WI",	 "DC2");
		distributionCenterForState.put("WY",	 "DC3");

	}
	
	// This method returns the destination state for the toZip in the barcode of the package
	public String getDestinationState()
	{	
		String state = ReadingZipCodeFromExcel.getZipToState().get(getToZip());	// Getting state for the toZip-zipcode from the excel file
		return state;
	}
	
	// This method returns the location of the distribution center in the format: "DistributionCenter it's city"
	public String getDistributionCenterLocation()
	{
		String code = getDistributionCenter();
		String city = stateOfDistributionCenter.get(code);
		
		return String.format("%s %s", code, city);
	}
	
	// This method returns the location of the warehouse
	public String getWarehouseLocation()
	{
		return String.format("%s", "Whse");
	}
	
	// This method returns the location of the package destination
	public String getDestinationLocation()
	{
		return String.format("%s %s", destinationStreet, toZip);		
	}

	// set destination street - We get Destination street to route class from package class.
	// In package class we are taking an Route object and we are setting destination street.
	public void setDestinationStreet(String destination)
	{
		this.destinationStreet = destination;
	}
	
	// This method returns the destination street of the package
	public String getDestinationStreet()
	{
		return destinationStreet;
	}
	
	// set toZip - We get toZip/destination zip code to route class from package class.
	// In package class we are taking an Route object and we are setting destination zip code.
	public void setToZip(String zip)
	{
		this.toZip = zip;
	}
	
	// This method returns toZip
	public String getToZip()
	{
		return toZip;
	}
	
	// This method will take state as parameter and return the distribution center of that state.
	public String getDistributionCenter()
	{
		String state = ReadingZipCodeFromExcel.getZipToState().get(getToZip());
		return distributionCenterForState.get(state);
	}

	// This method sets warehouse location of the route of package
	public void setWarehouseLocation(String warehouseLocation) {
		this.warehouseLocation = warehouseLocation;
	}

	// This method sets distribution center location of the route of package
	public void setDistributionCenterLocation(String distributionCenterLocation) {
		this.distributionCenterLocation = distributionCenterLocation;
	}

	// This method sets destination location of the route of package
	public void setDestinationLocation(String destinationLocation) {
		this.destinationLocation = destinationLocation;
	}
	
	
}
