package com.jits.transfer;

import com.jits.core.Address;
import com.jits.core.Box;
import com.jits.core.DeliveryTime;
import com.jits.core.Parcel;
import com.jits.core.ParcelWeight;
import com.jits.core.ParcelWeightFactory;
import com.jits.core.ShippingCost;

public class DeliveryRequest implements IConfirmation{

	private Parcel parcel;
	
	public DeliveryRequest(Parcel parcel)
	{
		this.parcel = parcel;
	}
	
	@Override
	public String getStatus() {

		return "pending";
	}

	@Override
	public Address getOrigin() {
		return parcel.getOrigin();
	}

	@Override
	public Address getDest() {
		return parcel.getDestination();
	}

	@Override
	public String getPackageType() {
		return parcel.getType();
	}

	@Override
	public String getDeliveryType() {
		return parcel.getShipment();
	}

	@Override
	public double getWeight() {
		ParcelWeight parcelWeight = ParcelWeightFactory.getParcelWeight(parcel);
		double weight = parcelWeight.weighParcel();
		return weight;
	}

	@Override
	public boolean isInsured() {
		String response = parcel.getInsurance();
		return Boolean.parseBoolean(response);
	}

	@Override
	public double getDeliveryTime() {
		DeliveryTime deliveryTime = new DeliveryTime(parcel);
		double time = deliveryTime.getDeliveryTime();
		return time;
	}

	@Override
	public double getCost() {
		ShippingCost shippingCost = new ShippingCost(parcel);
		double cost = shippingCost.getShippingCost();
		return cost;
	}
	
	@Override
	public String toString()
	{
		return String.format("%nDelivery Status: %s%nOrigin Address: %s%nDestination Address: %s%nParcel Type: %s%nDelivery Type: %s%nParcel Weight: %s%nInsured: %s%nDelivery Time: %s%nShipping Cost: %s%n", 
				getStatus(), getOrigin(), getDest(), getPackageType(), getDeliveryType(), getWeight(), isInsured(), getDeliveryTime(), getCost());
	}

}
