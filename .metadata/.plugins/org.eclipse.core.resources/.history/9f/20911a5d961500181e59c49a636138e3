package com.jits.transfer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.jits.core.Address;
import com.jits.core.Box;
import com.jits.core.DeliveryTime;
import com.jits.core.Parcel;
import com.jits.core.ParcelWeight;
import com.jits.core.ParcelWeightFactory;
import com.jits.core.ShippingCost;

@XmlRootElement
public class DeliveryRequest implements IConfirmation{

	private Parcel parcel;
	
	public DeliveryRequest(Parcel parcel)
	{
		this.parcel = parcel;
	}
	
	@XmlElement
	int count = 0;
	@Override
	public String getStatus() {
		String status = null;
		switch(count) {
		case 0:
			status = "Pending";
			count++;
			break;
		case 1:
			status = "Confirmed";
			break;
		}
		return status;
	}
	
	@XmlAttribute
	public String getId()
	{
		return parcel.getParcelId();
	}
	
	@XmlElement
	@Override
	public Address getOrigin() {
		return parcel.getOrigin();
	}

	@XmlElement
	@Override
	public Address getDest() {
		return parcel.getDestination();
	}

	@XmlElement
	@Override
	public String getPackageType() {
		return parcel.getType();
	}

	@XmlElement
	@Override
	public String getDeliveryType() {
		return parcel.getShipment();
	}

	@XmlElement
	@Override
	public double getWeight() {
		ParcelWeight parcelWeight = ParcelWeightFactory.getParcelWeight(parcel);
		double weight = parcelWeight.weighParcel();
		return weight;
	}

	@XmlElement
	@Override
	public boolean isInsured() {
		if (parcel.getType().equals("B"))
		{
		String result = ((Box) parcel).getInsured();
		return Boolean.parseBoolean(result);
		} else return false;
	}

	@XmlElement
	@Override
	public double getDeliveryTime() {
		DeliveryTime deliveryTime = new DeliveryTime(parcel);
		double time = deliveryTime.getDeliveryTime();
		return time;
	}

	@XmlElement
	@Override
	public double getCost() {
		ShippingCost shippingCost = new ShippingCost(parcel);
		double cost = shippingCost.getShippingCost();
		return cost;
	}
	
	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}

	@Override
	public String toString()
	{
		return String.format("%nDelivery Status: %s%nOrigin Address: %s%nDestination Address: %s%nParcel Type: %s%nDelivery Type: %s%nParcel Weight: %s%nInsured: %s%nDelivery Time: %s%nShipping Cost: %s%n", 
				getStatus(), getOrigin(), getDest(), getPackageType(), getDeliveryType(), getWeight(), isInsured(), getDeliveryTime(), getCost());
	}

}
