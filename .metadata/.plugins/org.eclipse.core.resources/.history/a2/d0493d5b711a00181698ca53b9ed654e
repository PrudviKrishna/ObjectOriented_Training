package com.lambdas.IntStreamOperations;

import java.util.stream.IntStream;

// IntStreamOperations.java
// Demonstrating IntStream Operations
public class IntStreamOperations {

	public static void main(String[] args) {
		
		int[] values = {3, 10, 7, 8, 9, 1, 3, 4, 6};
		
		// display original values
		IntStream.of(values)
				 .forEach(value -> System.out.printf("%d ", value));
		System.out.println();
		
		// count, min, max, sum and average of the values
		System.out.printf("%nCount: %d%n", IntStream.of(values).count());	// count
		
		System.out.printf("%nMin: %d%n", IntStream.of(values).min().getAsInt());	// min
		
		System.out.printf("%nMax: %d%n", IntStream.of(values).max().getAsInt());	// max
		
		System.out.printf("%nSum: %d%n", IntStream.of(values).sum());	// sum
		
		System.out.printf("%nAverage: %.2f%n", IntStream.of(values).average().getAsDouble());	// average
		
		// Sum of values with reduce method
		System.out.printf("%nSum via reduce method: %d%n", IntStream.of(values).reduce(0, (x, y) -> x + y));
		
		// sum of squares of values with reduce method
		System.out.printf("%nSum of squares of values via reduce method: %d%n", IntStream.of(values).reduce(0, (x, y) -> x + y * y));
		
		// product of values with reduce method
		System.out.printf("%nProduct of values via reduce method: %d%n", IntStream.of(values).reduce(1, (x, y) -> x * y));
		
		// even values displayed in sorted order
		IntStream.of(values).filter(value -> value % 2 == 0).sorted().forEach(values -> System.out.printf("%d ", value););
	}

}
