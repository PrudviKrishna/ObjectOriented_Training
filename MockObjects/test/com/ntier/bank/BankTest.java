package com.ntier.bank;
import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.ntier.bank.Account;
import com.ntier.bank.Bank;

class BankTest {
	
	// The business object under test
	private Bank bank;
	private Account acct;
	
//	@Before
//	public void setUp()
//	{
//		// create mock objects
//		acct = EasyMock.createMock(Account.class);
//		// feed it to the business object
//		bank = new Bank(acct);
//	}
	
	public BankTest()
	{
		// create mock objects
		acct = EasyMock.createMock(Account.class);
		// feed it to the business object
		bank = new Bank(acct);
	}
	
	@Test
	public void testGetName()
	{
		// record method call on it
		EasyMock.expect(acct.getName()).andReturn("Prudvi Krishna").anyTimes();
		// get it ready for action
		EasyMock.replay(acct);
		// My test
		assertEquals("Prudvi Krishna", bank.getAccountHolderName());
		// call verify
		EasyMock.verify(acct);
	}
	
	@Test
	public void testGetAccountBalance()
	{
		// record method call on it
		EasyMock.expect(acct.getBalance("USD")).andReturn(100.0);
		// get it ready for action
		EasyMock.replay(acct);
		// My Test
		assertEquals(100.0, bank.getAccountBalance("USD"), 0.001);
		// call verify
		EasyMock.verify(acct);
	}
	
	@Test
	public void testCloseAccount()
	{
		acct.close(); // we do not have to do expect with no return value
		EasyMock.replay(acct);
		assertEquals("account closed", bank.closeAccount());
		EasyMock.verify(acct);
	}

}
