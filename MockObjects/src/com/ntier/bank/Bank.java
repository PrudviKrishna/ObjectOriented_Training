package com.ntier.bank;

public class Bank {

	// The collaborator object
	private Account acct;
	
	public Bank(Account acct)
	{
		this.acct = acct;
	}
	
	public String getAccountHolderName()
	{
		return acct.getName();
	}
	
	public double getAccountBalance(String currency)
	{
		return acct.getBalance(currency);
	}
	
	public String closeAccount()
	{
		acct.close();
		return "account closed";
	}
}
