/**
 * 
 */
package com.ntier.mockinsuranceprovider;

/**
 * @author W1WG
 *
 */
public class CarReservation {
	
	private InsuranceProvider insuranceProvider;
	
	public CarReservation(InsuranceProvider insuranceProvider)
	{
		this.insuranceProvider = insuranceProvider;
	}
	
	public boolean insure(String make, String model)
	{
		return insuranceProvider.insure(make, model);
	}
	
	public boolean payPremium(double premium)
	{
		 insuranceProvider.payPremium(premium);
		 return ((premium == 10.0)? true : false);
	}
}
