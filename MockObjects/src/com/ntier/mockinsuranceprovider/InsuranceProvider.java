/**
 * 
 */
package com.ntier.mockinsuranceprovider;

/**
 * @author W1WG
 *
 */
public interface InsuranceProvider {
	
	public boolean insure(String make, String model);
	public void payPremium(double premium);
}
