package com.journaldev.state;

public interface State {

	public void doAction();
	
}
