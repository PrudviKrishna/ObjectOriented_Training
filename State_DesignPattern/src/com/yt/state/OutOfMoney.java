package com.yt.state;

public class OutOfMoney implements ATMState{

	private ATMMachine atmMachine;
	
	public OutOfMoney(ATMMachine atmMachine)
	{
		this.atmMachine = atmMachine;
	}
	
	@Override
	public void insertCard() {
		System.out.println("We don't have money");
		
	}

	@Override
	public void ejectCard() {
		System.out.println("We don't have money, you did not enter card");
		
	}

	@Override
	public void insertPin(int pinEntered) {
		System.out.println("We don't have money");
		
	}

	@Override
	public void requestCash(int cashToWithdraw) {
		System.out.println("We don't have money");
		
	}

}
