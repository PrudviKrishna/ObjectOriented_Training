package com.yt.state;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Test {

	public static void main(String[] args) {
		
		ArrayList<String> a = new ArrayList<String>();
		a.add("coke");
		a.add("pepsi");
		a.add("mirinda");
		System.out.println("Total ArrayList: " + a);
		
		String[] s1 = new String[a.size()];
		String[] s2 = a.toArray(s1);
		
		System.out.println(s1==s2);
		System.out.println(Arrays.toString(s1));
		System.out.println(Arrays.toString(s2));
		
		s1 = new String[1];
		s1[0] = "Test Data";
		String[] s3	 = a.toArray(s1);
		System.out.println("=============================");
		System.out.println(s1==s3);
		System.out.println(Arrays.toString(s1));
		System.out.println(Arrays.toString(s3));
		
		int x = 3;
		int y = ++x * 4 / x-- + --x;
		System.out.println("y + x is" + (y + x));
		
		// LocalDate.parse
		LocalDate date = LocalDate.parse("2014-12-30");
		System.out.println("1." + date);
		date = date.plusDays(2);
		System.out.println("2." + date);
		
//		int z[] = new int[0];
//		System.out.println(z);
		Integer one = new Integer(1);
		Integer two = new Integer(2);
		one = two;
		one = null; // which are available for garbage collection
		
		System.out.println(Instant.now());
		
//		Object i = Integer.valueOf(42);
//		String str = (String) i;
		
		String x1 = "1";
		int i = Integer.parseInt(x1);
		System.out.println(i);
		
		
	}

}
