package com.javat.prototype;

public interface Prototype {

	public Prototype getClone();
}
