package com.journaldev.prototype;

import java.util.List;

public class PrototypePatternTest {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		Employees emp = new Employees();
		emp.loadData();
		
		// Use the clone object to get the Employee Object
		Employees newEmp = (Employees) emp.clone();    
		// If the  object cloning was not provided, we will have to make database call to fetch the employee list every time 
		// and then do the manipulations that would have been resource and time consuming.
		Employees newEmp1 = (Employees) emp.clone();
		
		List<String> list = newEmp.getEmpList();
		list.add("John Snow");
		
		List<String> list1 = newEmp1.getEmpList();
		list1.add("Daenerys");
		
		System.out.println("emp List: " + emp.getEmpList());
		System.out.println("newEmp List: " + newEmp.getEmpList());
		System.out.println("newEmp1 List: " + newEmp1.getEmpList());
	}

}
