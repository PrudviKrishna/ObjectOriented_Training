package com.mvc.dinesh;

public class FrontControllerPatternDemo {

	public static void main(String[] args) {
		
		FrontController frontController = new FrontController();
		frontController.dispatchRequest("COMPANY");
		frontController.dispatchRequest("EMPLOYEE");
	}

}
