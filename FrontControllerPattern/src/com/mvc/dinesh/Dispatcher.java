package com.mvc.dinesh;

public class Dispatcher {

	private EmployeeView employeeView;
	private CompanyView companyView;
	
	
	public Dispatcher() {
		employeeView = new EmployeeView();
		companyView = new CompanyView();
	}
	
	public void dispatch(String request) {
		if (request.equalsIgnoreCase("EMPLOYEE"))
		{
			employeeView.view();
		} else {
			companyView.view();
		}
	}
}
