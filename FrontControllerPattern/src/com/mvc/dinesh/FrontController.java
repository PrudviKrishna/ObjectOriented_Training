package com.mvc.dinesh;

public class FrontController {

	private Dispatcher dispatcher;
	
	public FrontController()
	{
		dispatcher = new Dispatcher();
	}
	
	public void dispatchRequest(String request)
	{
		System.out.println("Page requested: " + request);
		dispatcher.dispatch(request);
	}
}
