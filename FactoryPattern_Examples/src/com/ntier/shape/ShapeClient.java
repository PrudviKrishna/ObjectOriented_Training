package com.ntier.shape;

public class ShapeClient {

	public static void main(String[] args) {
		
		// call the factory method, get a shape back
		Shape s1 = ShapeFactory.getInstance("CIRCLE"); // returns a square
		s1.calcArea();

	}

}
