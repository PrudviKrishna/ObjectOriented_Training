package com.ntier.factory_method_pattern;

import java.util.Map;

public abstract class Employee {
	
	private long id;
	private String firstName;
	private String lastName;
	private Document document;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public abstract void getPapers(Map<String, String> map);

}
