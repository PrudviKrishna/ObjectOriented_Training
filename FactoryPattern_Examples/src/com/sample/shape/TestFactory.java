package com.sample.shape;

public class TestFactory {

	public static void main(String[] args) {
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		// get an object of Circle and call its draw method
		Shape shape1 = shapeFactory.getShape("CIRCLE");
		// Come back later

	}

}
