package com.jits.shipping;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class RouteTest {
	
	private Route route;
	private Barcode barcode;
	private Package packet;
	
	
	public RouteTest()
	{
		try {
			barcode = new Barcode(1, "1", "20170", "64468", 4.2, 5, 5, 5, "Nothing", "12 Main St.");
			packet = new Package(barcode);
			route = new Route();
		} catch (InvalidBarcodeException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateRoute() {
		List<Location> l = route.generateRoute(packet);
		String[] loc = {"Whse", "DistCtr: DC2 and Kansas City", "Dest: 12 Main St. and 64468"};
		int count = 0;
		
		
		for(Location location : l)
		{
			assertEquals(loc[count], location.getLocation());
			count++;
		}
	}

}
