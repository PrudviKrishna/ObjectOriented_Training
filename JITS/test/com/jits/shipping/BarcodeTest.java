package com.jits.shipping;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;


public class BarcodeTest {

	private Barcode barcode;
	
	public BarcodeTest()
	{
		try {
			barcode = new Barcode(0, "1", "75034", "61701", 4.2, 4, 4, 4, "Nothing", "6 Clobertin Court.");
		} catch (InvalidBarcodeException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetPackageId() {
		assertFalse(4 == barcode.getPackageId());
	}

	@Test
	public void testSetPackageId() throws InvalidBarcodeException {
		int packageId = 5;
		barcode.setPackageId(packageId);
		assertFalse(barcode.getPackageId() == packageId);
	}

	@Test
	public void testGetShipMethod() {
		assertEquals("GRD", barcode.getShipMethod());
		assertTrue("GRD".equals(barcode.getShipMethod()));
		assertFalse("AIR".equals(barcode.getShipMethod()));
	}

	@Test
	public void testSetShipMethod() {
		String shipMethod = "3";
		barcode.setShipMethod(shipMethod);
		assertEquals("RAL", barcode.getShipMethod());
		assertTrue("RAL".equals(barcode.getShipMethod()));
		assertFalse("GRD".equals(barcode.getShipMethod()));
		
	}

	@Test
	public void testGetFromZip() {
		assertEquals("75034", barcode.getFromZip());
		assertFalse("54321".equals(barcode.getFromZip()));
	}

	@Test
	public void testSetFromZip() throws InvalidBarcodeException {
		String fromZip = "24680";
		barcode.setFromZip(fromZip);
		assertEquals("24680", barcode.getFromZip());
		assertFalse("54321".equals(barcode.getFromZip()));
	}

	@Test
	public void testGetToZip() {
		assertEquals("61701", barcode.getToZip());
		assertFalse("12345".equals(barcode.getToZip()));
	}

	@Test
	public void testSetToZip() throws InvalidBarcodeException {
		String toZip = "98660";
		barcode.setToZip(toZip);
		assertEquals("98660", barcode.getToZip());
		assertFalse("12345".equals(barcode.getToZip()));
	}

	@Test
	public void testGetWeight() {
		assertEquals(4, barcode.getWeight());
		assertFalse(4.8 == barcode.getWeight());
	}

	@Test
	public void testSetWeight() throws InvalidBarcodeException {
		double weight = 5.6;
		barcode.setWeight(weight);
		assertEquals(6, barcode.getWeight());
		assertFalse(4.8 == barcode.getWeight());
	}

	@Test
	public void testGetHeight() {
		assertEquals(4, barcode.getHeight());
		assertFalse(4.8 == barcode.getHeight());
	}

	@Test
	public void testSetHeight() {
		int height = 5;
		barcode.setHeight(height);
		assertEquals(5, barcode.getHeight());
		assertFalse(4 == barcode.getHeight());
	}

	@Test
	public void testGetWidth() {
		assertEquals(4, barcode.getWidth());
		assertFalse(4.8 == barcode.getWidth());
	}

	@Test
	public void testSetWidth() {
		int width = 4;
		barcode.setWidth(width);
		assertEquals(4, barcode.getWidth());
		assertFalse(3 == barcode.getWidth());
	}

	@Test
	public void testGetDepth() {
		assertEquals(4, barcode.getDepth());
		assertFalse(4.8 == barcode.getDepth());
	}

	@Test
	public void testSetDepth() {
		int depth = 3;
		barcode.setDepth(depth);
		assertEquals(3, barcode.getDepth());
		assertFalse(4 == barcode.getDepth());
	}

	@Test
	public void testGetHazards() {
		assertEquals("Nothing", barcode.getHazards());
		assertFalse("Something Hazardeous".equals(barcode.getHazards()));
	}

	@Test
	public void testSetHazards() {
	String hazards = "Careful";
	barcode.setHazards(hazards);
	assertEquals("Careful", barcode.getHazards());
	assertFalse("Something Hazardeous".equals(barcode.getHazards()));
	}
	
	@Test
	public void testGetDestinationStreet()
	{
		assertEquals("6 Clobertin Court.", barcode.getDestinationStreet());
		assertTrue("6 Clobertin Court.".equals(barcode.getDestinationStreet()));
		assertFalse("12 Main St.".equals(barcode.getDestinationStreet()));
	}
	
	@Test
	public void testSetDestinationStreet()
	{
		String destinationStreet = "12 Main St.";
		barcode.setDestinationStreet(destinationStreet);
		assertEquals("12 Main St.", barcode.getDestinationStreet());
		assertFalse("6 Clobertin Court.".equals(barcode.getDestinationStreet()));
	}
}
