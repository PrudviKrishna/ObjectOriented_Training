package com.jits.shipping;

// RailYard.java
// This class contains the details of the Rail Yard
public class RailYard {

	// attributes
	private String regionCode;
	private String city;
	private String state;
	private String zipCode;
	
	// No-arg constructor
	public RailYard()
	{
		
	}
	
	// Arg - constructor
	public RailYard(String regionCode, String city, String state, String zipCode) {
		this.regionCode = regionCode;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}

	// get region of the Rail Yard
	public String getRegionCode() {
		return regionCode;
	}

	// set region of the Rail Yard
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	// get city of the Rail Yard
	public String getCity() {
		return city;
	}
	// set city of the Rail Yard
	public void setCity(String city) {
		this.city = city;
	}
	// get code of the Rail Yard
	public String getState() {
		return state;
	}
	// set code of the Rail Yard
	public void setState(String state) {
		this.state = state;
	}
	// get zipode of the Rail Yard
	public String getZipCode() {
		return zipCode;
	}
	// set zipCode of the Rail Yard
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
