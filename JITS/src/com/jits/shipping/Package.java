package com.jits.shipping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import common.Logger;

// Package.java
// This class knows about Barcode of the package and route of the package

public class Package {
	
	private static final Logger LOG = Logger.getLogger(Package.class);

	// attributes; package has barcode and route
	private Barcode barcode;
	//private String locationStatus;
	
	private List<Location> route;
	private List<String> trackingStrings;
    private int routeIndex = 0;
    private int MAX ;
    
	// Constructor
	public Package(Barcode barcode)
	{
		this.barcode = barcode;
		this.trackingStrings = new ArrayList<>();
		this.setUpRoute();
    	this.movePackageToNextLocation();
    	
	}
	
	// This method generates the route of the package for shipment
	public void setUpRoute()
	{
		this.route = Route.generateRoute(this);
//		LOG.debug("Route established.");
//		List<Location> route = new ArrayList<>();
//		route.add(new Warehouse(this));
//		try {
//			route.add(new DistributionCenterLocation(this));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		route.add(new Destination(this));
//		this.route = route;		
		MAX = route.size()-1;
	}
	
	// This method moves the package from current location to the next location
	public Location movePackageToNextLocation()
	{
		if (routeIndex == MAX)
		{
			routeIndex--;
		}
		else routeIndex++;
		
		Location location = route.get(routeIndex);
		location.logTracking(location.getTrackingString());   // logging current location tracking string to a file
		trackingStrings.add(location.getTrackingString());	  // adding current location tracking string to 'trackingStrings' arraylist
		LOG.info("Returns the location of the package");
		return location;
	}
	
	// get the route of the package
	public List<Location> getRoute() {
		return route;
	}

	// set the route of the package
	public void setRoute(List<Location> route) {
		this.route = route;
	}

	// get the tracking string of the package
	public List<String> getTrackingStrings() {
		return trackingStrings;
	}

	// set the tracking string of the package
	public void setTrackingStrings(List<String> trackingStrings) {
		this.trackingStrings = trackingStrings;
	}

	// get barcode of the Barcode
	public Barcode getBarcode() {
		return barcode;
	}
	
	// set barcode of the package
	public void setBarcode(Barcode barcode) {
		this.barcode = barcode;
	}
}
