package com.jits.shipping;

import static java.lang.Math.*;

public class DistanceCalculator 
{
	private static double R = 6371;  // earth's radius in kilometers
	
  // prevent instantiation
  public DistanceCalculator() {}
  
  /**
   * Calculates the distance between two points.
   * @param fromLat latitude in decimal degrees
   * @param fromLng longitude in decimal degrees
   * @param toLat   latitude in decimal degrees
   * @param toLng   longitude in decimal degrees
   * @return distance in miles
   */
  public static double calcDistance(double fromLat, double fromLng, double toLat, double toLng) {
		return calcDistanceSphericalLawOfCosine(fromLat, fromLng, toLat, toLng);
  }
	
	public static double convertKmToMiles(double km) {
		return km * .62;
	}
	
	public static double convertMilesToKm(double miles) {
		return miles / .62;
	}
  
  public static double calcDistanceSphericalLawOfCosine(double fromLat, double fromLng, double toLat, double toLng) {
    double rToLat = toRadians(toLat);
    double rToLng = toRadians(toLng);
    double rFromLat = toRadians(fromLat);
    double rFromLng = toRadians(fromLng);
    return acos(sin(rFromLat) * sin(rToLat) + cos(rFromLat) * cos(rToLat) * cos(rFromLng - rToLng)) * R;
  }
}