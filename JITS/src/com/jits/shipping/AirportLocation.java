package com.jits.shipping;

import java.util.Date;

public class AirportLocation extends Tracking implements Location{

	private Airport airport;
	
	// Parameterized constructor; calls super class constructor with super keyword to initialize packet.
	public AirportLocation(Airport airport, Package packet) {
		super(packet);
		this.airport = airport;
	}

	// This method returns the location of the Airport in the required format{Arprt: code and name}
	@Override
	public String getLocation() {
		
		String code = getAirport().getCode();
		String name = getAirport().getName();
		return String.format("%s: %s and %s", "Arprt", code, name);
	}

	// This method returns the tracking string of the Airport in the required format {Airport code|package Id|time stamp}
	@Override
	public String getTrackingString() {
		int packageId = getPacket().getBarcode().getPackageId();
		Date timeStamp = new Date();
		String airportCode = getAirport().getCode();
		return String.format("%s|%d|%s", airportCode, packageId, timeStamp);
	}

	public Airport getAirport() {
		return airport;
	}

	public void setAirport(Airport airport) {
		this.airport = airport;
	}
	
	
	
	
}
