/**
 * 
 */
package com.jits.shipping;

import java.util.HashMap;

/**
 * @author W1WG
 * The package is moved to the nearest distribution center of the destination; if it is Ground shipment 
 * and from distribution center it is shipped to destination.
 */
public class DistributionCenter {

	// HashMap for distribution center and its corresponding states.
	private static HashMap<String, String> cityOfDistributionCenter;	

	static {
		cityOfDistributionCenter = new HashMap<>();
		loadCitiesForDC();
	}
	
	public static void loadCitiesForDC()
	{
		// Adding key value pairs to HashMap
		cityOfDistributionCenter.put("DC1", "Raleigh");
		cityOfDistributionCenter.put("DC2", "Kansas City");
		cityOfDistributionCenter.put("DC3", "Denver");
	}

	public static HashMap<String, String> getCityOfDistributionCenter() {
		return cityOfDistributionCenter;
	}

	public static void setCityOfDistributionCenter(HashMap<String, String> cityOfDistributionCenter) {
		DistributionCenter.cityOfDistributionCenter = cityOfDistributionCenter;
	}
	
	
}
