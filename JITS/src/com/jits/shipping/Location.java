/**
 * 
 */
package com.jits.shipping;

/**
 * @author W1WG
 * Location interface contains getLocation(), getTrackingString(), logTracking()  methods 
 * which are common in all the location classes. The implemented class should override these methods
 */
public interface Location {

	String getLocation();
	String getTrackingString();
	void logTracking(String trackingString);
}
