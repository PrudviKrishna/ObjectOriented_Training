package com.jits.shipping;

import com.jits.shipping.util.TrackingWriter;

public abstract class Tracking implements Location {
	
	private Package packet;
	
	public Tracking(Package packet)
	{
		this.packet = packet;
	}

	// TrackingWriter is a utility class for writing tracking strings to a file 'tracking.txt' 
	private TrackingWriter writer = new TrackingWriter("tracking.txt", true);
	
	// This method writes the tracking string to the file
	public void logTracking(String trackingString)
	{
		writer.write(trackingString);
	}

	public Package getPacket() {
		return packet;
	}

	public void setPacket(Package packet) {
		this.packet = packet;
	}	
}
