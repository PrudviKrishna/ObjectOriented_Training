package com.jits.shipping;

import java.util.Date;

// RailYardLocation.java
// This class contains the location of the Rail Yard.
public class RailYardLocation extends Tracking {

	// attribute
	private RailYard railyard;
	
	// constructor
	public RailYardLocation(Package packet, RailYard railyard) {
		super(packet);
		this.railyard =  railyard;
	}
	
	// get rail yard 
	public RailYard getRailyard() {
		return railyard;
	}

	// set rail yard
	public void setRailyard(RailYard railyard) {
		this.railyard = railyard;
	}

	// This method returns the location of the package with Rail shipment
	@Override
	public String getLocation() {
		String regionCode = getRailyard().getRegionCode();
		String city = getRailyard().getCity();
		String state = getRailyard().getState();
		return String.format("%s: %s, %s, and %s", "RY", regionCode, city, state);
	}

	// This method returns the tracking string of the package with Rail shipment
	@Override
	public String getTrackingString() {
		String regionCode = getRailyard().getRegionCode();
		int packageId = getPacket().getBarcode().getPackageId();
		Date timeStamp = new Date();
		return String.format("%s|%s|%s", regionCode, packageId, timeStamp);
	}
}
