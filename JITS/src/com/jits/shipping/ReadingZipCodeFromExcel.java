package com.jits.shipping;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

// ReadingZipCodeFromExcel.java
// This class is used to read the Zip codes from excel sheet 

public class ReadingZipCodeFromExcel {
	
	// HashMap to store ZipCode as key and State as value
	private static HashMap<String, String> zipToState = new HashMap<>();

	public static HashMap<String, String> getZipToState() {
		return zipToState;
	}

	 public static void loadMap() throws IOException
	{
		try
		{
			// Create a workbook from a file 
		Workbook workbook = Workbook.getWorkbook(new File("zip.xls"));
		
//		System.out.println("Total sheets: " + workbook.getNumberOfSheets()); // This workbook contains only 1 sheet
		
		for (int  sheetCount = 0; sheetCount < workbook.getSheets().length; sheetCount++)
		{
			// Get the sheets from the workbook
			Sheet sheet = workbook.getSheet(sheetCount);
			
//			System.out.println("Total rows in this sheet is: " + sheet.getRows());
//			System.out.println("Total columns in this sheet is: " + sheet.getColumns());
			
			// Iterate through rows in the sheet
			for (int row = 0; row < sheet.getRows(); row++)
			{
				String zip = null;   // String to store  zip value
				String state = null; // String to store state value
				
				// Iterate through columns in the sheet
				for (int column = 0; column < sheet.getColumns(); column++ )
				{
					Cell cell = sheet.getCell(column, row);
					
					// Column 0 contains ZipCode; storing zipCode in zip variable
					if (column == 0)
						zip = cell.getContents();
					else
						state = cell.getContents(); // Column 1 contains state; storing state value in state variable
				}
				zipToState.put(zip,  state); // Adding ZipCode and State to the HashMap
			}
		}
		
		// Test code:
		//System.out.println(zipToState.entrySet());
			
		} catch (BiffException e) // handling the exception
		{
			e.printStackTrace();
		}
	}
}
