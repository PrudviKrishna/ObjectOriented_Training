package com.jits.shipping;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

// RaiYardLocator.java
// This class locates the nearest Rail Yard to the package using zip codes
public class RailYardLocator {

	// attributes
	// zipcode->Coordinate table ["98103"->Coordinate(47.67335, 122.342621)]
	private static Map<String, Coordinate> zipCodeMap = new HashMap<String, Coordinate>(29500);
	
	// Rail Yards
	private static Collection<RailYard> railYardList = new ArrayList<RailYard>();
	
	static {
		loadRailYards();
		loadZipcodeData();
	}
	
	// This method finds the nearest/ closest Rail Yard to the give zip code 
	public static RailYard findClosestRailYard(String zipCode)
	{
		RailYard result = null;
		// get coordinate for zip code
		Coordinate zipCord = zipCodeMap.get(zipCode);
		
		// iterate over rail yards, find distance between zipcode and each one
		// hold onto airport with smallest distance
		double shortestDistance = 10000000000.0;
		for (RailYard rail : railYardList)
		{
			String railYardZip = rail.getZipCode();
			Coordinate railYardZipCord = zipCodeMap.get(railYardZip);
			double distance = zipCord.distanceTo(railYardZipCord);
			if (distance < shortestDistance)
			{
				shortestDistance = distance;
				result = rail;
			}
		}
		// return rail yard with smallest distance
		return result;
	}
	
	private static void loadRailYards()
	{
		railYardList.add(new RailYard("NW01", "Portland", "OR", "97212"));
		railYardList.add(new RailYard("SW02", "Phoenix", "AZ", "85003"));
		railYardList.add(new RailYard("NC03", "Rapid City", "SD", "57701"));
		railYardList.add(new RailYard("SC04", "San Antonio", "TX", "78202"));
		railYardList.add(new RailYard("NE05", "Cleveland", "OH", "44102"));
		railYardList.add(new RailYard("SE06", "Jacksonville", "FL", "32202"));
	}
	
	// This method loads the zip code and respective coordinate values from 'zipLatLng.xls' file to 'zipCodeMap' Map.
	public static void loadZipcodeData()
	{
		WorkbookSettings settings = new WorkbookSettings();
		settings.setSuppressWarnings(true);
		
		Workbook workbook = null;
		try 
		{
			// load spreadsheet
			workbook = Workbook.getWorkbook(new File("zipLatLng.xls"), settings);
			Sheet sheet = workbook.getSheet(0);
			
			// read all rows (zero-indexed), store zipcode/ coordinate value in map
			int numRows = sheet.getRows();
			for (int i = 1; i < numRows; i++)
			{
				Cell[] rowCells = sheet.getRow(i);
				String zipCode = rowCells[1].getContents();
				String lat = rowCells[4].getContents();
				String lng = rowCells[5].getContents();
				zipCodeMap.put(zipCode, new Coordinate(Double.parseDouble(lat), Double.parseDouble(lng)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (workbook != null) {
				workbook.close();
			}
		}	
	}
	
}
