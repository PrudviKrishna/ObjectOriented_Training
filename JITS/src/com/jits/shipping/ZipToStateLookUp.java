package com.jits.shipping;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import common.Logger;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

// ReadingZipCodeFromExcel.java
// This class is used to read the Zip codes from excel sheet 

public class ZipToStateLookUp {
	
	private static final Logger LOG = Logger.getLogger(ZipToStateLookUp.class);
	
	// HashMap to store ZipCode as key and State as value
	private static HashMap<String, String> zipToState;
	
	static {
		zipToState = new HashMap<>();
		try {
			LOG.debug("Loading loadZipStateValuesMap()");
			loadZipStateValuesMap();
		} catch (IOException e) {
			e.printStackTrace();
			//LOG.error("IO Exception", e);
			LOG.error(e.getMessage());
			
		}
	}
	
	public static void loadZipStateValuesMap() throws IOException
	{
		 WorkbookSettings settings = new WorkbookSettings();
		 settings.setSuppressWarnings(true);
		try
		{
			// Create a workbook from a file 
			LOG.debug("Loading States by ZipCodes from zip.xls file by using JExcel");
		Workbook workbook = Workbook.getWorkbook(new File("zip.xls"), settings);
		
		for (int  sheetCount = 0; sheetCount < workbook.getSheets().length; sheetCount++)
		{
			// Get the sheets from the workbook
			Sheet sheet = workbook.getSheet(sheetCount);

			// Iterate through rows in the sheet
			for (int row = 0; row < sheet.getRows(); row++)
			{
				String zip = null;   // String to store  zip value
				String state = null; // String to store state value
				
				// Iterate through columns in the sheet
				for (int column = 0; column < sheet.getColumns(); column++ )
				{
					Cell cell = sheet.getCell(column, row);
					
					// Column 0 contains ZipCode; storing zipCode in zip variable
					if (column == 0)
						zip = cell.getContents();
					else
						state = cell.getContents(); // Column 1 contains state; storing state value in state variable
				}
				LOG.debug("Loading Zip code and state to a zipToState map");
				zipToState.put(zip,  state); // Adding ZipCode and State to the HashMap
				
			}
		}	
		} catch (BiffException e) // handling the exception
		{
			e.printStackTrace();
			LOG.error("BiffException", e);
		}
	}

	public static HashMap<String, String> getZipToState() {
		return zipToState;
	}

	public static void setZipToState(HashMap<String, String> zipToState) {
		ZipToStateLookUp.zipToState = zipToState;
	}
	
	
}
