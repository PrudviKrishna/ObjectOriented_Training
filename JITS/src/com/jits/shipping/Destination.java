package com.jits.shipping;

import java.io.IOException;
import java.util.Date;

// Destination is the package delivery location/ package destination
public class Destination extends Tracking {
	
	public Destination(Package packet)
	{
		super(packet);
	}
	
	// This method returns the location of the Destination in the required format{Dest: destination street and zip code}
	@Override
	public String getLocation()
	{
		String destinationStreet = getPacket().getBarcode().getDestinationStreet();
		String toZip = getPacket().getBarcode().getToZip();
		return String.format("%s: %s and %s", "Dest", destinationStreet, toZip);
	}

	// This method returns the tracking string of the Destination in the required format {encoded destination info|package Id|time stamp}
	@Override
	public String getTrackingString() {

		int packageId = getPacket().getBarcode().getPackageId();
		Date timeStamp = new Date();
		
		// concatenate destination street adn zip code, and removing all spaces
		String destinationInfo = getPacket().getBarcode().getDestinationStreet() + getPacket().getBarcode().getToZip();
		String destinationInfoWithoutSpace = destinationInfo.replaceAll("\\s", "");
		char[] charArray = destinationInfoWithoutSpace.toCharArray();
		
		int asciiSum = 0;
		for (int character = 0; character < charArray.length; character++)
		{
			int ascii = (int) charArray[character];
			asciiSum += ascii;				// sum of ascii values of each character in the concatenated string
		}

		return String.format("%s|%d|%s", asciiSum, packageId, timeStamp);
	}
	
	

}
