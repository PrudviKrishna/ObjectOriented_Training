package com.jits.shipping;

// Application-specific exception class for Barcode

public class InvalidBarcodeException extends Exception {

	public InvalidBarcodeException(String message)
	{
		super(message);
	}
	
	public InvalidBarcodeException(String message, Throwable throwable)
	{
		super(message, throwable);
	}
}
