package com.ntier.lang;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

// StringTest.java

public class StringTest {

	
	@Test
	public void testSubstring()
	{
		String s = "JUnit Rules";
	//	String s2 = s1.substring(2);
		assertEquals("Rules", s.substring(6));
		System.out.println("Expected value is same as Actual value. i.e. s.subString(6) == Rules");
	}
	
	@Test
	public void testLength()
	{
		String s = "JUnit Rules";
		assertEquals(11, s.length());
		System.out.println("Expected value is same as Actual value i.e. 11 == 11");
	}
	
}
