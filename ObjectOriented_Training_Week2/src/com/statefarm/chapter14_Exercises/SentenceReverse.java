package com.statefarm.chapter14_Exercises;

import java.util.Scanner;

// SentenceReverse.java
// This class reads a sentence and reverse its words with String split method and displays reverse sentence.

public class SentenceReverse {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);  // prompt
		
		System.out.print("Enter the sentence: ");
		String sentence = input.nextLine();
		
		// Splitting/ tokenizing words in sentence and storing them in words array.
		String[] words = sentence.split(" ");
		
		System.out.printf("%n%s: %n", "The sentence after reversing its words is");
		
		// looping through array and printing them in reverse order.
		for (int i = words.length - 1; i >= 0; i--)
		{
			System.out.printf("%s ", words[i]);
		}

	}

}
