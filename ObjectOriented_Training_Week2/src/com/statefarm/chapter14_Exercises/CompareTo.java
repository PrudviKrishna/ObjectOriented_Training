package com.statefarm.chapter14_Exercises;

import java.util.Scanner;

// CompareTo.java
// This class is used to compare two strings by using String CompareTo method

public class CompareTo {

	public static void main(String[] args) {
		
		Scanner input = new Scanner("System.in");
		
		System.out.println("Enter the first String to comapre: "); // prompt
		String string1 = input.next();
		
		System.out.println("Enter the second String to comapre: "); // prompt
		String string2 = input.next();
		
		int result = string1.compareTo(string2);
		System.out.printf("%nstring1.compareTo(string2) is %d", result);
		
		switch (result)
		{
		case 1:
			System.out.printf("%s is greater than %s%n", string1, string2);
			break;
		case 0:
			System.out.printf("%s is equal to %s%n", string1, string2);
			break;
		case -1:
			System.out.printf("%s is less than %s%n", string1, string2);
			break;
		}

	}

}
