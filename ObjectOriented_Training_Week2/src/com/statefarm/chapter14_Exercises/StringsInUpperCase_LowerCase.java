package com.statefarm.chapter14_Exercises;

import java.util.Scanner;

// StringsInUpperCase_LowerCase.java
// This class takes a line of text from user and prints the text twice; all the text in lowercase and uppercase.

public class StringsInUpperCase_LowerCase {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);  // prompt
		
		System.out.print("Enter the sentence: ");
		String sentence = input.nextLine();
		
		// Splitting/ tokenizing words in sentence and storing them in words array.
		String[] words = sentence.split(" ");
		
		for (int i = 0; i < words.length;  i++)
		{
			System.out.printf("%s ", words[i].toUpperCase());
		}
		System.out.println();
		
		for (int i = 0; i < words.length;  i++)
		{
			System.out.printf("%s ", words[i].toLowerCase());
		}
		
	}

}
