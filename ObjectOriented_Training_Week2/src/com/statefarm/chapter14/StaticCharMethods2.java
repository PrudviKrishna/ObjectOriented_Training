package com.statefarm.chapter14;

import java.util.Scanner;

// StaticCharMethods2.java
// Character class static conversion methods

public class StaticCharMethods2 {

	// executes application
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		//get radix
		System.out.println("Please enter a radix:");
		int radix = input.nextInt();
		
		//get user choice
		System.out.printf("Please choose one:%n1 --%s%n2 -- %s%n", "Convert digit to character", "Convert character to digit");
		int choice = input.nextInt();
		
		// process request
		switch(choice)
		{
		case 1:        // convert digit to character
			System.out.println("Enter a digit:");
			int digit = input.nextInt();
			System.out.printf("Convert digit to character: %s%n", Character.forDigit(digit, radix));
			break;
		case 2:        // convert character to digit
			System.out.println("Enter a character");
			char character = input.next().charAt(0);
			System.out.printf("Convert character to digit: %s%n", Character.digit(character, radix));
			break;
		}
	}
} // end class StaticCharMethods2
