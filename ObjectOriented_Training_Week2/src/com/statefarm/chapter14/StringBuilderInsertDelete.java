package com.statefarm.chapter14;

public class StringBuilderInsertDelete {

	public static void main(String[] args) {
		Object objectref = "hello";  // assign string to an Object reference 
		String string = "goodbye";
		char[] charArray = {'a', 'b', 'c', 'd', 'e', 'f'};
		boolean booleanValue = true;
		char characterValue = 'Z';
		int integerValue = 7;
		long longValue = 1000000000L; // L suffix indicates long
		float floatValue = 2.5f; // f indicates that 2.5 is a float
		double doubleValue = 33.333; // no suffix, double is default
		
		
		StringBuilder buffer = new StringBuilder();
		
		buffer.insert(0, objectref);
		buffer.insert(0, " "); // each of these contains two spaces
		buffer.insert(0, string);
		buffer.insert(0, " ");
		buffer.insert(0, charArray);
		
		    
		
		System.out.printf("buffer contains%n%s%n", buffer.toString());
		
	}

}
