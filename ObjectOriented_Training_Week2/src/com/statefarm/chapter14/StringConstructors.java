package com.statefarm.chapter14;

// StringConstructors.java
// String class constructors

public class StringConstructors {

	public static void main(String[] args) {
		
		char[] charArray = {'b', 'i', 'r', 't', 'h', ' ', 'd', 'a', 'y'};
		String s = new String("hello");
		
		// use String constructors
		
		String s1 = new String();          // no-arg constructor // The new STring object contains no characters i.e empty string.
		String s2 = new String(s);
		String s3 = new String(charArray);  // The new String object contains a copy of the characters in the array
		String s4 = new String(charArray, 6, 3); // The second-arg (6) specifies the starting position from which characters 
		                                         // in the array are accessed and third-arg(3) specifies number of characters (count) to access in the array.
		
		System.out.printf("s1 = %s%ns2 = %s%ns3 = %s%ns4 = %s%n", s1, s2, s3, s4);
	}
} // end class StringConstructors
