package com.statefarm.chapter14;

// StringValueOf.java
// String valueOf methods.

public class StringValueOf {

	public static void main(String[] args) {

		char[] charArray = {'a', 'b', 'c', 'd', 'e', 'f'};
		boolean booleanValue = true;
		char characterValue = 'Z';
		int integerValue = 7;
		long longValue = 1000000000L; // L suffix indicates long
		float floatValue = 2.5f; // f indicates that 2.5 is a float
		double doubleValue = 33.333; // no suffix, double is default
		Object objectRef = "hello"; // assign string to an Object reference
		
		// String.valueOf() returns the String representation of the argument.
		
		// Returns the string representation of the char[]
		System.out.printf("char array = %s%n", String.valueOf(charArray));
		
		// returns the string representation of the (subarray of) char[] from a specified index and upto specified length
		System.out.printf("part of char array = %s%n", String.valueOf(charArray, 3, 3));
		
		// returns string representation of the boolean value
		System.out.printf("boolean = %s%n", String.valueOf(booleanValue));
		
		// returns string representation of the character value
		System.out.printf("char = %s%n", String.valueOf(characterValue));
		System.out.printf("int = %s%n", String.valueOf(integerValue));
		System.out.printf("long = %s%n", String.valueOf(longValue));
		System.out.printf("float = %s%n", String.valueOf(floatValue));
		System.out.printf("double = %s%n", String.valueOf(doubleValue));
		System.out.printf("Object = %s%n", String.valueOf(objectRef));			
	}
} // end class StringValueOf
