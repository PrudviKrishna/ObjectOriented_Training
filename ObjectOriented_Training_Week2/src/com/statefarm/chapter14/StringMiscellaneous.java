package com.statefarm.chapter14;

// StringMiscellaneous.java
// This application demonstrates the length, charAt and getChars methods of String class.

public class StringMiscellaneous {

	public static void main(String[] args) {
		
		String s1 = "hello there";
		char[] charArray = new char[5];
		
		System.out.printf("s1: %s%n", s1);
		
		// test length method
		System.out.printf("%nLength of s1: %d%n", s1.length());
		
		// loop through characters in s1 with charAt and display reversed
		System.out.printf("%nThe string reversed is: ");
		for (int index = s1.length()-1; index >= 0; index--)
		{
			System.out.printf("%c", s1.charAt(index));
		}
		
		// copy characters from string into charArray
		s1.getChars(0, 5, charArray, 0);
		System.out.printf("%nThe character array is: ");

		for (char character : charArray)
		{
			System.out.print(character);
		}
	}
} // end class StringMiscellaneous
