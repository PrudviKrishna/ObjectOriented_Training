package com.statefarm.chapter9_Exercises;

public abstract class Quadrilateral {
    
	private double[] xCoordinate = new double[4];
	private double[] yCoordinate = new double[4];
	
	// constructor
	public Quadrilateral(double[] xCoordinate, double[] yCoordinate) {
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
	}

	public double[] getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(double[] xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public double[] getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(double[] yCoordinate) {
		this.yCoordinate = yCoordinate;
	}
	
	// abstract method to calculate area of quadrilateral
    public abstract double getArea();
	
	 
}
