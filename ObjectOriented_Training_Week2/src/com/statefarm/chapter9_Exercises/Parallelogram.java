package com.statefarm.chapter9_Exercises;

public class Parallelogram extends Quadrilateral {

	private double base;
	private double height;
	
	public Parallelogram(double[] xCoordinate, double[] yCoordinate, double base, double height) {
		super(xCoordinate, yCoordinate);
		this.base = base;
		this.height = height;
	}

	
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void getHeight(double height) {
		this.height = height;
	}


	@Override
	public double getArea()
	{
		return base * height;
	}
	
//	public double getPerimeter()
//	{
//		return 2 * (base + side);
//	}
	
	@Override
	public String toString()
	{
		return String.format("Parallelogram Properties: %n%s: %.2f%n%s: %.2f%n%s: %.2f%n",
				"Base", getBase(), "Height", getHeight(), "Area", getArea());
	}



}
