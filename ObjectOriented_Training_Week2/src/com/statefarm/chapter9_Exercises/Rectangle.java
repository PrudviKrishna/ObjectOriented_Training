package com.statefarm.chapter9_Exercises;

public class Rectangle extends Quadrilateral {

	private double length;
	private double width;
	
	public Rectangle(double[] xCoordinate, double[] yCoordinate, double length, double width) {
		super(xCoordinate, yCoordinate);
		this.length = length;
		this.width = width;
	}

	
	public double getLength() {
		return length;
	}


	public void setLength(double length) {
		this.length = length;
	}


	public double getWidth() {
		return width;
	}


	public void setWidth(double width) {
		this.width = width;
	}


	@Override
	public double getArea()
	{
		return length * width;
	}
	
	public double getPerimeter()
	{
		return 2 * (length + width);
	}
	
	@Override
	public String toString()
	{
		return String.format("Rectangle Properties: %n%s: %.2f%n%s: %.2f%n%s: %.2f%n%s: %.2f%n",
				"Length", getLength(), "Width", getWidth(), "Perimeter", getPerimeter(), "Area", getArea());
	}



}
