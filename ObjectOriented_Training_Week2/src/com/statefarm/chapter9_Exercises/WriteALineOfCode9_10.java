package com.statefarm.chapter9_Exercises;

// Chapter 9 - Exercise 9.10
public class WriteALineOfCode9_10 {

	
	// Specify class PieceWorker inherits from class EMployee
	// public class PieceWorker extends Employee {}
	
	
	// Call super class Employee'stoString method from subclass PieceWorker's toString method
	/**
	 * public String toString(){
	 *   return super.toString();
	 *    
	 * }
	 *
	 */
	
	// Call superclass Employee's constructor from subclass PieceWorker's constructor - assume that the super class constructor 
	// receives three strings representing the firstName, lastName and socialSecucrityNumber
	/**
	 * public PieceWorker(String firstName, String lastName, String socialSecurityNumber)
	 * {
	 *      super(firstName, lastName, socialSecurityNumber);
	 * }
	 */
	
}
