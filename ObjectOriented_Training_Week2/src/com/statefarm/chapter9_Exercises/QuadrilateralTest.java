package com.statefarm.chapter9_Exercises;

public class QuadrilateralTest {

	public static void main(String[] args) {
		
		double[] xCoordinate = {1, 2, 3, 4};
		double[] yCoordinate = {1, 2, 4, 4};
		
		Rectangle rectangle = new Rectangle(xCoordinate, yCoordinate, 4, 2);
		System.out.println(rectangle);
		
		Parallelogram parallelogram = new Parallelogram(xCoordinate, yCoordinate, 4, 2);
		System.out.println(parallelogram);
		
		Trapezoid trapezoid = new Trapezoid(xCoordinate, yCoordinate, 3, 4, 2);
		System.out.println(trapezoid);
		
		Square square = new Square(xCoordinate, yCoordinate, 2);
		System.out.println(square);

	}

}
