package com.statefarm.chapter9_Exercises;

public class Trapezoid extends Quadrilateral {

	private double base1;
	private double base2;
	private double height;
	
	public Trapezoid(double[] xCoordinate, double[] yCoordinate, double base1, double base2, double height) {
		super(xCoordinate, yCoordinate);
		this.base1 = base1;
		this.base2 = base2;
		this.height = height;
	}

	public double getBase1() {
		return base1;
	}

	public void setBase1(double base1) {
		this.base1 = base1;
	}

	public double getBase2() {
		return base2;
	}

	public void setBase2(double base2) {
		this.base2 = base2;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double getArea() {
		return (base1 + base2) * height /2;
	}
	
	@Override
	public String toString()
	{
		return String.format("Trapezoid Properties: %n%s: %.2f%n%s: %.2f%n%s: %.2f%n%s: %.2f%n",
				"Base1", getBase1(), "Base2", getBase2(), "Height", getHeight(), "Area", getArea());
	}
	
	
}
