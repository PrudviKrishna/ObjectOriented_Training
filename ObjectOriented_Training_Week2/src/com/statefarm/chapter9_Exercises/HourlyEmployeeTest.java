package com.statefarm.chapter9_Exercises;


public class HourlyEmployeeTest {

	public static void main(String[] args) {
		
		HourlyEmployee employee = new HourlyEmployee("Prudvi Krishna", "Tarugu Subbaiah", "123-45-6789", 40, 10);
		
		// get hourly employee data
		System.out.println("Employee information obtained by get methods: ");
		
		System.out.printf("%nFirst name is %s%n", employee.getFirstName());
		
		System.out.printf("Last name is %s%n", employee.getLastName());
		
		System.out.printf("Social security number is %s%n", employee.getSocialSecurityNumber());
		
		System.out.printf("Earnings is %.2f%n", employee.getEarnings());
		
		
		System.out.printf("%n%s%n%n%s", "Updated employee information obtained by toString: ", employee);
		
	}

}
