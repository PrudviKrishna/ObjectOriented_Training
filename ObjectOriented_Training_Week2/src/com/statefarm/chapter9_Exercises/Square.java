package com.statefarm.chapter9_Exercises;

public class Square extends Quadrilateral{


	private double side;

	public Square(double[] xCoordinate, double[] yCoordinate, double side) {
		super(xCoordinate, yCoordinate);
		this.side = side;
	}

	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double getArea() {
		
		return side * side;
	}
	
	@Override
	public String toString()
	{
		return String.format("Square Properties: %n%s: %.2f%n%s: %.2f%n",
				"Side", getSide(), "Area", getArea());
	}




}
