package com.statefarm.chapter8_Exercises;

// SavingsAccount.java

public class SavingsAccount {

	// Stores Annual Interest rate of all account holders
	private static double annualInterestRate;
	
	// Amount saver currently has on deposit
	private double savingsBalance;
	
	
	public SavingsAccount(double savingsBalance)
	{
		this.savingsBalance = savingsBalance;
	}

	// get savingsBalance
	public double getSavingsBalance() {
		return savingsBalance;
	}

	// set savingsBalance
	public void setSavingsBalance(double savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
	
	// get annualInterestRate
	public static double getAnnualInterestRate() {
		return annualInterestRate;
	}

	// set annualInterestRate
	public static void setAnnualInterestRate(double annualInterestRate) {
		SavingsAccount.annualInterestRate = annualInterestRate / 100;
	}

	// This method calculates monthly interest
	public double calculateMonthlyInterest()
	{
		return (savingsBalance * annualInterestRate) / 12;
	}
	
	
	// This method sets annualInterestRate with new value
	public static void modifyInterestRate(double newAnnualInterestRate)
	{
		annualInterestRate = newAnnualInterestRate;
	}
}
