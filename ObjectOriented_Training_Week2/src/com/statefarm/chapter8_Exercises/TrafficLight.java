package com.statefarm.chapter8_Exercises;

// enum type for TrafficLight's duration

public enum TrafficLight {

	RED(30),
	GREEN(25),
	YELLOW(5);
	
	// instance field duration
	private final int duration;
	
	// constructor
	private TrafficLight(int duration)
	{
		this.duration = duration;
	}
	
	// get duration
	public int getDuration()
	{
		return duration;
	}
}
