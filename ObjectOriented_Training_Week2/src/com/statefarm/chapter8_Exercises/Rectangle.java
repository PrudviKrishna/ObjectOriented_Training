package com.statefarm.chapter8_Exercises;

// Rectangle.java
// Rectangle class with length and width attributes to calculate perimeter and area of the rectangle.

public class Rectangle {

	// attributes
	private double length = 1.0;
	private double width = 1.0;
	
	// constructor
	public Rectangle(double length, double width)
	{
		if (length <= 0.0 || length >= 20.0)
		{
			throw new IllegalArgumentException("Length must be larger than 0.0 and less than 20.0");
		}
		
		if (width <= 0.0 || width >= 20.0)
		{
			throw new IllegalArgumentException("Width must be larger than 0.0 and less than 20.0");
		}
		this.length = length;
		this.width = width;
		
	}

	// return length of the rectangle
	public double getLength() {
		return length;
	}

	// assign length of the rectangle
	public void setLength(int length) {
		
		if (length <= 0.0 || length >= 20.0)
		{
			throw new IllegalArgumentException("Length must be larger than 0.0 and less than 20.0");
		}
		this.length = length;
	}

	// return width of the rectangle
	public double getWidth() {
		return width;
	}

	// assign the width of the rectangle
	public void setWidth(int width) {
		
		if (width <= 0.0 || width >= 20.0)
		{
			throw new IllegalArgumentException("Width must be larger than 0.0 and less than 20.0");
		}
		
		this.width = width;
	}
	
	public double getPerimeter()
	{
		return 2 * (length + width);
	}
	
	public double getArea()
	{
		return length * width;
	}
	
	@Override
	public String toString()
	{
		return String.format("Rectangle Properties: %n%s: %.2f%n%s: %.2f%n%s: %.2f%n%s: %.2f%n",
				"Length", length, "Width", width, "Perimeter", getPerimeter(), "Area", getArea());
	}
	
	
	


}
