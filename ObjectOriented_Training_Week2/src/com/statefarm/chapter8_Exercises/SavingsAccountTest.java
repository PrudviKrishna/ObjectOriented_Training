package com.statefarm.chapter8_Exercises;

// SavingsAccountTest.java
// class to test SavingsAccount 

public class SavingsAccountTest {

	public static void main(String[] args) {
		
		SavingsAccount saver1 = new SavingsAccount(2000);
		SavingsAccount saver2 = new SavingsAccount(3000);
		
		SavingsAccount.setAnnualInterestRate(4);
		
		for (int month = 1; month <= 12; month++)
		{
			saver1.setSavingsBalance(saver1.getSavingsBalance() + saver1.calculateMonthlyInterest()); 
			 System.out.printf("%n%s %d: %.2f%n", "Savings Balance for month", month, saver1.getSavingsBalance());
			 
			 saver2.setSavingsBalance(saver2.getSavingsBalance() + saver2.calculateMonthlyInterest()); 
			 System.out.printf("%n%s %d: %.2f%n", "Savings Balance for month", month, saver2.getSavingsBalance());
		}

	}

}
