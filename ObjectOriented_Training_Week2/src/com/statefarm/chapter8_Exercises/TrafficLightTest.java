package com.statefarm.chapter8_Exercises;

// TrafficLightTest.java to test enum type TrafficLight
public class TrafficLightTest {

	public static void main(String[] args) {
		
		// displaying traffic lights and duration
		System.out.printf("%-10s%10s%n", "TrafficLights", "Duration" );
		
		// values() returns an array that contains list of enumeration constants
		for (TrafficLight trafficLight : TrafficLight.values())
		{
			System.out.printf("%-10s%10d%n", trafficLight, trafficLight.getDuration());
		}
	}

}
