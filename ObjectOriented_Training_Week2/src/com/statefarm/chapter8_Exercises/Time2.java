package com.statefarm.chapter8_Exercises;

// Time2.java
// Fig 8.5 Time2 class representing time as number of seconds 
// since midnight rather than the three integer values hour, minute and second.

public class Time2 {


	private int hour; // 0-23          // 0 - 86400
	private int minute; // 0-59			// 0 - 3600
	private int second; // 0-59
	
	//Time2 no-argument constructor:
	//Initializes each instance variable to 0
	
	public Time2()
	{
		this(0,0,0); // invoke constructor with three arguments
	}
	
	// Time2 constructor: hour supplied, minute and second defaulted to 0
	public Time2(int hour)
	{
		this(hour,0,0); // invoke constructor with three arguments
	}
	
	// Time2 constructor: hour and minute supplied, second defaulted to 0
	public Time2(int hour, int minute)
	{
		this(hour,minute,0); // invoke constructor with three arguments
	}	
	
	// Time2 constructor: hour, minute and second supplied
	public Time2(int hour, int minute, int second)
	{
		if (hour < 0 || hour >= 24)
		{
			throw new IllegalArgumentException("hour must be 0-23");
		}
		if (minute < 0 || minute >= 60)
		{
			throw new IllegalArgumentException("minute must be 0-59");
		}
		if (second < 0 || second >= 60)
		{
			throw new IllegalArgumentException("second must be 0-59");
		}
		
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		
	}
	
	// Time2 constructor: Another Time2 object supplied
	public Time2(Time2 time)
	{
		// invoke constructor with three arguments
		this(time.getHour()/3600, time.getMinute()/60, time.getSecond());
		//this(time.getHour(), time.getMinute(), time.getSecond());
	}
	
	// set methods
	// set a new time value using universal time
	// validate the data
	public void setTime(int hour, int minute, int second)
	{
		if (hour < 0 || hour >= 24)
		{
			throw new IllegalArgumentException("hour must be 0-23");
		}
		if (minute < 0 || minute >= 60)
		{
			throw new IllegalArgumentException("minute must be 0-59");
		}
		if (second < 0 || second >= 60)
		{
			throw new IllegalArgumentException("second must be 0-59");
		}
	
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		
	}

	// validate and set hour
	public void setHour(int hour) {
		
		if (hour < 0 || hour >= 24)
		{
			throw new IllegalArgumentException("hour must be 0-23");
		}
		this.hour = hour;
	}
	
	// validate set minute
	public void setMinute(int minute) {
		
		if (minute < 0 || minute >= 60)
		{
			throw new IllegalArgumentException("minute must be 0-59");
		}
		this.minute = minute;
	}
	
	// validate set second
	public void setSecond(int second) {
		
		if (second < 0 || second >= 60)
		{
			throw new IllegalArgumentException("second must be 0-59");
		}
		this.second = second;
	}
	
	// Get methods
	// get hour value
	public int getHour() {
		// return hour;
		return (hour * 60 * 60);
	}

	// get minute value
	public int getMinute() {
		//return minute;
		return minute * 60;
	}

	//get second value
	public int getSecond() {
		return second;
	}

	// convert to string in universal-time format (HH:MM:SS)
		public String toUniversalString()
		{
			//return String.format("%02d:%02d:%02d", getHour(), getMinute(), getSecond());
			return String.valueOf(getHour()+getMinute()+getSecond());
		}

		// convert to string in standard-time format (H:MM:SS AM or PM)
		public String toString()
		{
//			return String.format("%d:%02d:%02d %s", ((getHour() == 0 || getHour() == 12) ? 12 : (getHour() % 12)), 
//					getMinute(), getSecond(), (getHour() < 12 ? "AM" : "PM"));return 
			return String.valueOf(getHour()+getMinute()+getSecond());
			
		}
}
