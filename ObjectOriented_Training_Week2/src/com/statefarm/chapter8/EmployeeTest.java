package com.statefarm.chapter8;

// Composition demonstration
public class EmployeeTest {

	public static void main(String[] args) {
		
		Date birth = new Date(3, 28, 1994);
		Date hire = new Date(12, 11, 2017);
		
		Employee employee = new Employee("Prudvi Krishna", "Tarugu Subbaiah", birth, hire);
		
		System.out.println(employee);
	}

} // end class EmployeeTest
