package com.statefarm.chapter8;

public class Time1Test {

	public static void main(String[] args) {
		
		// create and initialize a Time1 object
		Time1 time = new Time1(); // invokes Time1 constructor
		
		// output string representations of the time
		displayTime("After the Time object is created", time);
		System.out.println();
		
		// change the time and output updated time
		time.setTime(13, 27, 6);
		displayTime("After calling setTime", time);
		System.out.println();
		
		// attempt to set time with invalid values
		try
		{
		time.setTime(99, 99, 99); // all values out of range
		}
		catch (IllegalArgumentException e){
			System.out.printf("Exception: %s%n%n", e.getMessage());
		}
		
		// display time after attempt to set invalid values
		displayTime("After calling the setTime with invalid values	", time);
	}
	
	// display a Time1 object in 24-hour and 12-hour formats
	private static void displayTime(String header, Time1 t)
	{
		System.out.printf("%s:%nUniversal Time: %s%nStandard Time: %s%n", header, t.toUniversalString(), t.toString());
		
	}

}
