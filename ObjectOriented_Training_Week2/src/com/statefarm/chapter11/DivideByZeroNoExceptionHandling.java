package com.statefarm.chapter11;

import java.util.Scanner;

// DivideByZeroNoExceptionHandling.java
// Integer division without Exception Handling

public class DivideByZeroNoExceptionHandling {

	// demonstrates throwing an exception when a divide-by-zero occurs
	public static int quotient(int numerator, int denominator) {
		
        return numerator/ denominator;   // possible division by zero
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please enter an integer numerator: ");
		int numerator = input.nextInt();
		System.out.print("Please enter an integer denominator: ");
		int denominator = input.nextInt();
		
		int result = quotient(numerator, denominator);
		System.out.printf("%nResult: %d / %d = %d%n", numerator, denominator, result);
		
	}

} // end class DivideByZeroNoExceptionHandling
