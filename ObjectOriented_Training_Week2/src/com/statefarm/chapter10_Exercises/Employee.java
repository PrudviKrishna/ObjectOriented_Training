package com.statefarm.chapter10_Exercises;


public abstract class Employee {

	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;
	private Date birthDate;
	
	//constructor
	public Employee(String firstName, String lastName, String socialSecurityNumber, Date birthDate)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.birthDate = birthDate;
	}

	// return first name
	public String getFirstName() {
		return firstName;
	}

	// return last name
	public String getLastName() {
		return lastName;
	}

	// return social security number
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	
	// return string representation of employee object
	@Override
	public String toString()
	{
		return String.format("%s %s%nSocial Security Number: %s", getFirstName(), getLastName(), getSocialSecurityNumber());
	}
	
	// abstract methods must be overridden by concrete subclasses
	public abstract double earnings(); // no implementation here

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
}
