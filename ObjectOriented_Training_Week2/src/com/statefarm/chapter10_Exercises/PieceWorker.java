package com.statefarm.chapter10_Exercises;

// PieceWorker.java sub class of Employee class

public class PieceWorker extends Employee {

	private double wage;	// stores employees wage per piece
	private int pieces; 	// number of pieces produced
	private Date birthDate;
	
	// constructor
	public PieceWorker(String firstName, String lastName, String socialSecurityNumber, Date birthDate, double wage,
			int pieces) {
		super(firstName, lastName, socialSecurityNumber, birthDate);
		this.wage = wage;
		this.pieces = pieces;
	}

	public double getWage() {
		return wage;
	}

	public void setWage(double wage) {
		this.wage = wage;
	}

	public int getPieces() {
		return pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	@Override
	public double earnings() {
		return getPieces() * getWage();
	}
	

	
	
}
