package com.statefarm.chapter10_Exercises;

// HourlyEmployee.java
// HourlyEmployee class extends Employee class

public class HourlyEmployee extends Employee {

	private double wage; // wage per hour
	private double hours; // hours worked per week
	
	// constructor
	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, Date birthDate, double wage,
			double hours)
	{
		super(firstName, lastName, socialSecurityNumber, birthDate);
		
		if (wage < 0.0) // validate wage
		{
			throw new IllegalArgumentException("The hourly wage must be >=0.0");
		}
		
		if (hours < 0.0 || hours > 168.0) // validate hours worked
		{
			throw new IllegalArgumentException("Hours worked must be >=0.0 and <= 168.0");
		}
		
		this.wage = wage;
		this.hours = hours;
	}

	// return wage
	public double getWage() {
		return wage;
	}

	// set wage
	public void setWage(double wage) {
		if (wage < 0.0) // validate wage
		{
			throw new IllegalArgumentException("The hourly wage must be >=0.0");
		}
		this.wage = wage;
	}

	// return hours
	public double getHours() {
		return hours;
	}

	// set hours
	public void setHours(double hours) {
		if (hours < 0.0 || hours > 168.0) // validate hours worked
		{
			throw new IllegalArgumentException("Hours worked must be >=0.0 and <= 168.0");
		}
		this.hours = hours;
	}
	
	// calculate earnings; override abstract method earnings in Employee
	@Override
	public double earnings() {
		if (getHours() <= 40) // no overtime
		{
			return getHours() * getWage();
		}
		else
		{
			return  getWage() * 40 + (getHours() - 40) * 1.5 * getWage();
		}
	} // end class HOurlyEmployee
	
	
	
	
	
}
