package com.statefarm.chapter10_Exercises;

import java.time.LocalDate;
import java.util.Calendar;

// PayrollSystemTest.java
// Employee hierarchy test program

public class PayrollSystemTest {

	public static void main(String[] args)
	{
		
		// Date birth = new Date(3, 28, 1994);
		// create subclass objects
		SalariedEmployee salariedEmployee = new SalariedEmployee("Prudvi Krishna", "Tarugu Subbaiah", "123-45-6789", new Date(3, 28, 1994), 800);
		HourlyEmployee hourlyEmployee = new HourlyEmployee("John Snow", "Stark", "234-56-7890", new Date(1, 28, 1995) , 16.75, 40);
		CommissionEmployee commissionEmployee = new CommissionEmployee("Sansa", "Stark", "345-67-8901", new Date(5, 28, 1996), 10000, 0.06);
		BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Arya", "Stark", "456-78-9012", new Date(6, 28, 1997), 5000, 0.04, 300);
		PieceWorker pieceWorker = new PieceWorker("Mitchell", "Cornwell", "999-99-9999", new Date(1, 24, 1990), 40, 40);
		
//				System.out.println("Employees processed individually");
		
//		System.out.printf("%n%s%n%s: $%,.2f%n%n", salariedEmployee, "earned", salariedEmployee.earnings());
//		System.out.printf("%n%s%n%s: $%,.2f%n%n", hourlyEmployee, "earned", hourlyEmployee.earnings());
//		System.out.printf("%n%s%n%s: $%,.2f%n%n", commissionEmployee, "earned", commissionEmployee.earnings());
//		System.out.printf("%n%s%n%s: $%,.2f%n%n", basePlusCommissionEmployee, "earned", basePlusCommissionEmployee.earnings());
		
		// create four-element Employee array
		Employee[] employees = new Employee[4];
		
		// initialize array with employees
		employees[0] = salariedEmployee; 
		employees[1] = hourlyEmployee;
		employees[2] = commissionEmployee;
		employees[3] = basePlusCommissionEmployee;
		
		
		System.out.printf("Employees processed polymorphically:%n%n");
		
		// generically process each element in array employees
//		for (Employee currentEmployee : employees)
//		{
//			System.out.println(currentEmployee); // invokes toString
//			
//			// determine whether element is a BasePlusCommissionEmployee
//			if (currentEmployee instanceof BasePlusCommissionEmployee)
//			{
//				// downcast Employee reference to BasePluscommissionEMployee reference
//				BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;
//				employee.setBaseSalary(1.10 * employee.getBaseSalary());
//				
//				System.out.printf("New base salary with 10%% increase is: $%,.2f%n", employee.getBaseSalary());
//			} // end if
//			
//			System.out.printf("earned $%,.2f%n%n", currentEmployee.earnings() );
//			
//		} // end for
//	
	
		
		int month = Calendar.getInstance().get(Calendar.MONTH); // Returns current month
		int currentMonth = month + 1; // Calendar.MONTH gives from index 0; SO Jabuary would be 0; So, + 1.
	
		// loop through employees 
		for (Employee currentEmployee : employees)
		{
			int birthMonth = currentEmployee.getBirthDate().getMonth();
		
			// If birth day month is equals to current month, add $100 bonus
			if (birthMonth == currentMonth)
			{
				System.out.printf("%n%s%n%s: $%,.2f%n%n", currentEmployee, "earned", currentEmployee.earnings() + 100);
				
			}
			else System.out.printf("%n%s%n%s: $%,.2f%n%n", currentEmployee, "earned", currentEmployee.earnings());
	}
		
		// get type name of each object in employees array
		for (int j = 0; j < employees.length; j++)
		{
			System.out.printf("Employee %d is %s%n", j, employees[j].getClass().getName());
		}
		
		    Employee[] employees2 = new Employee[5];
			
			// initialize array with employees
			employees2[0] = salariedEmployee; 
			employees2[1] = hourlyEmployee;
			employees2[2] = commissionEmployee;
			employees2[3] = basePlusCommissionEmployee;
			employees2[4] = pieceWorker;
			
			for (Employee currentEmployee : employees2)
			{
				System.out.printf("%n%s%n%s: $%,.2f%n%n", currentEmployee, "earned", currentEmployee.earnings());
			}
		
		
	}
			
}
