package com.statefarm.chapter10_Exercises;

// BasePlusCommissionEmployeeInheritance.java

public class BasePlusCommissionEmployee extends CommissionEmployee {

	private double baseSalary; // base salary per week
	
	// six-argument constructor
			public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber, Date birthDate, double grossSales,
					double commissionRate, double baseSalary) {

				super(firstName, lastName, socialSecurityNumber, birthDate, grossSales, commissionRate);
				
				// if baseSalary is invalid throw exception
				if (baseSalary < 0.0) {
					throw new IllegalArgumentException("Base Salary must be >= 0.0");
				}

				this.baseSalary = baseSalary;
			} // end of constructor
			
			// return base salary
			public double getBaseSalary() {
				return baseSalary;
			}

			// set base salary
			public void setBaseSalary(double baseSalary) {
				if (baseSalary < 0.0) {  // validate base salary
					throw new IllegalArgumentException("Base Salary must be >= 0.0");
				}
				this.baseSalary = baseSalary;
			}

			// calculate earnings; override method earnings in commissionEmployee
			@Override
			public double earnings()
			{
				// return baseSalary + (getCommissionRate() * getGrossSales());
				return getBaseSalary() + super.earnings();
			}
			
			// return String representation of BasePlusCommissionEMployee object
			@Override
			public String toString()
			{
				return String.format("%s %s; %s: $%,.2f",
						"Base-salaried", super.toString(), "Base Salary", getBaseSalary());
			}
} // end class BasePlusCommissionEmployeeInheritance
