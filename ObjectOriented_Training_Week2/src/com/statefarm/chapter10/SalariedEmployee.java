package com.statefarm.chapter10;

// SalariedEmployee.java
// SalariedEmployee concrete class extends Employee abstract class

public class SalariedEmployee extends Employee {

	private double weeklySalary;
	
	// constructor
	public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber, double weeklySalary)
	{
				
		super(firstName, lastName, socialSecurityNumber);
		
		if (weeklySalary < 0.0)
		{
			throw new IllegalArgumentException("weekly salary must be >= 0.0");
		}
		this.weeklySalary = weeklySalary;
	}
	
	// set salary
	public void setWeeklySalary(double weeklySalalry)
	{
		if (weeklySalary < 0.0)
		{
			throw new IllegalArgumentException("weekly salary must be >= 0.0");
		}
		this.weeklySalary = weeklySalary;
	}
	
	// return salary
	public double getWeeklySalary()
	{
		return weeklySalary;
	}
	
	// calculate earnings; override abstract method earnings in Employee
	@Override
	public double earnings()
	{
		return getWeeklySalary();
	}
	
	// return string representation of SalariedEmployee object
	@Override
	public String toString()
	{
		return String.format("Salaried Employee: %s%n%s: $%,.2f", super.toString(), "Weekly Salary", getWeeklySalary());
	}
	
}
