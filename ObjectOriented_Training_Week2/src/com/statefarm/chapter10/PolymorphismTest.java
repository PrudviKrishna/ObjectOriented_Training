package com.statefarm.chapter10;

// PolymorphismTest.java
// Assigning super class and subclass variables to super class and sub class variables

public class PolymorphismTest {

	public static void main(String[] args)
	{
	// assign super class reference to super class variable
	CommissionEmployee commissionEmployee = new CommissionEmployee("Prudvi Krishna", "Tarugu Subbaiah", 
			"123-45-6789", 10000, 0.06);
	
	// assign sub class reference to sub class variable
	BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Mitchell", "Cornwell", 
			"987-65-4321", 15000, 0.08, 500);
	
	// invoke toString on super class object using super class variable
   System.out.printf("%s %s:%n%n%s%n%n","Call CommissionEMployee's toString","with super class reference to superclass object", 
		   commissionEmployee.toString() );
	
	// invoke toString on sub class object using sub class variable
   System.out.printf("%s %s:%n%n%s%n%n","Call BasePlusCommissionEmployee's toString","with sub class reference to sub class object", 
		   basePlusCommissionEmployee.toString() );
   
	// invoke toString on sub class object using super class variable
   CommissionEmployee commissionEmployee2 = basePlusCommissionEmployee;
   System.out.printf("%s %s:%n%n%s%n%n","Call BasePlusCommissionEmployee's toString","with super class reference to sub class object", 
		   commissionEmployee2.toString() );
}

}