package com.statefarm.chapter10.Payable_Interface;

// Payable.java
// Payable interface declaration

public interface Payable {

	double getPaymentAmount(); // calculate payment; no implementation
}
