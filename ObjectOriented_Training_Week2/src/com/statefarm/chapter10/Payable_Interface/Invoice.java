package com.statefarm.chapter10.Payable_Interface;

// Invoice.java
// Invoice class that implements Payable interface

public class Invoice implements Payable {

	private final String partNumber;
	private final String partDescription;
	private int quantity;
	private double pricePerItem;
	
	// constructor
	public Invoice(String partNumber, String partDescription, int quantity, double pricePerItem)
	{
		
		if (quantity < 0) // validate quantity
		{
			throw new IllegalArgumentException("Quantity must be >= 0");
		}
		
		if (pricePerItem < 0.0) // validate pricePerItem
		{
			throw new IllegalArgumentException("Price per Item must be >= 0");
		}
		
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.quantity = quantity;
		this.pricePerItem = pricePerItem;
		
	} // end constructor

	// get quantity
	public int getQuantity() {
		return quantity;
	}

	// set quantity
	public void setQuantity(int quantity) {
		if (quantity < 0) // validate quantity
		{
			throw new IllegalArgumentException("Quantity must be >= 0");
		}
		this.quantity = quantity;
	}

	// get price per item
	public double getPricePerItem() {
		return pricePerItem;
	}

	// set price per item
	public void setPricePerItem(double pricePerItem) {
		
		if (pricePerItem < 0.0) // validate pricePerItem
		{
			throw new IllegalArgumentException("Price per Item must be >= 0");
		}
		
		this.pricePerItem = pricePerItem;
	}

	// get part number
	public String getPartNumber() {
		return partNumber;
	}

	// get description
	public String getPartDescription() {
		return partDescription;
	}
	
	// return String representation of Invoice object
	@Override
	public String toString()
	{
		return String.format("%s: %n%s: %s%n%s: %s%n%s: %d%n%s: $%,.2f%n%n","Invoice", "Part Number", getPartNumber(), 
				"Part Description", getPartDescription(), "Quantity",getQuantity(), "Price per Item", getPricePerItem());
	}

	// method required to carry out contract with interface Payable
	@Override
	public double getPaymentAmount() {
		return getQuantity() * getPricePerItem(); // calculate total cost
	}
	
} // end class Invoice
