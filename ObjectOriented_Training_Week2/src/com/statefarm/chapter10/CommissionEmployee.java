package com.statefarm.chapter10;

// CommissionEmployee.java
// CommissionEmployee class represents an employee paid a percentage of gross sales

public class CommissionEmployee extends Employee {

	private double grossSales; // gross weekly sales
	private double commissionRate; // commission percentage

	// five-argument constructor
	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
			double commissionRate) {

		super(firstName, lastName, socialSecurityNumber);
		
		// if grossSales is invalid throw exception
		if (grossSales < 0.0) {
			throw new IllegalArgumentException("Gross sales must be >= 0.0");
		}

		// if commissionRate is invalid throw exception
		if (commissionRate < 0.0 || commissionRate >= 1.0) {
			throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
		}

		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	} // end of constructor

	// return gross sales amount
	public double getGrossSales() {
		return grossSales;
	}

	// set gross sales amount
	public void setGrossSales(double grossSales) {
		
		if (grossSales < 0.0) {
			throw new IllegalArgumentException("Gross sales must be >= 0.0");
		}
		this.grossSales = grossSales;
	}

	// return commission rate
	public double getCommissionRate() {
		return commissionRate;
	}

	// set commission rate
	public void setCommissionRate(double commissionRate) {
		
		if (commissionRate < 0.0 || commissionRate >= 1.0) {
			throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
		}
		this.commissionRate = commissionRate;
	}
	
	// calculate earnings; override abstract method earnings in Employee
	@Override
	public double earnings()
	{
		return getCommissionRate() * getGrossSales();
	}
	
	// return string representation of CommissionEmployee Object
	@Override
	public String toString()
	{
		return String.format("%s: %s%n%s: $%,.2f; %s:  %.2f", 
				"Commission Employee", super.toString(),
				"Gross sales", getGrossSales(), "Commission rate", getCommissionRate());
	}
} // end class CommissionEmployee
