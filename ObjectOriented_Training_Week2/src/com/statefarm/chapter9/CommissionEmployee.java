package com.statefarm.chapter9;

// CommissionEmployee.java
// CommissionEmployee class represents an employee paid a percentage of gross sales

public class CommissionEmployee {

	private String firstName;
	private String lastName;
	private String socialSecurityNumber;
	private double grossSales; // gross weekly sales
	private double commissionRate; // commission percentage

	// five-argument constructor
	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
			double commissionRate) {

		// implicit call to object's default constructor occurs here

		// if grossSales is invalid throw exception
		if (grossSales < 0.0) {
			throw new IllegalArgumentException("Gross sales must be >= 0.0");
		}

		// if commissionRate is invalid throw exception
		if (commissionRate < 0.0 || commissionRate >= 1.0) {
			throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
		}

		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	} // end of constructor

	// return first name
	public String getFirstName() {
		return firstName;
	}

	// return last name
	public String getLastName() {
		return lastName;
	}

	// return social security number
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	// return gross sales amount
	public double getGrossSales() {
		return grossSales;
	}

	// set gross sales amount
	public void setGrossSales(double grossSales) {
		
		if (grossSales < 0.0) {
			throw new IllegalArgumentException("Gross sales must be >= 0.0");
		}
		this.grossSales = grossSales;
	}

	// return commission rate
	public double getCommissionRate() {
		return commissionRate;
	}

	// set commission rate
	public void setCommissionRate(double commissionRate) {
		
		if (commissionRate < 0.0 || commissionRate >= 1.0) {
			throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
		}
		this.commissionRate = commissionRate;
	}
	
	// calculate earnings
	public double earnings()
	{
		return commissionRate * grossSales;
		
	}
	
	// return string representation of CommissionEmployee Object
	public String toString()
	{
		return String.format("%s: %s %s%n%s: %s%n%s %.2f%n%s:  %.2f", 
				"Commission Employee", firstName, lastName, 
				"Social Security Number", socialSecurityNumber,
				"Gross sales", grossSales, "Commission rate", commissionRate);
	}
} // end class CommissionEmployee
