package com.statefarm.chapter9;

// BasePlusCommissionEmployeeInheritance.java

public class BasePlusCommissionEmployeeInheritance extends CommissionEmployee {

	private double baseSalary; // base salary per week
	
	// six-argument constructor
			public BasePlusCommissionEmployeeInheritance(String firstName, String lastName, String socialSecurityNumber, double grossSales,
					double commissionRate, double baseSalary) {

				super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
				
				// if baseSalary is invalid throw exception
				if (baseSalary < 0.0) {
					throw new IllegalArgumentException("Base Salary must be >= 0.0");
				}

				this.baseSalary = baseSalary;
			} // end of constructor
			
			// return base salary
			public double getBaseSalary() {
				return baseSalary;
			}

			// set base salary
			public void setBaseSalary(double baseSalary) {
				if (baseSalary < 0.0) {
					throw new IllegalArgumentException("Base Salary must be >= 0.0");
				}
				this.baseSalary = baseSalary;
			}

			// calculate earnings
			@Override
			public double earnings()
			{
				// return baseSalary + (getCommissionRate() * getGrossSales());
				return getBaseSalary() + super.earnings();
			}
			
			@Override
			public String toString()
			{
				return String.format("%s %s%n%s: %.2f", 
						"Base-salaried", super.toString(), "Base Salary", getBaseSalary());
			}
} // end class BasePlusCommissionEmployeeInheritance
