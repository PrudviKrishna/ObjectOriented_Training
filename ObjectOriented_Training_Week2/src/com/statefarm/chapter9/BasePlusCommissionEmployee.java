package com.statefarm.chapter9;

// BasePlusCommissionEmployee.java
// BasePlusCommissionEmployee class represents an employee who receives a base salary in addition to commission

public class BasePlusCommissionEmployee {

	private String firstName;
	private String lastName;
	private String socialSecurityNumber;
	private double grossSales; // gross weekly sales
	private double commissionRate; // commission percentage
	private double baseSalary; // base salary per week
	
	// six-argument constructor
		public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
				double commissionRate, double baseSalary) {

			// implicit call to object's default constructor occurs here

			// if grossSales is invalid throw exception
			if (grossSales < 0.0) {
				throw new IllegalArgumentException("Gross sales must be >= 0.0");
			}

			// if commissionRate is invalid throw exception
			if (commissionRate < 0.0 || commissionRate >= 1.0) {
				throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
			}
			
			// if baseSalary is invalid throw exception
			if (baseSalary < 0.0) {
				throw new IllegalArgumentException("Base Salary must be >= 0.0");
			}

			this.firstName = firstName;
			this.lastName = lastName;
			this.socialSecurityNumber = socialSecurityNumber;
			this.grossSales = grossSales;
			this.commissionRate = commissionRate;
			this.baseSalary = baseSalary;
		} // end of constructor

		// return first name
		public String getFirstName() {
			return firstName;
		}

		// return last name
		public String getLastName() {
			return lastName;
		}

		// return social security number
		public String getSocialSecurityNumber() {
			return socialSecurityNumber;
		}

		// return gross sales amount
		public double getGrossSales() {
			return grossSales;
		}
		
		// set gross sales amount
		public void setGrossSales(double grossSales) {
			
			if (grossSales < 0.0) {
				throw new IllegalArgumentException("Gross sales must be >= 0.0");
			}
			this.grossSales = grossSales;
		}

		// return commission rate
		public double getCommissionRate() {
			return commissionRate;
		}

		// set commission rate
		public void setCommissionRate(double commissionRate) {
			
			if (commissionRate < 0.0 || commissionRate >= 1.0) {
				throw new IllegalArgumentException("commission rate must be > 0.0 and < 1.0");
			}
			this.commissionRate = commissionRate;
		}

		// return base salary
		public double getBaseSalary() {
			return baseSalary;
		}

		// set base salary
		public void setBaseSalary(double baseSalary) {
			if (baseSalary < 0.0) {
				throw new IllegalArgumentException("Base Salary must be >= 0.0");
			}
			this.baseSalary = baseSalary;
		}
		
		// calculate earnings
		public double earnings()
		{
			return baseSalary + (commissionRate * grossSales);
		}
		
		@Override
		public String toString()
		{
			return String.format("%s: %s %s%n%s: %s%n%s %.2f%n%s:  %.2f%n%s: %.2f", 
					"Commission Employee", firstName, lastName, 
					"Social Security Number", socialSecurityNumber,
					"Gross sales", grossSales, "Commission rate", commissionRate, "Base Salary", baseSalary);
		}
}
