package com.statefarm.chapter9;

//BasePlusCommissionEmployeeTest.java
//BasePlusCommissionEmployee test program

public class BasePlusCommissionEmployeeTest {

	public static void main(String[] args) {
		
    // 	BasePlusCommissionEmployee employee = new BasePlusCommissionEmployee("Prudvi Krishna", "Tarugu Subbaiah", 
	//			"123-45-6789", 10000, 0.06, 300.0);
		BasePlusCommissionEmployeeInheritance employee = new BasePlusCommissionEmployeeInheritance("Prudvi Krishna", "Tarugu Subbaiah", 
				"123-45-6789", 10000, 0.06, 300.0);
		
		      // get commission employee data
				System.out.println("Employee information obtained by get methods: ");
				
				System.out.printf("%nFirst name is %s%n", employee.getFirstName());
				
				System.out.printf("Last name is %s%n", employee.getLastName());
				
				System.out.printf("Social security number is %s%n", employee.getSocialSecurityNumber());
				
				System.out.printf("Gross sales is %.2f%n", employee.getGrossSales());
				
				System.out.printf("Commission rate is %.2f%n", employee.getCommissionRate());
				
				System.out.printf("Base salary is %.2f%n%n", employee.getBaseSalary());
				
				employee.setGrossSales(5000);
				employee.setCommissionRate(0.04);
				employee.setBaseSalary(1000);
				
				System.out.printf("%s%n%s", "Updated employee information obtained by toString: ", employee);
				

	}

}
