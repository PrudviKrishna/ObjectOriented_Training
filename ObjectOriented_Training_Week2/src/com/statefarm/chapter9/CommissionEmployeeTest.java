package com.statefarm.chapter9;

// CommissionEmployeeTest.java
// CommissionEMployee test program

public class CommissionEmployeeTest {

	public static void main(String[] args) {
		
		CommissionEmployee employee = new CommissionEmployee("Prudvi Krishna", "Tarugu Subbaiah", "123-45-6789", 10000, 0.06);
		
		// get commission employee data
		System.out.println("Employee information obtained by get methods: ");
		
		System.out.printf("%nFirst name is %s%n", employee.getFirstName());
		
		System.out.printf("Last name is %s%n", employee.getLastName());
		
		System.out.printf("Social security number is %s%n", employee.getSocialSecurityNumber());
		
		System.out.printf("Gross sales is %.2f%n", employee.getGrossSales());
		
		System.out.printf("Commission rate is %.2f%n%n", employee.getCommissionRate());
		
		employee.setGrossSales(5000);
		employee.setCommissionRate(0.10);
		
		System.out.printf("%s%n%s", "Updated employee information obtained by toString: ", employee);
		

	}

}
