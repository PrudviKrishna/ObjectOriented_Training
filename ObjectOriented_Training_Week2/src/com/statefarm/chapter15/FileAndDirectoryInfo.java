package com.statefarm.chapter15;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

// FileAndDirectoryInfo.java
// File class used to obtain file and directory information

public class FileAndDirectoryInfo {

	public static void main(String[] args) throws IOException {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter file name or directory name:");
		
		// create Path object based on user input
		// Paths class has static method get which converts given string as Path.
		Path path = Paths.get(input.nextLine());
		
		// Files class has static method exists, which receives a Path and determines whether it exists on disk.
		if (Files.exists(path))   // if path exists, output info about it.
		{
			// display file or directory information
			
			// Path class method getFileName() gets the String name of the file or directory without any location information.
			System.out.printf("%n%s exists%n", path.getFileName());
			
			// Files class isDirectory method receives a path and returns a boolean indicating whether the Path represent a directory on disk.
			System.out.printf("%s a directory%n", Files.isDirectory(path)? "Is" : "Is not");
			
			// Path method isAbsolute() returns a boolean indicating whether that Path represents an absolute path to a file or directory
			System.out.printf("%s asolute path%n", path.isAbsolute()? "Is" : "Is not");
			
			// Files static method getLastModifiedTime receives a Path and returns a FileTime indicating when the file was last modified
			// The program outputs the FileTime's default String representation.
			System.out.printf("%s Last modified: %s%n",Files.getLastModifiedTime(path));
			System.out.printf("Size: %s%n", Files.size(path));
			System.out.printf("Path: %s%n", path);
			System.out.printf("Absolute path: %s%n", path.toAbsolutePath());
			
			if (Files.isDirectory(path)) // output directory listing
			{
				System.out.printf("%nDirectory Contents:%n");
				
				// object for iterating through a drectory's contents
				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
				
				for (Path p : directoryStream)
				{
					System.out.println(p);
				}
			}
			
		}
		
		else // not file or directory, output error message
		{
			System.out.printf("%s does not exist%n", path);
		}

	} // end main

} // end class FileAndDirectoryInfo
