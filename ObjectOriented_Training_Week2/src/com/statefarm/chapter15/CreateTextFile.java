package com.statefarm.chapter15;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

// CreateTextFile.java
// Writing data to a sequential text file with class Formatter

public class CreateTextFile {

	// Formatter outputs the formatted strings, similar to printf; A Formatter object can output to various locations
	// such as to command window or file.
	private static Formatter output;  // outputs text to a file
	
	public static void main(String[] args) {
		
		openFile();
		addRecords();
		closeFile();

	}
	
	// open file clients.txt
	public static void openFile()
	{ 
		try
		{
	    // if the file not exist, it will be created.
	    // If the path of the file is not specified as below then the JVM assumes that the 
		// file is in the directory from which the program was executed
		output = new Formatter("clients.txt");  // open the file
		} catch (SecurityException securityException)
		{
			System.err.println("Write permission denied. Terminating.");
			
			// This method terminates the application. An argument of 0 to the method exit indicates successful program termination.
			// A nonzero value, such as 1 in this example, normally indicates that an error has occurred.
			System.exit(1); // terminate the program
		}
		catch (FileNotFoundException fileNotFoundException)
		{
			System.err.println("Error opening file. Terminating.");
			System.exit(1); // terminate the program
		}
	}
	
	// add records to file
	public static void addRecords()
	{
		Scanner input = new Scanner(System.in);
		System.out.printf("%s%n%s%n?", "Enter account number, first name, last name and balance.", "Enter end-of-file indicator to end input.");
		
		while(input.hasNext())  // loop until end-of-file indicator
		{
			try
			{
			// output new record to file; assumes valid input
			// The record's information is output using method format. Method format outputs a formatted String 
			// to the output destination of the Formatter object - the file clients.txt
			output.format("%d %s %s %.2f%n", input.nextInt(), input.next(), input.next(), input.nextDouble());
		}
			// If the Formatter object is closed, a FormatterClosedException will be thrown
			catch (FormatterClosedException formatterClosedException)
			{
				System.err.println("Error writing to file. Terminating.");
				break;
			}
			catch (NoSuchElementException elementException)
			{
				System.err.println("Invalid input. Please try again.");
				input.nextLine(); // discrad input so user can try again
			}
			
			System.out.print("? ");	
		} // end while
	
	} // end method addRecords

	// close file
	public static void closeFile()
	{
		if(output != null)
			output.close();
	}
} // end class CreateTextFile
