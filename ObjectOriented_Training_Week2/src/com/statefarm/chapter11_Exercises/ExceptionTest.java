package com.statefarm.chapter11_Exercises;

// 11.17 - Need to come back and make sure about the question and get it done correctly

public class ExceptionTest {

	public static void main(String[] args) {
		
		try
		{
		ExceptionA a = new ExceptionA();
		}
		catch (Exception exception) // exception thrown by ExceptionA
		{
			System.err.println("Exception occurred from ExcpetionA and handled in main");
		}
		
		finally // executes regardless of what occurs in try...catch
		{
			System.err.println("Finally executed in main");
		}
	}

}
