package com.statefarm.chapter11_Exercises;

// SomeClass.java
// Class for throwing an exception in a constructor
public class SomeClass {

	private int stories;
	
	public SomeClass(int stories)
	{
		if (stories < 1)
		{
			throw new IllegalArgumentException("Stories should be more than 1 for a Sprint");
		}
		this.stories = stories;
	}

	// get stories
	public int getStories() {
		return stories;
	}

	// set stories 
	public void setStories(int stories) {
		if (stories < 1)
		{
			throw new IllegalArgumentException("Stories should be more than 1 for a Sprint");
		}
		this.stories = stories;
	}
	
	
}
