package com.statefarm.chapter11_Exercises;

// SomeClassTest.java
// This class handles the exception thrown by SomeClass constructor

public class SomeClassTest {

	public static void main(String[] args) {
		
		try
		{
		SomeClass someClass = new SomeClass(1);
		}
		catch (IllegalArgumentException exception)
		{
			System.err.printf("%s: %s%n", "Exception Occurred", exception );
		}
		

	}

}
