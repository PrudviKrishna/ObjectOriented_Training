package com.statefarm.chapter15_Exercises;

import java.io.Serializable;

// TransactionRecord.java

public class TransactionRecord implements Serializable {

	private int accountNumber;
	private double amount;
	
	public TransactionRecord()
	{
		this(0, 0.0);
	}
	
	// constructor
	public TransactionRecord(int accountNumber, double amount) {
		this.accountNumber = accountNumber;
		this.amount = amount;
	}

	// get account  number
	public int getAccountNumber() {
		return accountNumber;
	}

	// set account number
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	// get amount
	public double getAmount() {
		return amount;
	}

	// set amount
	public void setAmount(double amount) {
		this.amount = amount;
	}

}
