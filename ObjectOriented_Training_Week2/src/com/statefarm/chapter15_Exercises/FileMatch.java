package com.statefarm.chapter15_Exercises;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.IllegalFormatException;
import java.util.Scanner;

// FileMatch.java

public class FileMatch {

	// attributes 
	//private static ObjectInputStream accountInput;
	private static Scanner accountInput;
	//private static ObjectInputStream transactionInput;
	private static Scanner transactionInput;
	private static Formatter matchOutput;
	private static Formatter logFile;
	private static TransactionRecord transaction;
	private static Account account;
	
	public FileMatch()
	{
		transaction = new TransactionRecord();
		account = new Account();
	}
	
	public static void main(String[] args) {
		
		openFile();
		//readRecords();
		processFiles();
		closeFile();

	}
	
	// This method open files for reading and writing
	public static void openFile()
	{
		try
		{
			// creating an input stream to read from text file "oldmast.txt"
			//accountInput = new ObjectInputStream(Files.newInputStream(Paths.get("oldmast.txt")));
			accountInput = new Scanner(Paths.get("oldmast.txt"));
			
			// creating an input stream to read from text file "trans.txt"
			//transactionInput = new ObjectInputStream(Files.newInputStream(Paths.get("trans.txt")));
			transactionInput = new Scanner(Paths.get("trans.txt"));
			
			// creating an output stream / instance of Formatter to write to file "newmast.txt"
			matchOutput = new Formatter("newmast.txt");
			
			// creating an output stream / instance of Formatter to write to file "log.txt"
			logFile = new Formatter("log.txt");
		
		}
		// catch exception if files does not exist
		catch (IOException ioException)
		{
			System.err.println("Error opening file. Terminating.");
			System.exit(1); // terminate the program
		}
	} // end method open files
	
	// The method Process Files reads records from two files "oldmast.txt" and "trans.txt",
	// process the data and writes the data to files "newmast.txt" and "log.txt".
	public static void processFiles()
	{
		int transactionAccountNumber;
		int accountNumber;
		
		try
		{
		// get transaction record and its account number
		transaction = getTransactionRecord();
		
		// if transaction is null return nothing
		if (transaction == null)
		{
			return ;
		}
		
		transactionAccountNumber = transaction.getAccountNumber();
		
		// get an account record and its account number
		account = getAccountRecord();
		
		// if account is null return nothing
		if (account == null)
		{
			return ;
		}
		accountNumber = account.getAccount();
		
		//If account number is not null
		while (accountNumber != 0)
		{
			// If there is no transaction record in TransactionRecord for a master card write master record (Account record) to the "newmast.txt"
			while (accountNumber < transactionAccountNumber)
			{
				matchOutput.format("%d %s %s %.2f%n", account.getAccount(), account.getFirstName(), account.getLastName(), account.getBalance());
				
				// get an account record and its account number
				account = getAccountRecord();
				
				// if account is null return nothing
				if (account == null)
				{
					return ;
				}
				accountNumber = account.getAccount();
			} // end while
			
			//if there is a transaction record for master record
			if (accountNumber == transactionAccountNumber)
			{
				// combine transaction record wit account record
				account.combine(transaction);
				
				matchOutput.format("%d %s %s %.2f%n", account.getAccount(), account.getFirstName(), account.getLastName(), account.getBalance());
				
				// get a new transaction
				transaction = getTransactionRecord();
				
				// if transaction is null return nothing
				if (transaction == null)
				{
					return ;
				}
				transactionAccountNumber = transaction.getAccountNumber();
				
				// get an account record and its account number
				account = getAccountRecord();
				
				// if account is null return nothing
				if (account == null)
				{
					return ;
				}
				accountNumber = account.getAccount();
				
			} // end if
			
			// if transaction record exists and no matching master record exist; write the transaction record to log file
			while (transactionAccountNumber < accountNumber)
			{
				logFile.format("%s %d%n", "Unmatched transaction record for Account number", transaction.getAccountNumber());
				
				// get a new transaction
				transaction = getTransactionRecord();
				
				// if transaction is null return nothing
				if (transaction == null)
				{
					return ;
				}
				transactionAccountNumber = transaction.getAccountNumber();
			} // end while
			
		} // end outer while
		} // end try
		catch (IllegalFormatException formatException)
		{
			System.err.println(formatException.getMessage());
			System.exit(1);
		} // end catch
	} // end processFiles 
	
	
//	public static void readRecords()
//	{
//		// System.out.printf("%-10s%-12s%-12s%10s%n", "Account", "First Name", "Last Name", "Balance");
//		
//		while(true)
//		{
//		try
//			{
//			                  Account account = (Account) accountInput.readObject();
//			                  account.getAccount();
//			                  System.out.println( account.getAccount());
//			                  
//			                  TransactionRecord transaction = (TransactionRecord) transactionInput.readObject();
//			                  transaction.getAccountNumber();
//			                  System.out.println( transaction.getAccountNumber());
//			                  
//			                  if(account.getAccount() == transaction.getAccountNumber())
//			                  {
//			                	  System.out.println("Same account number");
//			                	  System.out.println(account.getBalance() + 1);
//			                	  account.setBalance(account.getBalance() + 1);
//			                	  transaction.setAmount(transaction.getAmount() - 1);
//			                	  System.out.println(transaction.getAmount());
//			                	  
//			                	  matchOutput.format("%d %s %s %.2f%n", account.getAccount(), account.getFirstName(), account.getLastName(), account.getBalance());
//			                  }
//			                  
////			                  if ()
////			                  {
////			                	  
////			                  }
//	//	Account record = (Account) input.readObject();
//	//	System.out.printf("%-10s%-12s%-12s%10.2f%n", record.getAccount(), record.getFirstName(), record.getLastName(), record.getBalance());
//		}
////		catch (EOFException eofException)
////		{
////			System.out.printf("No more records%n");
////		}
//		catch (ClassNotFoundException classNotFoundException)
//		{
//			System.err.print("Invalid Object type. Terminating.");
//		}
//		catch (IOException ioException)
//		{
//			System.err.println("Error reading from file. Terminating.");
//			System.exit(1); // terminate the program
//		}
//		}
//	}
	
	public static TransactionRecord getTransactionRecord()
	{
		// try to read a record
		try
		{
			if (transactionInput.hasNext())
			{
				transaction.setAccountNumber(transactionInput.nextInt());
				transaction.setAmount(transactionInput.nextDouble());
				return transaction;
			} // end if
			
			else 
			{
				while (accountInput.hasNext())
				{
					account.setAccount(accountInput.nextInt());
					account.setFirstName(accountInput.next());
					account.setLastName(accountInput.next());
					account.setBalance(accountInput.nextDouble());
					
					// store in newmast.txt file
					matchOutput.format("%d %s %s %.2f%n", account.getAccount(), account.getFirstName(), account.getLastName(), account.getBalance());
				} // end while
			} // else
		} // end try
		
		catch (IllegalFormatException formatException)
		{
			System.err.println(formatException.getMessage());
			System.exit(1);
		} // end catch
		
		return null;
	} // end method getTransactionRecord
	
	// This method gets account record
	public static Account getAccountRecord()
	{
		// try to read a record
		try
		{
			// Read the data from "oldmast.txt"
			if (accountInput.hasNext())
			{
				account.setAccount(accountInput.nextInt());
				account.setFirstName(accountInput.next());
				account.setLastName(accountInput.next());
				account.setBalance(accountInput.nextDouble());
				return account;
			} // end if
			
			else 
			{
				
				logFile.format("%s %d%n", "Unmatched transaction record for account number", transaction.getAccountNumber());
				
				// These records are without transactions without accounts
				while (transactionInput.hasNext())
				{
					transaction.setAccountNumber(transactionInput.nextInt());
					transaction.setAmount(transactionInput.nextDouble());
				} // end while
			} // else
		} // end try
		
		catch (IllegalFormatException formatException)
		{
			System.err.println(formatException.getMessage());
			System.exit(1);
		} // end catch
		
		return null;
	} // end method getAccountRecord
	
	// This method closes all the stream files that are in open state.
	public static void closeFile()
	{
		try
		{
		if (accountInput != null)
		{
			accountInput.close();
		}
		if (transactionInput != null)
		{
			transactionInput.close();
		}
		if (matchOutput != null)
		{
			matchOutput.close();
		}
		if (logFile != null)
		{
			logFile.close();
		}
		} // end try
		catch (Exception Exception)
		{
			System.err.println("Error closing file. Terminating.");
		} // end catch
	}

} // end class FileMatch
