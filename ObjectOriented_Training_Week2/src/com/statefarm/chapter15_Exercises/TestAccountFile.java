package com.statefarm.chapter15_Exercises;

// TestAccountFile.java
// This class is for testing the CreateAccountTextFile class

public class TestAccountFile {

	public static void main(String[] args) {
		
		// creating object of CreateAccountTextFile class 
		CreateAccountTextFile accountFile = new CreateAccountTextFile();
		
		// calling openFile() to open the file.
		accountFile.openFile();
		
		// calling addRecords() to add records to the file
		accountFile.addRecords();
		
		// calling closeFile() to close the file
		accountFile.closeFile();
		

	}

}
