package com.ntier.employee;

public abstract class Employee {

	// Template method is final and it contains other methods, it is concrete method
	public final void setUpLifeInsurance(int amount, String benef)
	{
		this.chooseAmount(amount);
		this.chooseBeneficiary(benef);
		this.payInitialPremium();
	}
	
	// Template methods are public and the operation methods are protected. It is not required but commonly used.
	// Because clients call only template method, not the individual operation methods.
	protected void chooseBeneficiary(String beneficiary)
	{
		if (beneficiary != null)
		{
			System.out.println("Your beneficary is " + beneficiary);
		}
		else
		{
			System.out.println("No beneficiary selected at this time");
		}
	}
	
	protected abstract void chooseAmount(int amount);
	
	protected final void payInitialPremium()
	{
		System.out.println("Initial premium paid by cash");
	}
	
	}
