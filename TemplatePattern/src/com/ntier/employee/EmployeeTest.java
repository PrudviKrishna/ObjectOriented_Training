package com.ntier.employee;

public class EmployeeTest {

	public static void main(String[] args) {
		
		Employee e1 = new HourlyEmployee();
		e1.setUpLifeInsurance(500000, "Prudvi Krishna");
		System.out.println();
		
		Employee e2 = new HourlyEmployee();
		e2.setUpLifeInsurance(500000, null);
		System.out.println();
		
		Employee e3 = new SalariedEmployee();
		e3.setUpLifeInsurance(500000, "Mitchell Cornwell");
		System.out.println();
		
		Employee e4 = new SalariedEmployee();
		e4.setUpLifeInsurance(500000, null);
		System.out.println();
	}

}
