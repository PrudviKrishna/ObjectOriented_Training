package com.ntier.employee;

public class SalariedEmployee extends Employee {

	// override abstract operation method
	protected void chooseAmount(int amount)
	{
		System.out.println("You, a wage worker, have requested" + amount + " in coverage");
		System.out.println("Ypur maximum coverage amount is 250000");
		System.out.println("Your actual covergae amount will be " + String.valueOf( amount > 250000 ? 250000 : amount));
	}
	
	// optionally override concrete operation method
	protected void chooseBeneficiary(String beneficiary)
	{
		if (beneficiary != null)
		{
			System.out.println("Your beneficary is " + beneficiary);
		}
		else
		{
			System.out.println("Your beneficiary is your estate");
		}
	}
}
