/**
 * 
 */
package com.jits.audit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author W1WG
 *
 */
public class CostAudit {
		
	private static final Logger LOG = LogManager.getLogger(CostAudit.class);
	
	// BufferedWriter to write data to a file
	private static BufferedWriter writer;

	public static void costAuditing(String id, double cost)
	{
		// Getting Parcel Id   ---- This is done in calling method
		//String id = parcel.getParcelId();
		
		// Getting cost of the Parcel  ---- This is done in calling method
		//ShippingCost shippingCost = new ShippingCost(parcel);
		//double cost = shippingCost.getShippingCost();
		
		
		// Getting TimeStamp in yyyy-MM-dd format
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		LOG.debug("Converting the date instance into required format 'yyyy-MM-dd'");
		String timeStamp = dateFormat.format(date);  // This formats the date instance into desired format
		
		String audit = String.format("%s %.2f %s", id, cost, timeStamp);
			
		try {
			// Instantiating a BufferedWriter object, to write data to a CostAuditing file.
			writer = new BufferedWriter(new FileWriter("CostAuditing.txt"));
			LOG.debug("Writing audit into a file");
			writer.write(audit);  // Writes audits to the file - CostAuditing.txt
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
		} finally {
			try
			{
			if (writer!= null)
			{
				LOG.debug("Closing the file");
				writer.close();   // closes the file
			}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error(e.getMessage());
		}
	}
}
}
