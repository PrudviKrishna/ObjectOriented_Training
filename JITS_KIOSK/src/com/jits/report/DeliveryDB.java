package com.jits.report;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import com.jits.transfer.DeliveryRequest;

/*
 * STUDENTS: In Lab 6.1, if you used XMLEncoder to persist some object type other 
 * than Confirmation, then modify this class accordingly.
 * For example, if you XMLEncoded DeliveryInformation objects, 
 * replace all references to "Confirmation" with "DeliveryInformation".
 */
public class DeliveryDB
{
  /*
   * Reads the XML files from Lab 6.1 and turns them back into the Java
   * objects they once were, returning them in a List.
   * The path parameter is the location of the XML files, e.g., "/JITS/log".
   */
  public static List decodeDeliveries(String path)
  {
    List deliveryList = new ArrayList();
    DeliveryRequest delivery = null;

    File f = new File(path);
    File[] xmlfiles = f.listFiles(new XMLFileFilter());
    for (int i = 0; i < xmlfiles.length; i++)
    {
      delivery = decode(xmlfiles[i]);
      deliveryList.add(delivery);
    }
    return deliveryList;
  }

  private static DeliveryRequest decode(File file)
  {
    XMLDecoder decoder = null;
    DeliveryRequest delivery = null;
    try
    {
      decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(file)));
      delivery = (DeliveryRequest) decoder.readObject();
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (decoder != null) decoder.close();
    }
    return delivery;
  }

  private static class XMLFileFilter
  implements FileFilter
  {
    private static final String XML = "xml";

    public boolean accept(File file)
    {
      return file.getName().endsWith(XML);
    }
  }
}