/**
 * 
 */
package com.jits.shipping;

/**
 * @author W1WG
 *  ShippingCost class calculates the cost for shipping a Parcel
 */
public class ShippingCost {

	private static final double airCostFactor = 1.75;
	private static final double groundCostFactor = 1.1;
	private static final double weight = 1.0;
	private static final double volume = 1.0;
	private Parcel parcel;
	private TimeZone timeZone;
	
	public ShippingCost(Parcel parcel)
	{
		this.parcel = parcel;
		this.timeZone = new TimeZone();
	}
	
	public double getShippingCost()
	{
		double cost = 0;
		if (getParcel().getShipment().equalsIgnoreCase("AIR"))
		{
			cost = timeZone.getZoneDifference(getParcel()) * weight * volume * airCostFactor;
		} else if (getParcel().getShipment().equalsIgnoreCase("GRD"))
		{
			cost = timeZone.getZoneDifference(getParcel()) * weight * groundCostFactor * getDiscount();
		}
		return cost;
	}
	
	public double getDiscount()
	{
		String toZip = getParcel().getDestination().getPostalCode();
		String fromZip = getParcel().getOrigin().getPostalCode();

		if (timeZone.getTimeZone(fromZip).equals("PT") || timeZone.getTimeZone(toZip).equals("PT"))
		{
			return 0.05;
		}
		else return 0;
		
	}

	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}
	
	
}
