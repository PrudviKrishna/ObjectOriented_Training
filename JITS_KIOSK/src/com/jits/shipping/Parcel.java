/**
 * 
 */
package com.jits.shipping;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.thirdParty.calibration.MailScale;

/**
 * @author W1WG
 *
 */
@XmlRootElement
public class Parcel {

	private int parcelId;
	private Address origin;
	private Address destination;
	private String shipment;
	private ParcelType type;

	public Parcel()
	{
		
	}
	
	public Parcel(int parcelId, Address origin, Address destination, String shipment, ParcelType type) {
		this.parcelId = parcelId;
		this.origin = origin;
		this.destination = destination;
		this.shipment = shipment;
		this.type = type;
	}
	
//	public double weighParcel()
//	{
//		MailScale scale = new MailScale();
//		return scale.calculateWeight(this);
//	}

	@XmlAttribute
	public int getParcelId() {
		return parcelId;
	}

	public void setParcelId(int parcelId) {
		this.parcelId = parcelId;
	}

	@XmlElement
	public Address getOrigin() {
		return origin;
	}

	public void setOrigin(Address origin) {
		this.origin = origin;
	}

	@XmlElement
	public Address getDestination() {
		return destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	@XmlElement
	public String getShipment() {
		return shipment;
	}

	public void setShipment(String shipment) {
		this.shipment = shipment;
	}

	@XmlElement
	public ParcelType getType() {
		return type;
	}

	public void setType(ParcelType type) {
		this.type = type;
	}
	
	@Override
	public String toString()
	{
		return String.format("%nParcel Id: %d%n%nOrigin Address: %s%nDestination Address: %s%nShipment Method: %s%n %s%n%n", 
				getParcelId(), getOrigin().toString(), getDestination().toString(), getShipment(), getType().toString());
	}

	
}
