/**
 * 
 */
package com.jits.shipping;

/**
 * @author W1WG
 *
 */
public class Address {

	private String name;
	private String street;
	private String city;
	private String state;
	private String postalCode;
	
	public Address(String name, String street, String city, String state, String postalCode) {
		this.name = name;
		this.street = street;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return String.format("Name: %s%n Street: %s%n City: %s%n State: %s%n Postal Code: %s%n%n", getName(), getStreet(), getCity(), getState(), getPostalCode());
	}
	
	
	
	
}
