package com.jits.shipping;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ShipmentLogging {

	//public static void main(String[] args) {
		public static void shipmentLogging(Parcel parcel) {
		
//		Parcel parcel = new Parcel();
//		parcel.setParcelId(100);
//		parcel.setOrigin(new Address("Prudvi Krishna", "6 CLobertin Court.", "Bloomington", "Illinois", "61701"));
//		parcel.setDestination(new Address("Mitchell Cornwell", "Corp South", "Bloomington", "Illinois", "61702"));
//		parcel.setShipment("GRD");
//		parcel.setType(new Letter("Plain"));
		
		try {
			File file = new File("ShipmentLogging.xml");
			JAXBContext context = JAXBContext.newInstance(Parcel.class);
			Marshaller jaxbMarshal = context.createMarshaller();
			
			jaxbMarshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshal.marshal(parcel, file);
			jaxbMarshal.marshal(parcel, System.out);
			System.out.println(parcel.toString());
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
	}

}
