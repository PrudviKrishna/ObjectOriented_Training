/**
 * 
 */
package com.jits.shipping;

/**
 * @author W1WG
 *
 */
public class Letter extends ParcelType {

	private String letterType;
	
	public Letter(String letterType)
	{
		super(0, 0, 0);
		this.letterType = letterType;
	}
	
	public String getLetterType() {
		return letterType;
	}

	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}
	
	@Override
	public String toString()
	{
		return String.format("%nParcel Type: %s%nLetter Type: %s%nHeight: %d%nWidth: %d%nDepth: %d%n", "Letter", getLetterType(), getHeight(), getWidth(), getDepth());
	}
	
	
	
}
