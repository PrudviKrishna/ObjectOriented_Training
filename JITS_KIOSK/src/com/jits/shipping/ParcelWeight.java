/**
 * 
 */
package com.jits.shipping;

/**
 * @author W1WG
 *
 */
public interface ParcelWeight {

	double weighParcel();
}
