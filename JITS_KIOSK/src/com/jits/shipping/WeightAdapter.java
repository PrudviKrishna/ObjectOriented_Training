/**
 * 
 */
package com.jits.shipping;

import com.thirdParty.calibration.MailScale;

/**
 * @author W1WG
 *
 */
public class WeightAdapter implements ParcelWeight {

	private MailScale scale;
	private Parcel parcel;
	
	public WeightAdapter(Parcel parcel)
	{
		scale = new MailScale();
		this.parcel = parcel;
	}
	
	@Override
	public double weighParcel() {
		double weight = scale.calculateWeight(parcel);	
		return Math.ceil(weight);
	}
}
