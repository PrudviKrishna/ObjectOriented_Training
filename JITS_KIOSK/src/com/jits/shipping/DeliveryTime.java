package com.jits.shipping;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.helpers.DefaultValidationEventHandler;

public class DeliveryTime {

	private int numDays;
	private TimeZone timeZone;
	private Parcel parcel;
	private static final double airZoneFactor = 0.25;
	private static final double groundZoneFactor = 1.5;
	
	public DeliveryTime(Parcel parcel)
	{
		this.parcel = parcel;
		this.timeZone = new TimeZone();
	}
	
	public double getDeliveryTime()
	{
		double numDays = 0;
		if (getParcel().getShipment().equalsIgnoreCase("AIR"))
		{
			numDays = airZoneFactor * timeZone.getZoneDifference(getParcel());
		} else if (getParcel().getShipment().equalsIgnoreCase("GRD"))
		{
			numDays = groundZoneFactor * timeZone.getZoneDifference(getParcel());
		}
		return numDays;
	}
	
	public int getNumDays() {
		return numDays;
	}

	public void setNumDays(int numDays) {
		this.numDays = numDays;
	}

	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}
}
