/**
 * 
 */
package com.jits.core;

import java.util.List;

import org.apache.log4j.Logger;
import org.safety.Scanner;

/**
 * @author W1WG
 * This class gets Hazardous materials present in parcel and adds & removes necessary precautions
 */
public class HazardousDelivery {
	
	private static final Logger LOG = Logger.getLogger(HazardousDelivery.class);
	
	private static Scanner scanner;
	//private static Parcel parcel;
	
	// No-arg constructor
	public HazardousDelivery() {}
	// This method returns all the Hazardous materials present in a package
	public static List<String> getHarzards(Parcel parcel)
	{	
		LOG.info("Instantiating Scanner class (which is provided by Ntier");
		scanner = new Scanner();
		
		LOG.debug("Invoking Scanner class scan method - which scans packages for Hazardous material");
		return scanner.scan(parcel);
	}
	
	// This method adds precautions to the Hazardous materials present in the parcel at Origin
	public static void addPrecaution(String hazard)
	{
		LOG.debug("Adding precautions for respective Hazards");
		if (hazard.equalsIgnoreCase("chemical"))
		{
			System.out.println("Applied chemical neutralizer");
		} else if (hazard.equalsIgnoreCase("medical"))
		{
			System.out.println("Sealed with medical plastic");
		} else if (hazard.equalsIgnoreCase("nuclear"))
		{
			System.out.println("Placed in a lead container");
		}
	}
	
	// This method removes precautions to the Hazardous materials present in the parcel after reaching destination
	public static void removePrecaution(String hazard)
	{
		LOG.debug("Removing precautions after reaching destination");
		if (hazard.equalsIgnoreCase("chemical"))
		{
			System.out.println("Removed chemical neutralizer");
		} else if (hazard.equalsIgnoreCase("medical"))
		{
			System.out.println("Unsealed medical plastic");
		} else if (hazard.equalsIgnoreCase("nuclear"))
		{
			System.out.println("Extracted from a lead container");
		}
	}
	
}
