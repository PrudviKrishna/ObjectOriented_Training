package com.jits.core;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.jits.transfer.DeliveryRequest;
import com.jits.transfer.IConfirmation;

public class ShipmentLogging {

	private static final Logger LOG = Logger.getLogger(ShipmentLogging.class);
	
		public static void shipmentLogging(DeliveryRequest deliveryRequest) {
			
		try {
			LOG.debug("Creating a new file (ShipmentLogging.xml) for shipment logging");
			File file = new File("ShipmentLogging.xml");
			
			LOG.debug("Creating JAXBContext object");
			JAXBContext context = JAXBContext.newInstance(DeliveryRequest.class);
			
			LOG.debug("Creating Marshaller object");
			Marshaller jaxbMarshal = context.createMarshaller();
			
			// for pretty print of XML in JAXB
			jaxbMarshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshal.marshal(deliveryRequest, file);  // write to file
			//jaxbMarshal.marshal(parcel, System.out);
			//System.out.println(parcel.toString());
			
		} catch (JAXBException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
		}
		
	}

}
