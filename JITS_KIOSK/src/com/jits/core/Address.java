/**
 * 
 */
package com.jits.core;

/**
 * @author W1WG
 *
 */
public class Address {

	// attributes
	private String name;		// name of the person
	private String street;		// Street where the person lives
	private String city;		// city where person live
	private String state;		// State where the person live
	private String postalCode;	// Postal code of the state
	
	// Parameterized constructor; Initializing the attributes
	public Address(String name, String street, String city, String state, String postalCode) {
		this.name = name;
		this.street = street;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
	}

	// get Name of the person
	public String getName() {
		return name;
	}

	// set name of the person
	public void setName(String name) {
		this.name = name;
	}

	// get street 
	public String getStreet() {
		return street;
	}

	// set street
	public void setStreet(String street) {
		this.street = street;
	}

	// get city of the address
	public String getCity() {
		return city;
	}

	// set city of the address
	public void setCity(String city) {
		this.city = city;
	}

	// get state of the address
	public String getState() {
		return state;
	}

	// set state of the address
	public void setState(String state) {
		this.state = state;
	}

	// set postal code of the address
	public String getPostalCode() {
		return postalCode;
	}

	// set postal code of the address
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return String.format("Name: %s%n Street: %s%n City: %s%n State: %s%n Postal Code: %s%n%n", getName(), getStreet(), getCity(), getState(), getPostalCode());
	}
	
	
	
	
}
