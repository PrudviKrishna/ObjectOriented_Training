//package com.jits.core;
//
//import java.util.List;
//
//public class Precaution {
//	
//	private Parcel parcel;
//	
//	public Precaution(Parcel parcel)
//	{
//		this.parcel = parcel;
//	}
//	
//	
//	
//	public static void addPrecaution(String hazard)
//	{
//		if (hazard.equalsIgnoreCase("chemical"))
//		{
//			System.out.println("Applied chemical neutralizer");
//		} else if (hazard.equalsIgnoreCase("medical"))
//		{
//			System.out.println("Sealed with medical plastic");
//		} else if (hazard.equalsIgnoreCase("nuclear"))
//		{
//			System.out.println("Placed in a lead container");
//		}
//	}
//	
//	public static void removePrecaution(String hazard)
//	{
//		if (hazard.equalsIgnoreCase("chemical"))
//		{
//			System.out.println("Removed chemical neutralizer");
//		} else if (hazard.equalsIgnoreCase("medical"))
//		{
//			System.out.println("Unsealed medical plastic");
//		} else if (hazard.equalsIgnoreCase("nuclear"))
//		{
//			System.out.println("Extracted from a lead container");
//		}
//	}
//	
//	public void apply()
//	{
//		List<String> hazards = HazardousDelivery.getHarzards(parcel);
//		for (String hzd : hazards)
//		{
//			addPrecaution(hzd);
//		}
//	}
//	
//
//}
