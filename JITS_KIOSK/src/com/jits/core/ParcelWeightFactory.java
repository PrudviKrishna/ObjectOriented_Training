/**
 * 
 */
package com.jits.core;

import org.apache.log4j.Logger;

/**
 * @author W1WG
 * This is Factory class which contains static method that returns object of the WeightAdapter class, 
 * which calculates weight of the parcel
 */
public class ParcelWeightFactory {

	private static final Logger LOG = Logger.getLogger(ParcelWeightFactory.class);
	
	public static ParcelWeight getParcelWeight(Parcel parcel){
		{
			LOG.debug("Returns/Generates WeightAdapter object dynamically at runtime.");
			return new WeightAdapter(parcel);
		}
	}
}
