/**
 * 
 */
package com.jits.core;

/**
 * @author W1WG
 * Letter is of type Parcel
 */
public class Letter extends Parcel {

	public Letter()
	{
		super.setType("Letter");
	}
	
	public Letter(String parcelId, Address origin, Address destination, String shipment, String letterType) {
		super(parcelId, origin, destination, shipment, "0", "0", "0", "Letter", letterType);
	}
	
	@Override
	public String toString()
	{
		//return String.format("%nParcel Type: %s%nLetter Type: %s%nHeight: %d%nWidth: %d%nDepth: %d%n", "Letter", getLetterType(), getHeight(), getWidth(), getDepth());
		return String.format("%s%nLetter type: %s%n", super.toString(), getLetterType());
	}
	
	
	
}
