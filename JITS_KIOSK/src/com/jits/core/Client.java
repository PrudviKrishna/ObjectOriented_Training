/**
 * 
 */
package com.jits.core;

import java.util.Scanner;

/**
 * @author W1WG
 * Clinet class - The driver class for client, it works as if the UI is console.
 */
public class Client {

	public static void shipmentProcess()
	{
		Parcel parcel = null;
		Scanner input = new Scanner(System.in);

		// Setting parcel shipment method
		System.out.print("Select type of delivery: 'AIR' / 'GRD' : "); // prompt the user for delivery type

		String shipment = input.next().toUpperCase(); // Storing the shipment method in shipment variable
		
		System.out.printf("Selected shipment method is: %s%n", shipment); 

		// Setting parcel type - box/letter and its properties
		System.out.printf("%nSelect type of parcel: 'Box' / 'Letter' : "); // Prompt user for parcel type

		String type = input.next().toLowerCase();  // storing the type in type variable
		if (type.equals("box")) {
			parcel = new Box();
			parcel.setShipment(shipment);
			System.out.printf("Enter the height of the box: "); // prompt the user to enter height of the box
			String height = input.next(); // Storing the height in height variable
			parcel.setHeight(height);
			System.out.printf("Enter the width of the box: "); // prompt the user to enter width of the box
			String width = input.next();  // Storing the width in width variable
			parcel.setWidth(width);
			System.out.printf("Enter the depth of the box: "); // prompt the user to enter depth of the box
			String depth = input.next();  // Storing the depth in depth variable
			parcel.setDepth(depth);

		} else if (type.equals("letter")) {
			parcel = new Letter();
			System.out.printf("Specify type of letter: %n1. %s%n2. %s%n3. %s%n", "Plain", "Weather-proof",
					"Fire-proof"); // prompt the user to specify type of letter
			String letterType = input.next(); // storing the type of letter in letterType varibale
			parcel.setLetterType(letterType);			
		}
		
		// Setting parcel origin address
		System.out.print("\nEnter origin address: ");  // prompt the user to enter origin address
		System.out.printf("%n%s: ", "Enter the name"); // prompt the user to enter name
		input.nextLine();
		String originName = input.nextLine(); // Storing the name in originName variable
		System.out.printf("%s: ", "Enter the street"); // prompt the user to enter street
		//input.nextLine();
		String originStreet = input.nextLine();  // Storing the street in originStreet variable
		System.out.printf("%s: ", "Enter the city");  // prompt the user to enter city
		//input.nextLine();
		String originCity = input.nextLine();  // Storing the city in originCity variable
		System.out.printf("%s: ", "Enter the state"); // prompt the user to enter state
		//input.nextLine();
		String originState = input.nextLine();  // Storing the state in originState variable
		System.out.printf("%s: ", "Enter the postal code"); // prompt the user to enter postal code
		String originPostalCode = input.next();  // Storing the postal code in originPostalCode variable

		// Generating origin address by instantiating address object with the user entered values
		Address origin = new Address(originName, originStreet, originCity, originState, originPostalCode);
		parcel.setOrigin(origin);

		// Setting parcel destination address
		System.out.printf("%nEnter destination address: ");  // prompt the user to enter destination address
		System.out.printf("%n%s: ", "Enter the name");   // prompt the user to enter name
		input.nextLine();
		String destName = input.nextLine();  // Storing the name in destName variable
		System.out.printf("%s: ", "Enter the street"); // prompt the user to enter street
		//input.nextLine();
		String destStreet = input.nextLine();    // Storing the street in destStreet variable
		System.out.printf("%s: ", "Enter the city");  // prompt the user to enter city
		//input.nextLine();
		String destCity = input.nextLine();   // Storing the city in destCity variable
		System.out.printf("%s: ", "Enter the state");  // prompt the user to enter state
		//input.nextLine();
		String destState = input.nextLine();  // Storing the state in destState variable
		System.out.printf("%s: ", "Enter the postal code"); // prompt the user to enter postal code
		String destPostalCode = input.next(); // Storing the postal code in destPostalCode variable

		// Generating destination address by instantiating address object with the user entered values
		Address destination = new Address(destName, destStreet, destCity, destState, destPostalCode);
		parcel.setDestination(destination);
		
		System.out.printf("%nPlace your parcel on scale: ");
		String deliveryInfo = Scale.scale(parcel);   // Scale.scale method returns the weight, delivery time and shipping cost of the parcel
		System.out.printf("%nPlease review your order: %n%s%n", deliveryInfo); // prompt the user to review his delivery order
		System.out.printf("Do you want to process the shipment? Yes / No: "); // prompt the user for decision
		String decision = input.next();
		if (decision.equalsIgnoreCase("Yes"))
		{
			System.out.println("Shipment Confirmation:");
			//ShipmentLogging.shipmentLogging(parcel);
			System.out.println("Delivery is processed");
			System.out.println(parcel.toString());
			System.out.println(deliveryInfo);
		} else 
		{
			System.out.println("Delivery is cancelled");
		}
	}
}
