/**
 * 
 */
package com.jits.core;

/**
 * @author W1WG
 * ParcelWeight interface have one method which the implemented classes must adhere to.
 */
public interface ParcelWeight {

	double weighParcel();	// To calculate weight of the parcel
}
