/**
 * 
 */
package com.jits.core;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

import com.thirdParty.calibration.MailScale;

/**
 * @author W1WG
 *
 */
@XmlRootElement
public class Parcel {

	//private static final Logger LOG = Logger.getLogger(Parcel.class);
	
	// attributes
	private String parcelId;   // Id of the parcel
	private Address origin;		// origin address of the parcel
	private Address destination; // destination of the parcel
	private String shipment;	// shipment method of the parcel
	private String height;		// height of the parcel
	private String width;		// width of the parcel
	private String depth;		// depth of the parcel
	private String type;		// type of the parcel
	private String letterType;  // letter type

	public Parcel()
	{
		
	}
	
	// constructor
	public Parcel(String parcelId, Address origin, Address destination, String shipment, String height, String width, String depth, String type, String letterType) {
		this.parcelId = parcelId;
		this.origin = origin;
		this.destination = destination;
		this.shipment = shipment;
		this.height = height;
		this.width = width;
		this.depth = depth;
		this.type = type;
		this.letterType = letterType;
	}
	
	// get parcel id
	@XmlAttribute
	public String getParcelId() {
		return parcelId;
	}

	// set parcel id
	public void setParcelId(String parcelId) {
		this.parcelId = parcelId;
	}

	// get origin address of the parcel
	@XmlElement
	public Address getOrigin() {
		return origin;
	}

	// get origin address of the parcel
	public void setOrigin(Address origin) {
		this.origin = origin;
	}

	// get destination address of the parcel
	@XmlElement
	public Address getDestination() {
		return destination;
	}

	// get destination address of the parcel
	public void setDestination(Address destination) {
		this.destination = destination;
	}

	// get shipment of parcel
	@XmlElement
	public String getShipment() {
		return shipment;
	}

	// set shipment of the parcel
	public void setShipment(String shipment) {
		this.shipment = shipment;
	}
	
	// get height of the parcel
	@XmlElement
	public String getHeight() {
		return height;
	}

	// set height of the parcel
	public void setHeight(String height) {
		this.height = height;
	}

	// get width of the parcel
	@XmlElement
	public String getWidth() {
		return width;
	}

	// set width of the parcel
	public void setWidth(String width) {
		this.width = width;
	}

	// get depth of the depth
	@XmlElement
	public String getDepth() {
		return depth;
	}

	// set depth of the parcel
	public void setDepth(String depth) {
		this.depth = depth;
	}

	// get type of the parcel
	@XmlElement
	public String getType() {
		return type;
	}

	// set type of the parcel
	public void setType(String type) {
		this.type = type;
	}
	
	// get letter type
	public String getLetterType() {
		return letterType;
	}

	// set letter type
	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}
	
	@Override
	public String toString()
	{
		return String.format("%nParcel Id: %s%n%nOrigin Address: %s%nDestination Address: %s%nShipment Method: %s%n"
				+ "Shipment Type: %s%nWidth: %s%nHeight: %s%nDepth: %s%n", getParcelId(), getOrigin().toString(), getDestination().toString(), getShipment(), getType(), getWidth(), getHeight(), getDepth());
	}

	
}
