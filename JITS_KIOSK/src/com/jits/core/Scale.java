/**
 * 
 */
package com.jits.core;

import org.apache.log4j.Logger;

/**
 * @author W1WG
 * This class has scale() which calculates delivery time, weight and shipping cost of the parcel.
 */
public class Scale {

	private static final Logger LOG = Logger.getLogger(Scale.class);
	
	public static String scale(Parcel parcel)
	{
		LOG.debug("In Scale class - Getting weight of the parcel");
		ParcelWeight parcelWeight = ParcelWeightFactory.getParcelWeight(parcel);
		double weight = parcelWeight.weighParcel();
		
		LOG.debug("In Scale class - Getting delivery time of the parcel");
		DeliveryTime deliveryTime = new DeliveryTime(parcel);
		double time = deliveryTime.getDeliveryTime();
		
		LOG.debug("In Scale class - Getting shipping cost of the parcel");
		ShippingCost shippingCost = new ShippingCost(parcel);
		double cost = shippingCost.getShippingCost();
		
		return String.format("%nParcel weight: %.2f%nDelivery Time: %.2f%nShipping Cost: %.2f%n", weight, time, cost);
	}
}
