/**
 * 
 */
package com.jits.core;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author W1WG
 *
 */
public class TimeZone {
	
	private static final Logger LOG = Logger.getLogger(TimeZone.class);
	
	private static Map<String, Integer> timeZone = new HashMap<>();
	
	static {
		LOG.debug("Loading TimeZone Map");
		loadTimeZoneMap();
	}
	
	public static void loadTimeZoneMap()
	{
		timeZone.put("ET", 0);
		timeZone.put("CT", 1);
		timeZone.put("MT", 2);
		timeZone.put("PT", 3);
	}
	
	public String getTimeZone(String zip)
	{
		String timeZone = null;
		switch (zip.substring(0, 1)) {
		case "0": case "1" : case "2":
			timeZone = "ET";
			break;
		case "3": case "4" : case "5":
			timeZone = "CT";
			break;
		case "6": case "7":
			timeZone = "MT";
			break;
		case "8": case "9":
			timeZone = "ET";
			break;
		default: 
			timeZone = "PT";
			break;
		}
		
		return timeZone;
	}
	
	public int getZoneDifference(Parcel parcel)
	{
		LOG.debug("Getting toZip of the parcel in TimeZone class to calculate zoneDifference");
		String toZip = parcel.getDestination().getPostalCode();
		
		LOG.debug("Getting fromZip of the parcel in TimeZone class to calculate zoneDifference");
		String fromZip = parcel.getOrigin().getPostalCode();
		
		LOG.debug("Getting TimeZone of toZip to calculate Zone difference");
		String toTimeZone = getTimeZone(toZip);
		LOG.debug("Getting TimeZone of fromZip to calculate Zone difference");
		String fromTimeZone = getTimeZone(fromZip);
		
		int zoneDiff = Math.abs(timeZone.get(fromTimeZone) - timeZone.get(toTimeZone));
		return zoneDiff;
	}
	
	public static String getRailZone(String zip)
	{
		String railZone = null;
		int firstDigit = Integer.parseInt(zip.substring(0, 1));
		
		if (0 <= firstDigit && firstDigit <= 4)
		{
			railZone = "east";
		} else if (5 <= firstDigit && firstDigit <= 9 )
		{
			railZone = "west";
		}
		return railZone;
	}
}
