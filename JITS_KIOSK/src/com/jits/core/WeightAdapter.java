/**
 * 
 */
package com.jits.core;

import org.apache.log4j.Logger;

import com.thirdParty.calibration.MailScale;

/**
 * @author W1WG
 * WeightAdapter class calculates weight of the parcel
 * This class is the application of Adapter pattern and also delegation (in weighPaarcel method)
 */
public class WeightAdapter implements ParcelWeight {

	private static final Logger LOG = Logger.getLogger(WeightAdapter.class);
	
	private MailScale scale = new MailScale();
	private Parcel parcel;
	
	public WeightAdapter(Parcel parcel)
	{
		this.parcel = parcel;
	}
	
	@Override
	public double weighParcel() {
		LOG.debug("Invoking/Delegating scale method of MailScale class to weigh parcel weight");
		double weight = scale.calculateWeight(parcel);	// Delegating  the weight calculation 
		return Math.ceil(weight);
	}
}
