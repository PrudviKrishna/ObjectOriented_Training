package com.jits.core;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.helpers.DefaultValidationEventHandler;

import org.apache.log4j.Logger;

// This class calculates the delivery time of the parcel
public class DeliveryTime {

	private static final Logger LOG = Logger.getLogger(DeliveryTime.class);
	
	// attributes
	private int numDays;
	private TimeZone timeZone;
	private Parcel parcel;
	private static final double airZoneFactor = 0.25;		// zone factor of the air shipment method
	private static final double groundZoneFactor = 1.5;		// zone factor of the ground shipment method
	
	public DeliveryTime(Parcel parcel)
	{
		this.parcel = parcel;
		this.timeZone = new TimeZone();
	}
	
	// This method calculates delivery time of the parcel
	public double getDeliveryTime()
	{
		double numDays = 0;
		String shipMethod = getParcel().getShipment();
		if (shipMethod.equalsIgnoreCase("A"))
		{
			LOG.debug("Calculating delivery time for Air shipment method");
			numDays = airZoneFactor * timeZone.getZoneDifference(getParcel());
		} else if (shipMethod.equalsIgnoreCase("G"))
		{
			LOG.debug("Calculating delivery time for Ground shipment method");
			numDays = groundZoneFactor * timeZone.getZoneDifference(getParcel());
		}
		return numDays;
	}
	
	// get delivery time / number of days for delivery
	public int getNumDays() {
		return numDays;
	}

	// set delivery time / number of days for delivery
	public void setNumDays(int numDays) {
		this.numDays = numDays;
	}

	// get parcel
	public Parcel getParcel() {
		return parcel;
	}

	// set parcel
	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}
}
