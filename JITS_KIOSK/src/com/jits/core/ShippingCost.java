/**
 * 
 */
package com.jits.core;

import org.apache.log4j.Logger;

/**
 * @author W1WG
 *  ShippingCost class calculates the cost for shipping a Parcel
 */
public class ShippingCost {

	private static final Logger LOG = Logger.getLogger(ShippingCost.class);
	
	private static final double airCostFactor = 1.75;
	private static final double groundCostFactor = 1.1;
	private static final double weight = 1.0;
	private static final double volume = 1.0;
	private Parcel parcel;
	private TimeZone timeZone;
	
	public ShippingCost(Parcel parcel)
	{
		this.parcel = parcel;
		LOG.debug("Instantiating TimeZone object");
		this.timeZone = new TimeZone();
	}
	
	public double getShippingCost()
	{
		double cost = 0;
		String shipMethod = getParcel().getShipment();

		if (shipMethod.equalsIgnoreCase("A"))
		{
			LOG.debug("Calculating shipping cost for Air shipment method");
			cost = timeZone.getZoneDifference(getParcel()) * weight * volume * airCostFactor;
		} else if (shipMethod.equalsIgnoreCase("G"))
		{
			LOG.debug("Calculating shipping cost for Ground shipment method");
			cost = timeZone.getZoneDifference(getParcel()) * weight * groundCostFactor * getDiscount();
		}
		return cost;
	}
	
	public double getDiscount()
	{
		LOG.debug("Getting toZip of the parcel to calculate discount in ShippingCost class");
		String toZip = getParcel().getDestination().getPostalCode();
		
		LOG.debug("Getting fromZip of the parcel to calculate discount in ShippingCost class");
		String fromZip = getParcel().getOrigin().getPostalCode();

		if (timeZone.getTimeZone(fromZip).equals("PT") || timeZone.getTimeZone(toZip).equals("PT"))
		{
			return 0.05;
		}
		else return 0;
		
	}

	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}	
}
