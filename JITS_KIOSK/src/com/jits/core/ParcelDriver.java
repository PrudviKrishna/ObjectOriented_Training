/**
 * 
 */
package com.jits.core;

import java.util.List;

import com.jits.report.DeliveryDB;
import com.jits.transfer.DeliveryRequest;

/**
 * @author W1WG
 *
 */
public class ParcelDriver {

	public static void main(String[] args) {

		String path = "ShipmentLogging.xml";
		//Client.shipmentProcess();
		List<DeliveryRequest> deliveries = DeliveryDB.decodeDeliveries(path);
		
		for (DeliveryRequest delivery : deliveries)
		{
			System.out.println(delivery);
		}

	}
}
