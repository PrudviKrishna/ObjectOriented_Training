/**
 * 
 */
package com.jits.core;

/**
 * @author W1WG
 * Box is type of the parcel
 */
public class Box extends Parcel{
	
	// attribute initialized with false
	private String insured = "false";
	
	public Box()
	{
		super.setType("Box");
	}
	
	public Box(String parcelId, Address origin, Address destination, String shipment, String height, String width, String depth) {
		super(parcelId, origin, destination, shipment, height, width, depth, "Box", null);
	}
	
	// get parcel is insured
	public String getInsured() {
		return insured;
	}

	// set parcel insured
	public void setInsured(String insured) {
		this.insured = insured;
	}
	
	@Override
	public String toString()
	{
		return super.toString();
	}
}
