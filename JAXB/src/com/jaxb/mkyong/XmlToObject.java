package com.jaxb.mkyong;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlToObject {

	public static void main(String[] args) {
		
		try {
			File file = new File("file.xml");
			JAXBContext context = JAXBContext.newInstance(Customer.class);
			
			Unmarshaller unMarshal = context.createUnmarshaller();
			Customer customer = (Customer) unMarshal.unmarshal(file);
			System.out.println(customer	);
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
