package com.jaxb.javatpoint;

public class Answer {

	private int id;
	private String answerName;
	private String postedby;
	
	public Answer() {
		
	}
	
	public Answer(int id, String answerName, String postedby) {
		this.id = id;
		this.answerName = answerName;
		this.postedby = postedby;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAnswerName() {
		return answerName;
	}
	public void setAnswerName(String answerName) {
		this.answerName = answerName;
	}
	public String getPostedby() {
		return postedby;
	}
	public void setPostedby(String postedby) {
		this.postedby = postedby;
	}

}
