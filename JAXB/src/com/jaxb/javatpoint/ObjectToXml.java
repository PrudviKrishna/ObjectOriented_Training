package com.jaxb.javatpoint;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ObjectToXml {

	public static void main(String[] args) throws JAXBException, FileNotFoundException
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(Question.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		Answer ans1 = new Answer(101, "java is a programming language", "Prudvi");
		Answer ans2 = new Answer(102, "java is a platform", "Mitchell");
		
		ArrayList<Answer> list = new ArrayList<Answer>();
		list.add(ans1);
		list.add(ans2);
		
		Question que = new Question(1, "What is Java", list);
		
		jaxbMarshaller.marshal(que, new FileOutputStream("question.xml"));
		
	}
	
}
