package com.sample.payment_strategy;

public class ShoppingCartTest {

	public void main(String[] args)
	{
		ShoppingCart cart = new ShoppingCart();
		
		Item item1 = new Item("1234",10);
		Item item2 = new Item("5678",40);
		
		cart.addItem(item1);
		cart.addItem(item2);
		
		// pay by credit card
		cart.Pay(new CreditCardStrategy("Prudvi", "123456789", "829", "03281994"));
		
		// pay by pay pal
		cart.Pay(new PaypalStrategy("myemail@example.com", "mypwd"));
	}

}
