package com.sample.payment_strategy;

public interface PaymentStrategy {

	 void pay(int amount);
}
