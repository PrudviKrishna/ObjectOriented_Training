/**
 * 
 */
package com.statefarm.books;

import java.util.Date;

/**
 * @author W1WG
 *
 */
public class ElectronicBook extends Book {
	
	private Date dateSent;
	
	public ElectronicBook(String title, String author, Date dateSent) {
		super(title, author);
		this.dateSent = dateSent;
	}

	// get the date on which the electronic book sent
	public Date getDateSent() {
		return dateSent;
	}

	// set the date on which the electronic book sent
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}	
	
	public String getStatus()
	{
		return "transmitted";
	}
	
	// This method returns the price of the ElectronicBook
	@Override
	public double calculatePrice() {
		return 1;
	}
	
	@Override
	public String toString()
	{
		return String.format("%sdateSent: %s%n", super.toString(), getDateSent());
	}
}
