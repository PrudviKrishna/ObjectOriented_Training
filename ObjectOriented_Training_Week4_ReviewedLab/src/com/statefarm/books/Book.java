/**
 * 
 */
package com.statefarm.books;

/**
 * @author W1WG
 *
 */
public class Book implements BookStore {

	private String title; // Book title
	private String author; // Author of the book
	
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}

	// get book title
	public String getTitle() {
		return title;
	}

	// set book title
	public void setTitle(String title) {
		this.title = title;
	}

	// get book author
	public String getAuthor() {
		return author;
	}

	// set book author
	public void setAuthor(String author) {
		this.author = author;
	}

	// Calculate price of the book
	@Override
	public double calculatePrice() {

		return getAuthor().length();
	}
	
	@Override
	public String toString()
	{
		return String.format("Title: %s%nAuthor: %s%n", getTitle(), getAuthor());
	}
}
