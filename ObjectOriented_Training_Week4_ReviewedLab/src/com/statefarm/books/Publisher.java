/**
 * 
 */
package com.statefarm.books;

/**
 * @author W1WG
 *
 */
public class Publisher {
	
	private String name;  // publisher name
	private String address; // publisher address
	
	public Publisher(String name, String address) {
		this.name = name;
		this.address = address;
	}

	// get publisher name
	public String getName() {
		return name;
	}

	// set publisher name
	public void setName(String name) {
		this.name = name;
	}

	// get publisher address
	public String getAddress() {
		return address;
	}

	// set publisher address
	public void setAddress(String address) {
		this.address = address;
	}
}
