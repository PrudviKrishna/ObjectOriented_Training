/**
 * 
 */
package com.statefarm.books;

/**
 * @author W1WG
 *
 */
public class Author {
	// Attributes; author has first name, last name and city
	private String firstName;
	private String lastName;
	private String city;
	
	// constructor
	public Author(String firstName, String lastName, String city) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
	}

	// get first name
	public String getFirstName() {
		return firstName;
	}

	// set first name
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	// get last name
	public String getLastName() {
		return lastName;
	}

	// set last name
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	// get city
	public String getCity() {
		return city;
	}

	// set city
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
