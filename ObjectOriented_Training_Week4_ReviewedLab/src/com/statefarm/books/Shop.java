/**
 * 
 */
package com.statefarm.books;

import java.util.ArrayList;
import java.util.List;

/**
 * @author W1WG
 *
 */
public class Shop {

	private static List<Book> books = new ArrayList<Book>();;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Author author1 = new Author("Kathy", "Sierra", "Bloomington");
//		Author author2 = new Author("Paul", "Deitel", "Richardson");
		
		books.add(new Book("Head First Design Patters", "Kathy Sierra"));
		books.add(new Book("Java How To Program 10 Edition", "Paul Deitel"));
		
		books.add(new Magazine("The Next President", "Joe Klein", "Time"));
		books.add(new Magazine("Tech's Top Investors", "Prudvi Krishna", "Forbes"));
		
		books.add(new ElectronicBook("Java Programming", "Dillon Wallin", null));
		books.add(new ElectronicBook("Spring Hateoas", "Zach Benchley", null));
		
		double price = 0;
		for (Book book : books)
		{
			price += book.calculatePrice();
			System.out.println(book.toString());
		}
		
		System.out.printf("Total price is: %.2f", price);
		

	}

}
