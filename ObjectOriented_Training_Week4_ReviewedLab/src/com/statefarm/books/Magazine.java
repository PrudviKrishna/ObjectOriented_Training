/**
 * 
 */
package com.statefarm.books;

/**
 * @author W1WG
 *
 */
public class Magazine extends Book{

	private String publisher; // Publisher of the magazine
	
	public Magazine(String title, String author, String publisher) {
		super(title, author);
		this.publisher = publisher;
	}
	
	// get publisher
	public String getPublisher() {
		return publisher;
	}

	// set publisher
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	// calculate price of the magazine
	@Override
	public double calculatePrice() {

		return super.calculatePrice() * 12;
	}
		
	@Override
	public String toString()
	{
		return String.format("%sPublisher: %s%n", super.toString(), getPublisher());
	}
}