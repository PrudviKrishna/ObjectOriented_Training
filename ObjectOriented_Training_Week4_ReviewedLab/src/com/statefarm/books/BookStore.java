/**
 * 
 */
package com.statefarm.books;

/**
 * @author W1WG
 *
 */
public interface BookStore {

	double calculatePrice(); // This method calculates the price of the Book / Magazine / ElectronicBook
		
}
