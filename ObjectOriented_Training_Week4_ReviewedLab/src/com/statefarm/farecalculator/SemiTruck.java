/**
 * 
 */
package com.statefarm.farecalculator;

/**
 * @author W1WG
 *
 */
public class SemiTruck extends Diesel {

	private String license;
	
	public SemiTruck(int vin, int range, String manufacturer, String color, int numWheels, int cylinders, String license) {
		super(vin, range, manufacturer, color, numWheels, cylinders);
		this.license = license;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
	
	@Override
	public boolean isLimitable()
	{
		return true;
	}

}
