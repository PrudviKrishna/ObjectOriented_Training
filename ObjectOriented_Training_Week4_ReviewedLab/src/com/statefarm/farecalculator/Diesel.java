package com.statefarm.farecalculator;

public class Diesel extends Automobile{

	private int numWheels;
	private int cylinders;
	
	public Diesel(int vin, int range,String manufacturer, String color, int numWheels, int cylinders) {
		super(vin, range, manufacturer, color);
		this.numWheels = numWheels;
		this.cylinders = cylinders;
	}

	public int getNumWheels() {
		return numWheels;
	}

	public void setNumWheels(int numWheels) {
		this.numWheels = numWheels;
	}

	public int getCylinders() {
		return cylinders;
	}

	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}

	@Override
	public String getVehicleType()
	{
		return "Diesel";
	}

	@Override
	public String displayInfo()
	{
		return String.format("%s by %s with VIN %d is available to rent in %s. This beast has a range of %d and only costs $%,.2f", 
				getVehicleType(), getManufacturer(), getVin(), getColor(), getRange(), getBasePrice());
	}
}
