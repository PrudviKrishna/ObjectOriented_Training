package com.statefarm.farecalculator;

public abstract class Automobile implements Vehicle {

	private int vin;
	private int range;
	//private double basePrice;
	private String manufacturer;
	private String color = "black";
	
	private boolean isRenewable = false;
	private boolean isLimitable = false;
	
	public Automobile(int vin, int range, String manufacturer, String color) {
		this.vin = vin;
		
		// validating range
		if (range <= 0) {
			throw new IllegalArgumentException("Range must be positive distance");
		} else if (isLimitable()) {
			if (range >= 50 && range <= 499) {
				this.range = range;
			} else
				throw new IllegalArgumentException("Vehicle range must be within 50 and 499");
		} else
			this.range = range;

		this.manufacturer = manufacturer;
		this.color = color;
	}

	public int getVin() {
		return vin;
	}

	public void setVin(int vin) {
		this.vin = vin;
	}

	public void setRange() {
		if (range <= 0) {
			throw new IllegalArgumentException("Range must be positive distance");
		} else if (isLimitable()) {
			if (range >= 50 && range <= 499) {
				this.range = range;
			} else
				throw new IllegalArgumentException("Vehicle range must be within 50 and 499");
		} else
			this.range = range;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public int getRange()
	{
		return range;
	}
	
	public double getBasePrice() {
		return 0;
	}
		
	public boolean isLimitable() {
		return isLimitable;
	}

	public boolean isRenewable() {
		return isRenewable;
	}

	public void setRenewable(boolean isRenewable) {
		this.isRenewable = isRenewable;
	}

	public void setLimitable(boolean isLimitable) {
		this.isLimitable = isLimitable;
	}

	public abstract String getVehicleType();
	public abstract String displayInfo();	
	
}
