/**
 * 
 */
package com.statefarm.farecalculator;

/**
 * @author W1WG
 *
 */
public class Yacht extends Boat {

	public Yacht(int vin, int range) {
		super(vin, range);
	}

	@Override
	public String getVehicleType() {
		return "Yacht";
	}

	@Override
	public String displayInfo() {
		return String.format("%s with VIN %d is available to rent. This beauty has a range of %d and only costs $%,.2f", getVehicleType(), getVin(), getRange(), 0.0);
	}

}
