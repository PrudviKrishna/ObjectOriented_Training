package com.statefarm.farecalculator;

public enum BoatType {

	barge(5000),
	cargo(8500),
	speed(2200),
	yacht(60000);
	
	private final int tax;
	
	BoatType(int tax)
	{
		this.tax = tax;
	}
	
}
