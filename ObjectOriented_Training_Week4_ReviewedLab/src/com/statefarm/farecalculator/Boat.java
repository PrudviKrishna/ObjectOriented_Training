package com.statefarm.farecalculator;

public abstract class Boat implements Vehicle {

	private int vin;
	private int range;
	
	public Boat(int vin, int range) {
		this.vin = vin;
		this.range = 7 * range;
	}
	
	public int getVin() {
		return vin;
	}

	public void setVin(int vin) {
		this.vin = vin;
	}

	public int getRange()
	{
		return range;
	}
	
	public void setRange(int range) {
		this.range = 7 * range;
	}

	public abstract String getVehicleType();
	public abstract String displayInfo();




}
