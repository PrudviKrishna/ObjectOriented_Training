package com.statefarm.farecalculator;

public class Electric extends Automobile {

	private int numBatteries;
	private boolean isTesla = false;
	
	public Electric(int vin, int range, String manufacturer, String color, int numBatteries) {
		super(vin, range, manufacturer, color);
		this.numBatteries = numBatteries;
	}

	public int getNumBatteries() {
		return numBatteries;
	}

	public void setNumBatteries(int numBatteries) {
		this.numBatteries = numBatteries;
	}

	public boolean isTesla() {
		boolean result = getManufacturer().equalsIgnoreCase("Tesla");
		return result;
	}

	public void setTesla(boolean isTesla) {
		this.isTesla = isTesla;
	}
	
	@Override
	public boolean isLimitable() {
		return true;
	}
	
	@Override
	public boolean isRenewable() {
		return true;
	}
	
	@Override
	public String getVehicleType()
	{
		return "Electric";
	}

	@Override
	public String displayInfo()
	{
		return String.format("%s by %s with VIN %d is available to rent in %s. This beast has a range of %d and only costs $%,.2f", 
				getVehicleType(), getManufacturer(), getVin(), getColor(), getRange(), getBasePrice());
	}

}
