package com.statefarm.farecalculator;

public class VehicleTest {

	public static void main(String[] args) {
		
//		Automobiles automobile = new Standard(123, 500, "Ford", "red", "2/20", 4);
//		
//		//System.out.println(automobile.getBasePrice());
//		System.out.println(automobile.getColor());
//		System.out.println(automobile.getRange());
		 Automobile auto = new Electric(456, 450, "Tesla", "Black", 6);
		 
		 System.out.println(auto.getColor());
		 System.out.println(auto.getManufacturer());
		 System.out.println(auto.isLimitable());
		 System.out.println(auto.getRange());
		 System.out.println(auto.displayInfo());

		 Boat boat = new Speed(234, 500);
		 System.out.println(boat.getRange());
		 System.out.println(boat.getVehicleType());
		 System.out.println(boat.getVin());
		 System.out.println(boat.displayInfo());
	}

}
