package com.statefarm.week4reviewedlab;

import java.security.SecureRandom;

public class Performer implements IdGen{

	public Performer() {}
	
	@Override
	public long generateId() {
		SecureRandom randomNumbers = new SecureRandom();
		int id = 1 + randomNumbers.nextInt(7);
		return id;
	}
	
	public String perform()
	{
		return String.format("%d - performer", generateId());
	}
}
