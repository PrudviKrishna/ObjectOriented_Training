package com.statefarm.week4reviewedlab;

public interface IdGen {

	long generateId();
}
