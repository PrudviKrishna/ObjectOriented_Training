package com.statefarm.week4reviewedlab;

public class Vocalist extends Performer {

	private char key;
	
	public Vocalist(char key)
	{
		this.key = key;
	}

	public char getKey() {
		return key;
	}

	public void setKey(char key) {
		this.key = key;
	}
		
	public String perform(int volume)
	{
		return String.format("I sing in the key of - %c - at the volume %d - %d", getKey(), volume, generateId());
	}
	
}
