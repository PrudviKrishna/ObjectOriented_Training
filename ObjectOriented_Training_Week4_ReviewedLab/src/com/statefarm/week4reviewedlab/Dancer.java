package com.statefarm.week4reviewedlab;

public class Dancer extends Performer {

	private String style;
	
	public Dancer(String style)
	{
		this.style = style;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
	
	@Override
	public String perform()
	{
		return String.format("%s - %d - dancer", getStyle(),generateId());
	}
}
