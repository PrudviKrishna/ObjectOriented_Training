package com.statefarm.week4reviewedlab;

public class Audition {
	
	public static void main(String[] args)
	{
		Performer[] performers = new Performer[7];
		for (int i = 0; i < 4; i++)
		{
			performers[i] = new Performer();
		}
		
		performers[4] = new Dancer("tap");
		performers[5] = new Dancer("break");
		
		performers[6] = new Vocalist('G');
		
		for (Performer performer : performers)
		{
			System.out.println(performer.perform());
		}
		
	}
}
