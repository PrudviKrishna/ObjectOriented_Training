package com.ntier.proxypattern;

public class BankAccount {

	private int accountNum;
	private String pin;
	private String name;
	private double balance;
	
	public BankAccount() {
		
	}

	public BankAccount(int accountNum, String pin, String name, double balance) {
		this.accountNum = accountNum;
		this.pin = pin;
		this.name = name;
		this.balance = balance;
	}

	public int getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(int accountNum) {
		this.accountNum = accountNum;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
	
	
			
}
