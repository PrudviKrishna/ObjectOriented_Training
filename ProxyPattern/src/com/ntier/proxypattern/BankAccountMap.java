package com.ntier.proxypattern;

import java.util.HashMap;
import java.util.Map;

public class BankAccountMap {

	// database of accounts
	private static Map<Integer, BankAccount> accountMap = new HashMap<Integer, BankAccount>();
	
	static {
		accountMap.put(111111, new BankAccount(111111, "1234", "David Beckham", 100.00));
		accountMap.put(222222, new BankAccount(222222, "2345", "Bill Gates", 200.00));
		accountMap.put(333333, new BankAccount(333333, "3456", "Elton John", 300.00));
	}
	
	// other methods not shown fully
	static Boolean checkPin(int accountNum, String pin) {
		return false;
	}
	
	static BankAccount getAccount(int accountNum)
	{
		return new BankAccount();
	}
}

