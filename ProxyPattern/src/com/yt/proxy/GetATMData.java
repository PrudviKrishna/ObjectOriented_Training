package com.yt.proxy;

public interface GetATMData {

	ATMState getATMData();
	int getCashInMachine();
}
