package com.yt.proxy;

public class HasCorrectPin implements ATMState{

	private ATMMachine atmMachine;
	
	public HasCorrectPin(ATMMachine atmMachine)
	{
		this.atmMachine = atmMachine;
	}
	
	@Override
	public void insertCard() {
		System.out.println("You cannot enter more than one card");
		
	}

	@Override
	public void ejectCard() {
		System.out.println("Card ejected");
		atmMachine.setAtmState(atmMachine.getNoCardState());
		
	}

	@Override
	public void insertPin(int pinEntered) {
		System.out.println("Already entered PIN");
		
	}

	@Override
	public void requestCash(int cashToWithdraw) {
		if (cashToWithdraw > atmMachine.cashInMachine)
		{
			System.out.println("Don't have that Cash");
			System.out.println("Card ejected");
			atmMachine.setAtmState(atmMachine.getNoCardState());
		} else {
			System.out.println(cashToWithdraw + "is provided by the machine");
			atmMachine.setCashInMachine(atmMachine.cashInMachine - cashToWithdraw);
			System.out.println("Card ejected");
			atmMachine.setAtmState(atmMachine.getNoCardState());
		}
		
		if (atmMachine.cashInMachine <= 0)
		{
			atmMachine.setAtmState(atmMachine.getNoCashState());
		}
		
	}

}
