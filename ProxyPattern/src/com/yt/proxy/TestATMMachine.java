package com.yt.proxy;


public class TestATMMachine {

	public static void main(String[] args) {
		
		ATMMachine atmMachine = new ATMMachine();
		
		atmMachine.insertCard();
		atmMachine.ejectCard();
		System.out.println("-------------------------------");
		
		atmMachine.insertCard();
		atmMachine.insertPin(1234);
		atmMachine.requestCash(2000);
		
		System.out.println("-------------------------------");
		atmMachine.insertCard();
		atmMachine.insertPin(1234);
		
		GetATMData realATMMachine = new ATMMachine();
		
		GetATMData atmProxy = new ATMProxy();
		
		System.out.println("\nCurrent ATM State " + atmProxy.getATMData());
		System.out.println("\nCash in ATM Machine $ " + atmProxy.getCashInMachine());
		
	}

}
