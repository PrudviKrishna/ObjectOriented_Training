package com.yt.proxy;

public class NoCard implements ATMState{

	private ATMMachine atmMachine;
	
	public NoCard(ATMMachine atmMachine)
	{
		this.atmMachine = atmMachine;
	}
	
	@Override
	public void insertCard() {
		System.out.println("Enter the Card");
		atmMachine.setAtmState(atmMachine.getYesCardState());
		
	}

	@Override
	public void ejectCard() {
		System.out.println("Enter card first");
		
	}

	@Override
	public void insertPin(int pinEntered) {
		System.out.println("Enter card first");
		
	}

	@Override
	public void requestCash(int cashToWithdraw) {
		System.out.println("Enter card first");
		
	}

}
